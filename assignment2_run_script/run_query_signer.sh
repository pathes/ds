if [ $# -ne 2 ]; then
	echo "Usage: <port> <private_key_file>" >&2
	echo "<port> should be port on which query signer Spring will be running" >&2
	echo "<private_key_file> should be a file containing query signer private key" >&2
	exit 1
fi

# Get the script directory and go one level up
PROJECT_DIR=$(dirname $(realpath $(dirname $0)))


# Calculate config file path
CONFIG_FILE=$(realpath $2)

# Working directory shall be project directory - it should contain `static` subdirectory
cd $PROJECT_DIR

java -cp "$PROJECT_DIR/bin:lib/*:lib/compile/*:lib/cup.jar:lib/JLex.jar:lib/protobuf-java-2.6.1.jar" \
    -Djava.util.logging.config.file=$PROJECT_DIR/logging.properties \
    pl.edu.mimuw.cloudatlas.querysigner.QuerySignerApplication $CONFIG_FILE --server.port=$1
