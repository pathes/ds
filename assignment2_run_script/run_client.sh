if [ $# -ne 2 ]; then
	echo "Usage: <port> <query_signer_ip>:<query_signer_port>" >&2
	echo "<port> should be port on which client Spring will be running" >&2
	echo "<query_signer_ip>:<query_signer_port> should be ip:port on which query signer Spring will be running" >&2
	exit 1
fi

# Get the script directory and go one level up
PROJECT_DIR=$(dirname $(realpath $(dirname $0)))

touch $PROJECT_DIR/client.policy
echo 'grant codeBase "file:'$PROJECT_DIR'/bin" { permission java.security.AllPermission; };' > $PROJECT_DIR/client.policy

# Working directory shall be project directory - it should contain `static` subdirectory
cd $PROJECT_DIR

java -cp "$PROJECT_DIR/bin:lib/*:lib/compile/*" \
    -Djava.rmi.server.codebase=file:$PROJECT_DIR/bin \
    -Djava.security.policy=$PROJECT_DIR/client.policy \
    -Djava.util.logging.config.file=$PROJECT_DIR/logging.properties \
    pl.edu.mimuw.cloudatlas.client.ClientApplication $2 --server.port=$1
