if [ $# -ne 2 ]; then
	echo "Usage: <agent ip or host name>> <fetch interval in seconds>" >&2
	echo "<hostname> should be set in /etc/hosts" >&2
	exit 1
fi

# Get the script directory and go one level up
PROJECT_DIR=$(dirname $(realpath $(dirname $0)))

touch $PROJECT_DIR/fetcher.policy
echo 'grant codeBase "file:'$PROJECT_DIR'/bin" { permission java.security.AllPermission; };' > $PROJECT_DIR/fetcher.policy 
java -cp $PROJECT_DIR/bin:lib/cup.jar:lib/JLex.jar:lib/protobuf-java-2.6.1.jar \
    -Djava.rmi.server.codebase=file:$PROJECT_DIR/bin \
    -Djava.security.policy=$PROJECT_DIR/fetcher.policy \
    -Djava.util.logging.config.file=$PROJECT_DIR/logging.properties \
    pl.edu.mimuw.cloudatlas.fetcher.AttributesFetcherClient \
    $1 $2 $PROJECT_DIR/src/pl/edu/mimuw/cloudatlas/fetcher/scripts
