if [ $# -ne 1 ]; then
	echo "Usage: <config_file>" >&2
	echo "<config_file> should be textproto representation compatible with pl/edu/mimuw/cloudatlas/agent/config.proto AgentConfiguration" >&2
	exit 1
fi

# Get the script directory and go one level up
PROJECT_DIR=$(dirname $(realpath $(dirname $0)))

# Calculate config file path
CONFIG_FILE=$(realpath $1)

java -cp $PROJECT_DIR/bin:$PROJECT_DIR/lib/protobuf-java-2.6.1.jar \
    pl.edu.mimuw.cloudatlas.agent.ConfigValidator \
    $CONFIG_FILE
