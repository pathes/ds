if [ $# -ne 2 ]; then
	echo "Usage: <agent ip or host name> <config_file>" >&2
	echo "<hostname> should be set in /etc/hosts" >&2
	echo "<config_file> should be textproto representation compatible with pl/edu/mimuw/cloudatlas/agent/config.proto AgentConfiguration" >&2
	exit 1
fi

# Get the script directory and go one level up
PROJECT_DIR=$(dirname $(realpath $(dirname $0)))

# Calculate config file path
CONFIG_FILE=$(realpath $2)

touch $PROJECT_DIR/agent.policy
echo 'grant codeBase "file:'$PROJECT_DIR'/bin" { permission java.security.AllPermission; };' > $PROJECT_DIR/agent.policy
java -cp $PROJECT_DIR/bin:lib/cup.jar:lib/JLex.jar:lib/protobuf-java-2.6.1.jar \
    -Djava.rmi.server.codebase=file:$PROJECT_DIR/bin/ \
    -Djava.rmi.server.hostname=$1 \
    -Djava.security.policy=$PROJECT_DIR/agent.policy \
    -Djava.util.logging.config.file=$PROJECT_DIR/logging.properties \
    pl.edu.mimuw.cloudatlas.agent.Agent \
    $CONFIG_FILE
