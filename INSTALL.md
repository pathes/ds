Distributed Systems 2015
========================
----
# Dependencies

To run project, you need Java in version `>=7`.

Launcher scripts use `realpath`, so it's obligatory to install it.
```sh
sudo apt-get install realpath
```

-----
# Compilation

Compilation process creates `bin` directory and compiles project into `bin`.
To compile, run in shell:

```sh
ant
```

To clean compiled files, run in shell:
```sh
ant clean
```

-----
# Configuration files
There are two types of configuration files - agent configuration textproto files
and Query Signer RSA key files.

### Generation of RSA keyes
Only PKCS#8 format for private key and DER format for public key are accepted
as RSA keys. Example generation can be done using following script:

```sh
openssl genrsa -out private_key.pem 2048

openssl pkcs8 -topk8 -inform PEM -outform DER -in private_key.pem \
    -out private_key.der -nocrypt

openssl rsa -in private_key.pem -pubout -outform DER -out public_key.der
```

Example keys can be found in `assignment2_configs` directory.

### Agent configuration
Agent configuration file need to be consistent with protobuf structure format
defined in:
```
src/pl/edu/mimuw/cloudatlas/agent/config.proto
```

Example textproto config file can be found in:
```
assignment2_configs/example.textproto
```

`query_signer_public_key_file` attribute should contain a path to the query
signer's public key. The paths should be absolute if possible. Without that,
the paths will be relative to main project directory.

-----
# Running Application

First step is starting `rmiregistry` in `bin` directory. You can do it by
launching a script written for convenience:

```sh
./assignment2_run_script/run_registry.sh
```

### Run Agent

```sh
./assignment2_run_script/run_agent.sh <agent IP address> <path to textproto config>
```

### Run Fetcher

```sh
./assignment2_run_script/run_fetcher.sh <agent IP address> <refresh interval in ms>
```


### Run Client

```sh
./assignment2_run_script/run_client.sh <client port> <querysigner IP:port>
```

### Run Query Signer

```sh
./assignment2_run_script/run_query_signer.sh <querysigner port> <path to RSA private key>
```

### Minimal example setup on one machine

If you want to test quickly the setup on one machine, here you have
an example out-of-the-box solution.

Run each command in separate terminal:
```sh
./assignment2_run_script/run_registry.sh
```

```sh
./assignment2_run_script/run_agent.sh localhost assignment2_configs/example.textproto
```

```sh
./assignment2_run_script/run_fetcher.sh localhost 10
```

```sh
./assignment2_run_script/run_client.sh 8080 localhost:8888
```

```sh
./assignment2_run_script/run_query_signer.sh 8888 assignment2_configs/private_key.der
```

### Writing configuration files

When writing or modifying an agent configuration file, it might be helpful
to validate whether the config is well-formed.

For that purpose, you can use:

```sh
./assignment2_run_script/config_validator.sh <path to textproto config>
```

-----
# Running Tests

### Dependencies

To test you may need to add JUnit to the `CLASSPATH`:

(see http://stackoverflow.com/questions/29878517/junit-gives-error-java-lang-noclassdeffounderror-junit-framework-junit4testadap)

```sh
export CLASSPATH=/usr/share/java/junit4.jar
```

### Compilation

Test compilation process creates `test` directory and compiles test classes
from project into `test` directory.

To run them in Eclipse, you can use shortcut `Shift+Alt+X  T`

All test files have suffix `Test.java`.

To compile, run in shell:

```sh
ant compile-test
```

**Note:** testing commands are separate than build ones.
Remember to `ant clean-test` before you make `ant test`.

To compile and run tests:
```sh
ant test
```

To compile and run tests in verbose mode:
```sh
ant test-full
```

To clean compiled files, run in shell:
```sh
ant clean-test
```
