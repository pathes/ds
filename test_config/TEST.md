Distributed Systems 2015
========================
----

# Example configuration for testing on 5 machines

## Node names and IPs

  - /uw/khaki31 - 192.168.0.13
  - /uw/khaki13 - 192.168.0.21
  - /uw/khaki1 - 192.168.0.14
  - /pjwstk/whatever01 - 192.168.0.25
  - /pjwstk/whatever02 - 192.168.0.24

## Starting order

First started node was /uw/khaki1  (agent, fetcher, query signer)
Second started node was /uw/khaki31  (agent, fetcher, client)
Third started node was /uw/khaki13  (agent, fetcher), as uw/khaki13 has no valid contacts fallback contact was added by www interface.
Next started nodes were /pjwstk/whatever01 and /pjwstk/whatever02 (agent, fetcher)


## Installed query
```
SELECT sum(num_processes) AS num_processes
```

Results of queries can be seen in charts:
- `uw.png` - `num_processes` for machines in `/uw` network
- `pure.png` - here we can see effects of agents' disconnections (they were terminated by user)
- `pjwstk.png` - `num_processes` for machines in `/pjwstk` network

