Distributed Systems 2015
========================

# Tools

## Eclipse

To add `sh` extension:
http://download.eclipse.org/technology/dltk/updates-dev/5.4/

## Ivy
Spring jars can be downloaded using jar (they might be depricated at future)

```sh
ant resolve-web-dependencies
```

## Google Protocol Buffers
CloudAtlas uses Google protocol buffers (shortly, *protobufs*) as a
serializable data format.

Every time we change any `*.proto` file that keeps the definition of
a protobuf, we need to generate Java source files that keep their
object definition.

Such operation can be done using `protoc` compiler. Example:

```sh
protoc --proto_path=src --java_out=src src/pl/edu/mimuw/cloudatlas/message/message.proto
```

but fortunately, we have an automated way for doing this for all
`*.proto` files in `src`:

```sh
ant protobuf
```

To get `protoc` compiler:

```sh
sudo apt-get install protobuf-compiler libprotobuf-java
```

Make sure that `protoc` is in version `>=2.6`.
On Ubuntu 12.04, there may be need to install it from PPA.

# Miscellaneous

## Cryptography

RSA key pairs generation is described here:
```
http://codeartisan.blogspot.com/2009/05/public-key-cryptography-in-java.html
```

## Logging
If you want more detailed runtime information, modify `logging.properties` file.

For example, change `java.util.logging.ConsoleHandler.level` from `INFO` to `FINE`.
