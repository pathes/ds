CloudAtlas
==========

Authors
-------

- Paweł Kamiński, 320408
- Patryk Hes, 320327

Installation
------------

See `INSTALL.md`

Development
-----------

See `README_DEVELOPERS.md`

Architecture and usage
----------------------

See `doc/cloudatlas_manual.pdf`

Example network test
----------------------

See `test_config/TEST.md`
