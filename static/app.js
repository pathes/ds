(function(window, angular, storage) {

  angular.module('cloudAtlasApp', ['ngRoute', 'chart.js'])
    .config(function ($routeProvider) {
      $routeProvider
        .when('/zmi', {
          templateUrl: 'partials/zmi.html',
          controller: 'zmiCtrl'
        })
        .when('/query', {
          templateUrl: 'partials/query.html',
          controller: 'queryCtrl'
        })
        .when('/fallback-contacts', {
          templateUrl: 'partials/fallback_contacts.html',
          controller: 'fallbackContactsCtrl'
        })
        .otherwise({
          redirectTo: '/zmi'
        });
    })

    .controller('menuCtrl', function ($scope, $route, $location, agentInterface) {
      $scope.hostname = agentInterface.getHostname();
      $scope.newHostname = $scope.hostname;
      $scope.menuEntries = [
        {title: 'Browse ZMI', href: '/zmi'},
        {title: 'Queries', href: '/query'},
        {title: 'Fallback contacts', href: '/fallback-contacts'},
      ];
      $scope.currentPath = $route.current;
      $scope.$on('$routeChangeStart', function (next, current) {
        $scope.currentPath = $location.path();
      });
      $scope.setHostname = function () {
        $scope.hostname = $scope.newHostname;
        agentInterface.setHostname($scope.newHostname);
      };
    })

    .service('loggerService', function () {
      var msg = "";
      var msgChangeCallbacks = [];
      var loggerService = this;
      this.registerMsgChangeCallback = function (callback) {
        msgChangeCallbacks.push(callback);
      }
      this.setMsg = function (newMsg) {
        msg = newMsg;
        for (var i = 0; i < msgChangeCallbacks.length; ++i) {
          msgChangeCallbacks[i](msg);
        }
      }
      this.setJson = function (newMsg) {
        if (newMsg.data.message) {
          loggerService.setMsg(newMsg.data.error + ": " + newMsg.data.message);
        } else {
          loggerService.setMsg(newMsg.data.error);
        }
      }
    })

    .controller('loggerCtrl', function ($scope, loggerService) {
      loggerService.registerMsgChangeCallback(function (msg) {
        $scope.msg = msg;
      })
    })

    // Utility service that is interface for Agent's REST services
    .service('agentInterface', function ($http, loggerService) {
      var agentInterface = this;
      var hostname = storage.getItem('agent_hostname') || 'localhost';
      var hostnameChangeCallbacks = [];

      this.getHostname = function () {
        return hostname;
      };
      this.setHostname = function (newHostname) {
        hostname = newHostname;
        for (var i = 0; i < hostnameChangeCallbacks.length; ++i) {
          hostnameChangeCallbacks[i]();
        }
        storage.setItem('agent_hostname', newHostname);
      };
      this.registerHostnameChangeCallback = function (callback) {
        hostnameChangeCallbacks.push(callback);
      }

      // Get a particular ZMI
      this.getZmi = function (zmiPath, callback) {
        $http({
          method: 'GET',
          url: '/agent/' + agentInterface.getHostname() + '/attributes',
          params: {zmiPath: zmiPath}
        }).then(
          function success(response) {
            callback(response.data.attributes_map_entries);
          },
          loggerService.setJson
        );
      }
      this.setEntry = function (key, value, zmiPath, callback) {
        $http({
          method: 'POST',
          url: '/agent/' + agentInterface.getHostname() + '/attribute',
          data: {
            pathname: zmiPath,
            key: key,
            value: value
          }
        }).then(
          function success(response) {
            callback();
          },
          loggerService.setJson
        );
      }

      // List of all zones that are held by agent
      this.getZones = function (callback) {
        $http({
          method: 'GET',
          url: '/agent/' + agentInterface.getHostname() + '/zones'
        }).then(
          function success(response) {
            response.data.sort();
            callback(response.data);
          },
          loggerService.setJson
        );
      }

      // List of queries installed on agent
      this.getQueries = function (callback) {
        $http({
          method: 'GET',
          url: '/agent/' + agentInterface.getHostname() + '/query'
        }).then(
          function success(response) {
            callback(response.data.query);
          },
          loggerService.setJson
        );
      }

      // Add or delete a query - depending on emptiness of content
      this.setQuery = function (name, content, callback) {
        $http({
          method: 'POST',
          url: '/agent/' + agentInterface.getHostname() + '/query',
          data: {
            queryName: name,
            queryContent: content
          }
        }).then(
          callback,
          loggerService.setJson
        );
      }

      // List of fallback contacts that are held by agent
      this.getFallbackContacts = function (callback) {
        $http({
          method: 'GET',
          url: '/agent/' + agentInterface.getHostname() + '/contacts'
        }).then(
          function success(response) {
            callback(response.data.contact);
          },
          loggerService.setJson
        );
      }

      // Set all fallback contacts on agent
      this.setFallbackContacts = function (contacts, callback) {
        $http({
          method: 'POST',
          url: '/agent/' + agentInterface.getHostname() + '/contacts',
          data: {contact: contacts}
        }).then(
          callback,
          loggerService.setJson
        );
      }

      return this;
    })

    .controller('zmiCtrl', function ($scope, $http, $interval,
                                     agentInterface, loggerService) {

      function getZmi(zone) {
        agentInterface.getZmi(zone, function (entries) {
          $scope.entries = entries;
          updateCharts(entries);
        });
      }

      var refresher = $interval(function () {
        getZmi($scope.pickedZone);
      }, 2000);

      function getZonesAndInitialZmi() {
        $scope.zones = ['/'];
        // By default, fetch ZMI entries of `/`
        agentInterface.getZmi('/', function (entries) {
          $scope.entries = entries;
        });
        // Fetch zones list
        agentInterface.getZones(function (zones) {
          $scope.zones = zones;
          var pickedZone = storage.getItem('zmi_zone');
          // If we cannot access stored zone, pick `/`
          if (!pickedZone || zones.indexOf(pickedZone) === -1) {
            pickedZone = '/';
          }
          $scope.pickedZone = pickedZone;
        });
      }

      // Check if picked zone is a leaf
      function checkLeaf() {
        if (!$scope.zones || !Array.isArray($scope.zones)) {
          return false;
        }
        if (!$scope.pickedZone) {
          return false;
        }
        for (var i = 0; i < $scope.zones.length; i++) {
          var prefix = $scope.zones[i].slice(0, $scope.pickedZone.length);
          if (
            prefix === $scope.pickedZone
            && $scope.zones[i] !== $scope.pickedZone
          ) {
            return false;
          }
        }
        return true;
      }

      getZonesAndInitialZmi();

      // Set watcher on pickedZone
      $scope.$watch('pickedZone', function (zone) {
        if (zone == null) {
          zone = '/';
        } else {
          storage.setItem('zmi_zone', zone);
        }
        getZmi(zone);
        $scope.isLeaf = checkLeaf();
        resetCharts();
      });

      // Get zones on every hostname change
      agentInterface.registerHostnameChangeCallback(getZonesAndInitialZmi);

      $scope.$on('$destroy', function () {
        if (refresher) {
          $interval.cancel(refresher);
        }
      });

      $scope.setEntry = function () {
        var parsedValue;
        try {
          parsedValue = JSON.parse($scope.newValue);
        } catch (err) {
          loggerService.setMsg("Invalid JSON in value");
        }
        agentInterface.setEntry(
          $scope.newKey, JSON.parse($scope.newValue), $scope.pickedZone,
          function () {
            $scope.newKey = "";
            $scope.newValue = "";
          }
        );
      };

      function resetCharts() {
        $scope.charts = {
          list: [],
          entryCount: 200,
          labels: Array(200).fill("")
        };
      }
      resetCharts();

      function updateCharts(entries) {
        var newEntries = {};
        var i;
        // Filter out non-numeric entries
        for (i = 0; i < entries.length; ++i) {
          if ('value_int' in entries[i].value) {
            newEntries[entries[i].key] = entries[i].value.value_int;
          } else if ('value_double' in entries[i].value) {
            newEntries[entries[i].key] = entries[i].value.value_double;
          } else if ('value_time' in entries[i].value) {
            newEntries[entries[i].key] = entries[i].value.value_time;
          } else if ('value_interval' in entries[i].value) {
            newEntries[entries[i].key] = entries[i].value.value_interval;
          }
        }
        // Go through existing charts
        for (i = 0; i < $scope.charts.list.length; ++i) {
          var name = $scope.charts.list[i].name;
          if (name in newEntries) {
            $scope.charts.list[i].data[0].push(newEntries[name]);
            newEntries[name] = null;
          } else {
            $scope.charts.list[i].data[0].push(null);
          }
        }
        // Go through new charts that need to be created
        for (name in newEntries) {
          if (newEntries[name] != null) {
            var newData = Array($scope.charts.entryCount).fill(null);
            newData.push(newEntries[name]);
            var newChart = {
              name: name,
              data: [newData]
            };
            $scope.charts.list.push(newChart);
          }
        }

        $scope.charts.labels.push("");

        if ($scope.charts.entryCount < 200) {
          $scope.charts.entryCount++;
        } else {
          for (i = 0; i < $scope.charts.list.length; ++i) {
            $scope.charts.list[i].data[0].shift();
          }
          $scope.charts.labels.shift();
        }
      }
    })

    .controller('queryCtrl', function ($scope, $interval, agentInterface) {
      function reloadQueries() {
        agentInterface.getQueries(function (queries) {
          $scope.queries = queries;
        });
      }

      $scope.setQuery = function (name, content) {
        agentInterface.setQuery(name, content, function () {
          $scope.newName = '';
          $scope.newContent = '';
          $interval(reloadQueries, 200, 10);
        });
      }

      $scope.queries = [];
      reloadQueries();
    })

    .controller('fallbackContactsCtrl', function ($scope, agentInterface) {
      $scope.newContact = function () {
        // Make a shallow copy of contacts
        var newContacts = ($scope.contacts || []).slice(0);
        // Add a new contact into the table
        newContacts.push({
          path_name: $scope.newPathname || '/',
          inet_address: $scope.newAddress || '127.0.0.1'});
        // On update success, set those contacts and clear inputs
        agentInterface.setFallbackContacts(newContacts, function () {
          $scope.contacts = newContacts;
          $scope.newPathname = '';
          $scope.newAddress = '';
        });
      }

      $scope.deleteContact = function (contact) {
        // Make a shallow copy of contacts
        var newContacts = ($scope.contacts || []).slice(0);
        // Try to delete contact
        var deleteIndex = newContacts.indexOf(contact);
        if (deleteIndex == -1) {
          return;
        }
        newContacts.splice(deleteIndex, 1);
        // On update success, update list and clear inputs
        agentInterface.setFallbackContacts(newContacts, function () {
          $scope.contacts = newContacts;
          $scope.newPathname = '';
          $scope.newAddress = '';
        });
      }

      $scope.contacts = [];
      agentInterface.getFallbackContacts(function (contacts) {
        $scope.contacts = contacts;
      });
    })
}(window, angular, window.localStorage));
