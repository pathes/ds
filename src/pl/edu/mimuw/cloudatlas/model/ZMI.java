/**
 * Copyright (c) 2014, University of Warsaw
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted
 * provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 * conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package pl.edu.mimuw.cloudatlas.model;

import java.io.PrintStream;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import pl.edu.mimuw.cloudatlas.model.CloudAtlasModels.AttributesMapEntryProto;
import pl.edu.mimuw.cloudatlas.model.CloudAtlasModels.ZmiProto;

/**
 * A zone management information. This object is a single node in a zone hierarchy. It stores zone attributes as well as
 * references to its father and sons in the tree.
 */
public class ZMI implements Cloneable {
	private final AttributesMap attributes = new AttributesMap();

	private final List<ZMI> sons = new ArrayList<ZMI>();
	private ZMI father;

	/**
	 * Creates a new ZMI with no father (the root zone) and empty sons list.
	 */
	public ZMI() {
		this(null);
	}

	/**
	 * Creates a new ZMI with the specified node as a father and empty sons list. This method does not perform any
	 * operation on the <code>father</code>. Especially, setting this object as a <code>father</code>'s son must be done
	 * separately.
	 *
	 * @param father a father of this ZMI
	 * @see #addSon(ZMI)
	 */
	public ZMI(ZMI father) {
		this.father = father;
	}

	/**
	 * Gets a father of this ZMI in a tree.
	 *
	 * @return a father of this ZMI or <code>null</code> if this is the root zone
	 */
	public ZMI getFather() {
		return father;
	}

	/**
	 * Sets or changes a father of this ZMI in a tree. This method does not perform any operation on the
	 * <code>father</code>. Especially, setting this object as a <code>father</code>'s son must be done separately.
	 *
	 * @param father a new father for this ZMI
	 * @see #addSon(ZMI)
	 */
	public void setFather(ZMI father) {
		this.father = father;
	}

	/**
	 * Gets a list of sons of this ZMI in a tree. Modifying a return value will cause an exception.
	 *
	 * @return
	 */
	public List<ZMI> getSons() {
		return Collections.unmodifiableList(sons);
	}

	/**
	 * Adds the specified ZMI to the list of sons of this ZMI. This method does not perform any operation on a
	 * <code>son</code>. Especially, setting this object as a <code>son</code>'s father must be done separately.
	 *
	 * @param son
	 * @see #ZMI(ZMI)
	 * @see #setFather(ZMI)
	 */
	public void addSon(ZMI son) {
		sons.add(son);
	}

	/**
	 * Removes the specified ZMI from the list of sons of this ZMI. This method does not perform any operation on a
	 * <code>son</code>. Especially, its father remains unchanged.
	 *
	 * @param son
	 * @see #setFather(ZMI)
	 */
	public void removeSon(ZMI son) {
		sons.remove(son);
	}

	/**
	 * Gets a map of all the attributes stored in this ZMI.
	 *
	 * @return map of attributes
	 */
	public AttributesMap getAttributes() {
		return attributes;
	}

	/**
	 * Prints recursively in a prefix order (starting from this ZMI) a whole tree with all the attributes.
	 *
	 * @param stream a destination stream
	 * @see #toString()
	 */
	public void printAttributes(PrintStream stream) {
		for(Entry<Attribute, Value> entry : attributes)
			stream.println(entry.getKey() + " : " + entry.getValue().getType() + " = " + entry.getValue());
		stream.println();
		for(ZMI son : sons)
			son.printAttributes(stream);
	}

	/**
	 * Creates an independent copy of a whole hierarchy. A returned ZMI has the same reference as a father (but the
	 * father does not have a reference to it as a son). For the root zone, the copy is completely independent, since
	 * its father is <code>null</code>.
	 *
	 * @return a deep copy of this ZMI
	 */
	@Override
	public ZMI clone() {
		ZMI result = new ZMI(father);
		result.attributes.add(attributes.clone());
		for(ZMI son : sons) {
			ZMI sonClone = son.clone();
			result.sons.add(sonClone);
			sonClone.father = result;
		}
		return result;
	}

	/**
	 * Prints a textual representation of this ZMI. It contains only attributes of this node.
	 *
	 * @return a textual representation of this object
	 * @see #printAttributes(PrintStream)
	 */
	@Override
	public String toString() {
		return attributes.toString();
	}

	public static ZMI fromProto(ZmiProto proto) {
		ZMI result = new ZMI();
		for (ZmiProto sonProto : proto.getSonsList()) {
			ZMI newSon = ZMI.fromProto(sonProto);
			newSon.setFather(result);
			result.addSon(newSon);
		}
		for (AttributesMapEntryProto entry : proto.getAttributesMapEntriesList()) {
			result.getAttributes().add(entry);
		}
		return result;
	}

	/**
	 * Convert ZMI to protobuf format.
	 * @param allowedAttributes Set containing the names of attributes that are allowed to be in the protobuf.
	 *                          If the set is null or empty, all attributes are taken into account.
	 * @param depthLimit How many levels should be included.
	 * 					 depthLimit = 0 means no recursion
	 *                   depthLimit < 0 means recursion without depth limit
	 * @return ZmiProto protobuf object corresponding to that ZMI object.
	 */

	public ZmiProto toProto(Set<String> allowedAttributes, int depthLimit) {
		ZmiProto.Builder resultBuilder = ZmiProto.newBuilder();

		int newDepthLimit = depthLimit;
		if (depthLimit > 0) {
			newDepthLimit--;
		}

		if (depthLimit != 0) {
			for (ZMI son : sons) {
				resultBuilder.addSons(son.toProto(allowedAttributes, newDepthLimit));
			}
		}

		if (allowedAttributes == null || allowedAttributes.isEmpty()) {
			resultBuilder.addAllAttributesMapEntries(attributes.entryProtoList());
		} else {
			for (AttributesMapEntryProto entry : attributes.entryProtoList()) {
				if (allowedAttributes.contains(entry.getKey())) {
					resultBuilder.addAttributesMapEntries(entry);
				}
			}
		}

		return resultBuilder.build();
	}

	/**
	 * Get a ZMI subtree following given path.
	 * @param components List of pathname components to follow.
	 * @param createNonExistent Specifies if we should create nodes that can't be found.
	 * @return ZMI represented by path (may be null if ZMI not found).
	 */
	public ZMI findDescendant(PathName pathname, boolean createNonExistent) {
		return findDescendant(pathname.getComponents(), createNonExistent);
	}

	/**
	 * Get a ZMI subtree following given path.
	 * @param components List of pathname components to follow.
	 * @param createNonExistent Specifies if we should create nodes that can't be found.
	 * @return ZMI represented by path (may be null if ZMI not found).
	 */
	public ZMI findDescendant(List<String> components, boolean createNonExistent) {
		if (components.isEmpty()) {
			return this;
		}
		String head = components.get(0);
		ZMI foundSon = null;
		// Go recursively through sons
		for (ZMI son : sons) {
			Value sonsName = son.getAttributes().getOrNull("name");
			if (sonsName == null) {
				throw new TreeTraversalException("Found ZMI node inside of tree without name: " + son);
			}
			if (((ValueString) son.getAttributes().getOrNull("name")).getValue().equals(head)) {
				foundSon = son;
				break;
			}
		}

		if (foundSon == null) {
			if (createNonExistent) {
				// If we didn't find the son, let's create it
				foundSon = new ZMI(this);
				this.addSon(foundSon);
				AttributesMap sonAttrs = foundSon.getAttributes();
				sonAttrs.add("name", new ValueString(head));
				sonAttrs.add("owner", new ValueString("/"));
				sonAttrs.add("contacts", new ValueList(Collections.<Value>emptyList(), TypePrimitive.CONTACT));
				sonAttrs.add("timestamp", new ValueTime(0L));
			} else {
				// If we didn't find the son, return null
				return null;
			}
		}
		// If we found the son, perform recursive search
		return foundSon.findDescendant(components.subList(1, components.size()), createNonExistent);
	}

	public PathName getPathName() {
		Deque<String> names = new ArrayDeque<String>();
		ZMI current = this;
		while (current.getFather() != null) {
			Value value = current.getAttributes().getOrNull("name");
			if (value == null || !(value instanceof ValueString)) {
				break;
			}
			names.push(((ValueString) current.getAttributes().get("name")).getValue());
			current = current.getFather();
		}
		// Stack is LIFO, so the iteration order will be opposite than the injection order
		return new PathName(names);
	}

	/**
	 * @return List containing all contacts from this particular ZMI.
	 */
	public List<ValueContact> getContacts() {
		// Get the value
		Value fetchedValue = attributes.getOrNull("contacts");
		// Retrieve inner data structure
		Iterable<Value> iterableOfValue;
		if (fetchedValue instanceof ValueList) {
			iterableOfValue = ((ValueList) fetchedValue).getValue();
		} else if (fetchedValue instanceof ValueSet) {
			iterableOfValue = ((ValueSet) fetchedValue).getValue();
		} else {
			return Collections.<ValueContact>emptyList();
		}
		// Validate that all elements are contacts
		List<ValueContact> listOfContacts = new ArrayList<ValueContact>();
		for (Value value : iterableOfValue) {
			if (value instanceof ValueContact) {
				listOfContacts.add((ValueContact) value);
			}
		}
		return listOfContacts;
	}

	/**
	 * @return Timestamp of this ZMI. If not set or invalid, returns null.
	 */
	public Long getTimestamp() {
		Value value = attributes.getOrNull("timestamp");
		if (value == null || !(value instanceof ValueTime)) {
			return null;
		}
		return ((ValueTime) value).getValue();
	}
}
