package pl.edu.mimuw.cloudatlas.model;

import org.junit.Test;
import static org.junit.Assert.*;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class ValueStringTest {

	// set -> string: in format { e1, e2, e3 }
	@Test
	public void testFromSet() {
		Set<Value> set = new LinkedHashSet<Value>();
		set.add(new ValueInt(1l));
		set.add(new ValueInt(5l));
		set.add(new ValueInt(15l));
		ValueSet valueSet = new ValueSet(set, TypePrimitive.INTEGER);
		ValueString converted = (ValueString) valueSet.convertTo(TypePrimitive.STRING);
		assertEquals("{1, 5, 15}", converted.getValue());
	}

	// set -> string:  empty set: { }
	@Test
	public void testFromEmptySet() {
		Set<Value> set = new LinkedHashSet<Value>();
		ValueSet valueSet = new ValueSet(set, TypePrimitive.INTEGER);
		ValueString converted = (ValueString) valueSet.convertTo(TypePrimitive.STRING);
		assertEquals("{}", converted.getValue());
	}

	// list -> string: in format [ e1, e2, e3 ]
	@Test
	public void testFromList() {
		List<Value> list = new ArrayList<Value>();
		list.add(new ValueInt(1l));
		list.add(new ValueInt(5l));
		list.add(new ValueInt(15l));
		ValueList valueList = new ValueList(list, TypePrimitive.INTEGER);
		ValueString converted = (ValueString) valueList.convertTo(TypePrimitive.STRING);
		assertEquals("[1, 5, 15]", converted.getValue());
	}

	// list -> string: empty list: [ ]
	@Test
	public void testFromEmptyList() {
		List<Value> list = new ArrayList<Value>();
		ValueList valueList = new ValueList(list, TypePrimitive.INTEGER);
		ValueString converted = (ValueString) valueList.convertTo(TypePrimitive.STRING);
		assertEquals("[]", converted.getValue());
	}

	// string -> string: identity
	@Test
	public void testFromString() {
		ValueString original = new ValueString("foo");
		ValueString converted = (ValueString) original.convertTo(TypePrimitive.STRING);
		assertEquals("foo", converted.getValue());
	}

	// string -> string: identity
	@Test
	public void testFromEmptyString() {
		ValueString original = new ValueString("");
		ValueString converted = (ValueString) original.convertTo(TypePrimitive.STRING);
		assertEquals("", converted.getValue());
	}

	// contact -> string: arbitrarily
	@Test
	public void testFromContact() throws UnknownHostException {
		ValueContact contact = new ValueContact(
				new PathName("/foo/bar"),
				InetAddress.getByName("127.0.0.1"));
		ValueString converted = (ValueString) contact.convertTo(TypePrimitive.STRING);
		assertEquals("(/foo/bar, /127.0.0.1)", converted.getValue());
	}

	// null -> string: NULL
	@Test
	public void testFromNull() {
		ValueString converted = (ValueString) ValueNull.getInstance().convertTo(TypePrimitive.STRING);
		assertEquals("NULL", converted.getValue());
	}

	// boolean -> string: true
	@Test
	public void testFromTrue() {
		ValueString converted = (ValueString) new ValueBoolean(true).convertTo(TypePrimitive.STRING);
		assertEquals("true", converted.getValue());
	}

	// boolean -> string: false
	@Test
	public void testFromFalse() {
		ValueString converted = (ValueString) new ValueBoolean(false).convertTo(TypePrimitive.STRING);
		assertEquals("false", converted.getValue());
	}

	// duration -> string: in format (-|+)days hh:mm:ss.fff and 0 is formatted to +0
	@Test
	public void testFromDuration() {
		String[] durationStrings = {"+2 21:37:42.137", "-1 21:37:42.137", "+0 00:00:00.000"};
		for (String durationString : durationStrings) {
			ValueDuration duration = new ValueDuration(durationString);
			ValueString converted = (ValueString) duration.convertTo(TypePrimitive.STRING);
			assertEquals(durationString, converted.getValue());
		}
	}
}