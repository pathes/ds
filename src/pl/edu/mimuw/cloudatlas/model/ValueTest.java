package pl.edu.mimuw.cloudatlas.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Test;

import pl.edu.mimuw.cloudatlas.model.CloudAtlasModels.ValueProto;

public class ValueTest {

	@Test
	public void testSerializingSet() throws IOException {
		ValueSet set = new ValueSet(TypePrimitive.STRING);
		set.add(new ValueString("foo"));
		set.add(new ValueString("bar"));
		set.add(new ValueString("baz"));
		ValueProto proto = set.toProto();
		Value set2 = Value.fromProto(proto);
		assertTrue(set2 instanceof ValueSet);
		ValueSet casted = (ValueSet) set2;
		assertEquals(3, casted.size());
		assertTrue(casted.contains(new ValueString("foo")));
		assertTrue(casted.contains(new ValueString("bar")));
		assertTrue(casted.contains(new ValueString("baz")));
	}


	@Test
	public void testSerializingList() throws IOException {
		ValueList list = new ValueList(TypePrimitive.STRING);
		list.add(new ValueString("foo"));
		list.add(new ValueString("bar"));
		list.add(new ValueString("baz"));
		ValueProto proto = list.toProto();
		Value list2 = Value.fromProto(proto);
		assertTrue(list2 instanceof ValueList);
		ValueList casted = (ValueList) list2;
		assertEquals(3, casted.size());
		assertEquals(new ValueString("foo"), casted.get(0));
		assertEquals(new ValueString("bar"), casted.get(1));
		assertEquals(new ValueString("baz"), casted.get(2));
	}
}
