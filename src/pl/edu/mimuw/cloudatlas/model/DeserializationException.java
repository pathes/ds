package pl.edu.mimuw.cloudatlas.model;

@SuppressWarnings("serial")
public class DeserializationException extends RuntimeException {
	public DeserializationException(String message) {
		super(message);
	}
}
