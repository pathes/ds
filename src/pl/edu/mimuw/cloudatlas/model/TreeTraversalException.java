package pl.edu.mimuw.cloudatlas.model;

@SuppressWarnings("serial")
public class TreeTraversalException extends RuntimeException {
	public TreeTraversalException(String message) {
		super(message);
	}
}
