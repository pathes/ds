package pl.edu.mimuw.cloudatlas.model;

import org.junit.Test;

import pl.edu.mimuw.cloudatlas.model.CloudAtlasModels.ValueListProto;
import pl.edu.mimuw.cloudatlas.model.CloudAtlasModels.ValueProto;

import static org.junit.Assert.*;

import org.junit.Before;

public class ValueListTest {
	private ValueList list;

	@Before
	public void setUp() {
		list = new ValueList(TypePrimitive.INTEGER);
		list.add(new ValueInt(15L));
		list.add(new ValueInt(10000000L));
		list.add(new ValueInt(-3L));
	}

	@Test
	public void testToProto() {
		ValueListProto listProto = ValueListProto.newBuilder()
			.addValues(ValueProto.newBuilder().setValueInt(15L).build())
			.addValues(ValueProto.newBuilder().setValueInt(10000000L).build())
			.addValues(ValueProto.newBuilder().setValueInt(-3L).build())
			.build();
		ValueProto expected = ValueProto.newBuilder()
			.setValueList(listProto)
			.build();
		assertEquals(expected, list.toProto());
	}

	@Test
	public void testFromProto() {
		ValueListProto listProto = ValueListProto.newBuilder()
			.addValues(ValueProto.newBuilder().setValueInt(15L).build())
			.addValues(ValueProto.newBuilder().setValueInt(10000000L).build())
			.addValues(ValueProto.newBuilder().setValueInt(-3L).build())
			.build();
		ValueProto valueProto = ValueProto.newBuilder()
			.setValueList(listProto)
			.build();
		assertEquals(list, Value.fromProto(valueProto));
	}

	/**
	 * Test if exception is thrown when there are mixed types in list proto.
	 */
	@Test(expected = DeserializationException.class)
	public void testFromMalformedProto() {
		ValueListProto listProto = ValueListProto.newBuilder()
			.addValues(ValueProto.newBuilder().setValueInt(15L).build())
			.addValues(ValueProto.newBuilder().setValueInt(10000000L).build())
			.addValues(ValueProto.newBuilder().setValueString("foo").build())
			.build();
		ValueProto valueProto = ValueProto.newBuilder()
			.setValueList(listProto)
			.build();
		Value.fromProto(valueProto);
	}
}
