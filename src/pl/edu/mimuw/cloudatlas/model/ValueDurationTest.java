package pl.edu.mimuw.cloudatlas.model;

import org.junit.Test;
import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map.Entry;
import org.junit.Before;

public class ValueDurationTest {

	private HashMap<String, Long> testcases;

	@Before
	public void setUp() {
		testcases = new HashMap<String, Long>();
		testcases.put("+2 21:37:42.137", 250662137l);
		testcases.put("-1 21:37:42.137", -164262137l);
		testcases.put("+0 00:00:00.000", 0l);
		testcases.put("+0 00:00:00.001", 1l);
		testcases.put("+0 00:00:01.000", 1000l);
	}

	@Test
	public void testMultiplyDuration() {
		ValueDuration duration = new ValueDuration(5000l);
		ValueInt multiplier = new ValueInt(5l);
		ValueDuration multiplied1 = duration.multiply(multiplier);
		assertEquals(new Long(25000l), multiplied1.getValue());
		ValueDuration multiplied2 = (ValueDuration) multiplier.multiply(duration);
		assertEquals(new Long(25000l), multiplied2.getValue());
	}


	@Test
	public void testDivideDuration() {
		ValueDuration duration = new ValueDuration(50000l);
		ValueInt divisor = new ValueInt(2l);
		ValueDuration divided = (ValueDuration) duration.divide(divisor);
		assertEquals(new Long(25000l), divided.getValue());
	}

	@Test(expected = ArithmeticException.class)
	public void testDivideDurationByZero() {
		ValueDuration duration = new ValueDuration(2l);
		ValueInt divisor = new ValueInt(0l);
		@SuppressWarnings("unused")
		ValueDuration divided = (ValueDuration) duration.divide(divisor);
	}

	@Test
	public void testModuloDuration() {
		ValueDuration duration = new ValueDuration(1737l);
		ValueInt quotient = new ValueInt(800l);
		ValueDuration remainder = duration.modulo(quotient);
		assertEquals(new Long(137l), remainder.getValue());
	}


	@Test(expected = ArithmeticException.class)
	public void testModuloDurationByZero() {
		ValueDuration duration = new ValueDuration(2l);
		ValueInt quotient = new ValueInt(0l);
		@SuppressWarnings("unused")
		ValueDuration remainder = duration.modulo(quotient);
	}

	@Test
	public void testParseDuration() {
		for (Entry<String, Long> entry : testcases.entrySet()) {
			ValueDuration duration = new ValueDuration(entry.getKey());
			assertEquals(entry.getValue(), duration.getValue());
		}
	}

	@Test(expected = IllegalArgumentException.class)
	public void testParseDurationInvalid() {
		new ValueDuration("+2 21:37:42.137137");
	}

	@Test
	public void testDurationToString() {
		for (Entry<String, Long> entry : testcases.entrySet()) {
			assertEquals(
					entry.getKey(),
					new ValueDuration(entry.getValue()).toString());
		}
	}

	// Test convertTo method
	// string -> duration: in format (-|+)days hh:mm:ss.fff and 0 is formatted to +0
	@Test
	public void testStringConvertToDuration() {
		for (Entry<String, Long> entry : testcases.entrySet()) {
			ValueString valueString = new ValueString(entry.getKey());
			ValueDuration converted = (ValueDuration) valueString.convertTo(TypePrimitive.DURATION);
			assertEquals(
					entry.getValue(),
					converted.getValue());
		}
	}
}
