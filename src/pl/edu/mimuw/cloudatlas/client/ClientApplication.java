package pl.edu.mimuw.cloudatlas.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class ClientApplication {
	public static void main(String[] args) {
		SpringApplication.run(ClientApplication.class, args);
		ClientStartArguments.setQuerySignerUrl(args[0]);
	}
}
