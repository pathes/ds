#!/bin/bash
# This script allows to check if all client API endings works

# TODO(pawelk): finish tests, check responses

echo "Test getting attributes"

curl -X GET http://localhost:8080/agent/127.0.0.1/attributes\?zmiPath\=%2F
 
echo "Test getting zones"

curl -X GET http://localhost:8080/agent/127.0.0.1/zones
 
echo "Test getting contacts"

curl -X GET http://localhost:8080/agent/127.0.0.1/contacts
 
echo "Test setting contacts"

curl -X POST -H 'Content-Type: application/json' -d '{"contact": [{"path_name": "/uw/fallback1", "inet_address": "10.0.0.1"}, {"path_name": "/uw/fallback2", "inet_address": "10.0.0.2"}]}' http://localhost:8080/agent/127.0.0.1/contacts

echo "Test setting attribute"

curl -X POST -H 'Content-Type: application/json' -d '{"pathname": "/uw/khaki31", "key": "foo", "value": {"value_string": "asdf"}}' http://localhost:8080/agent/127.0.0.1/attribute

echo "Test setting query"

curl -X POST -H 'Content-Type: application/json' -d '{"qyery":"proc_load"}' http://localhost:8080/agent/127.0.0.1/query
