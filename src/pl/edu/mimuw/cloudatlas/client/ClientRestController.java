package pl.edu.mimuw.cloudatlas.client;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.googlecode.protobuf.format.JsonFormat;

import pl.edu.mimuw.cloudatlas.RMIinterface.ClientInterface;
import pl.edu.mimuw.cloudatlas.RMIinterface.GetAttributesFromZoneClientResponse;
import pl.edu.mimuw.cloudatlas.RMIinterface.RegisterQueryAgentResponse;
import pl.edu.mimuw.cloudatlas.RMIinterface.SetAttributeClientResponse;
import pl.edu.mimuw.cloudatlas.model.CloudAtlasModels.SetAttributeProto;
import pl.edu.mimuw.cloudatlas.model.CloudAtlasModels.ValueContactProto;
import pl.edu.mimuw.cloudatlas.model.CloudAtlasModels.ValueContactProtoList;
import pl.edu.mimuw.cloudatlas.model.CloudAtlasModels.ZmiProto;
import pl.edu.mimuw.cloudatlas.model.ValueContact;
import pl.edu.mimuw.cloudatlas.model.CloudAtlasModels.QueryProto;
import pl.edu.mimuw.cloudatlas.model.CloudAtlasModels.QueryProtoList;
import pl.edu.mimuw.cloudatlas.querysigner.QuerySignerSignRequest;
import pl.edu.mimuw.cloudatlas.querysigner.QuerySignerSignResponse;


@RestController
public class ClientRestController {
    private final Logger logger = Logger.getLogger(ClientRestController.class);
    private final ObjectMapper mapper = new ObjectMapper();
    private final JsonFormat jsonFormat = new JsonFormat();


    private ResponseEntity<String> jsonError(String message, HttpStatus status) {
    	return new ResponseEntity<String>("{\"error\": \"" + message + "\"}", status);
    }

    private static ResponseEntity<String> emptyOk = new ResponseEntity<String>("{}", HttpStatus.OK);

    /**
     * Get values of attributes of a given zone.
	 * @param agentIp Destination agent's IP address.
     * @param zmiPath Pathname for locating destination ZMI. Default is "/".
     * @return Response entity containing JSON response encoded as a string, compatible with ZmiProto.
     */
	@RequestMapping(value = "/agent/{agent_ip}/attributes",
			method = RequestMethod.GET,
			params = {"zmiPath"},
			produces="application/json")
	public ResponseEntity<String> getAttributesFromZone(
			@PathVariable("agent_ip") String agentIp,
			@RequestParam(value = "zmiPath") String zmiPath) {

		logger.info("Get attributes: Get attributes called");

		if (zmiPath == null) {
			zmiPath = "/";
		}

		try {
			// Send request via RMI.
			Registry registry = LocateRegistry.getRegistry(agentIp.toString());
			ClientInterface stub = (ClientInterface) registry.lookup("Client");
			GetAttributesFromZoneClientResponse responseRMI = stub.getAttributesFromZone(zmiPath);

			switch (responseRMI.getResponse()) {
				case OK:
					logger.info("Get attributes: Message received.");
					return new ResponseEntity<String>(jsonFormat.printToString(ZmiProto.parseFrom(responseRMI.getZmiProtoBytes())), HttpStatus.OK);
				case BAD_PATH:
					logger.error("Get attributes: Bad path.");
					return jsonError("Bad path", HttpStatus.BAD_REQUEST);
				case NOT_KEPT:
					logger.error("Get attributes: Path Not Kept.");
					return jsonError("Path not kept", HttpStatus.NOT_FOUND);
				default:
					logger.error("Get attributes: Client Bad Request.");
					return jsonError("Client bad request", HttpStatus.BAD_REQUEST);
			}

		} catch (Exception e) {
			logger.error("Get attributes:  Client RMI exception:");
			e.printStackTrace();
			return jsonError("Client RMI exception", HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	/**
	 * Get all zones that are contained on agent.
	 * @param agentIp Destination agent's IP address.
	 * @return Response entity containing JSON response encoded as a string.
	 */
	@RequestMapping(value = "/agent/{agent_ip}/zones",
			method = RequestMethod.GET,
			produces="application/json")
	public ResponseEntity<String> getZones(
			@PathVariable("agent_ip") String agentIp) {

		logger.info("Get zones: Retrieving set of zones called.");

		try {
			// Send request via RMI.
			Registry registry = LocateRegistry.getRegistry(agentIp.toString());
			ClientInterface stub = (ClientInterface) registry.lookup("Client");
			Set<String> responseRMI = stub.getAllZonesPathnames();

			logger.info("Get zones: Message received.");
			return new ResponseEntity<String>(mapper.writeValueAsString(responseRMI), HttpStatus.OK);

		} catch (Exception e) {
			logger.error("Get zones:  Client RMI exception:");
			e.printStackTrace();
			return jsonError("Client RMI exception", HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	/**
	 * Get fallback contacts from agent.
	 * @param agentIp Destination agent's IP address.
	 * @return Response entity containing JSON response encoded as a string, compatible with ValueContactProtoList.
	 */
	@RequestMapping(value = "/agent/{agent_ip}/contacts",
			method = RequestMethod.GET,
			produces="application/json")
	public ResponseEntity<String> getFallbackContacts(
			@PathVariable("agent_ip") String agentIp) {

		logger.info("Get contacts: Get fallback contacts called");

		try {
			// Send request via RMI.
			Registry registry = LocateRegistry.getRegistry(agentIp.toString());
			ClientInterface stub = (ClientInterface) registry.lookup("Client");
			List<ValueContact> rmiResponse = stub.getFallbackContacts();

			ValueContactProtoList.Builder builder = ValueContactProtoList.newBuilder();
			for (ValueContact contact : rmiResponse) {
				builder.addContact(contact.toValueContactProto());
			}
			logger.info("Get contacts: Message received.");
			return new ResponseEntity<String>(jsonFormat.printToString(builder.build()), HttpStatus.OK);

		} catch (Exception e) {
			logger.error("Get contacts:  Client RMI exception:");
			e.printStackTrace();
			return jsonError("Client RMI exception", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Set fallback contacts in given agent.
	 * @param agentIp Destination agent's IP address.
	 * @param contactListString Contact list to substituted in agent.
	 * @return Response entity containing JSON response encoded as a string.
	 */
	@RequestMapping(value = "/agent/{agent_ip}/contacts",
			method = RequestMethod.POST,
			consumes="application/json",
			produces="application/json")
	public ResponseEntity<String> setFallbackContacts(
			@PathVariable("agent_ip") String agentIp,
			@RequestBody String contactListString) {

		logger.info("Set contacts: Set fallback contacts called");

		if (contactListString == null) {
			logger.warn("Set contacts: Bad fallback contacts structure or no contacts.");
			return jsonError("Set contacts: Bad fallback contacts structure or no contacts", HttpStatus.BAD_REQUEST);
		}

		ValueContactProtoList.Builder builder = ValueContactProtoList.newBuilder();
		InputStream stream = new ByteArrayInputStream(contactListString.getBytes(StandardCharsets.UTF_8));
		try {
			jsonFormat.merge(stream, builder);
		} catch (IOException e) {
			logger.error("Set contacts: Invalid contact list");
			e.printStackTrace();
			return jsonError("Invalid contact list", HttpStatus.BAD_REQUEST);
		}
		ValueContactProtoList protoList = builder.build();

		// Convert ValueContactProtoList to List<ValueContact> for RMI
		List<ValueContact> contacts = new ArrayList<ValueContact>();
		for (ValueContactProto proto : protoList.getContactList()) {
			contacts.add(ValueContact.fromValueContactProto(proto));
		}

		try {
			// Send request via RMI.
			Registry registry = LocateRegistry.getRegistry(agentIp.toString());
			ClientInterface stub = (ClientInterface) registry.lookup("Client");
			Boolean responseRMI = stub.setFallbackContacts(contacts);

			if (responseRMI) {
				logger.info("Set contacts: Contacts set.");
				return emptyOk;
			}

			logger.info("Set contacts: no RMI response.");
			return jsonError("No RMI response", HttpStatus.INTERNAL_SERVER_ERROR);

		} catch (Exception e) {
			logger.error("Set contacts:  Client RMI exception:");
			e.printStackTrace();
			return jsonError("Client RMI exception", HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	/**
	 * Set an attribute value of ZMI specified by pathname.
	 * @param requestBody String containing JSON request compatible with SetAttributeProto
	 * @return Response entity containing JSON response encoded as a string.
	 */
	@RequestMapping(value = "/agent/{agent_ip}/attribute",
			method = RequestMethod.POST,
			produces="application/json")
	public ResponseEntity<String> setAttributeValue(
			@PathVariable("agent_ip") String agentIp,
			@RequestBody String requestBody)  {

		logger.info("Set attribute: Set attribute called");

		if (requestBody == null) {
			logger.error("Set attribute: Bad request.");
			return jsonError("Bad request", HttpStatus.BAD_REQUEST);
		}

		// Convert JSON to SetAttributeProto
		SetAttributeProto.Builder setAttributeBuilder = SetAttributeProto.newBuilder();
		InputStream stream = new ByteArrayInputStream(requestBody.getBytes(StandardCharsets.UTF_8));
		try {
			jsonFormat.merge(stream, setAttributeBuilder);
		} catch (IOException e) {
			logger.error("Set contacts: Invalid set attribute request");
			e.printStackTrace();
			return jsonError("Invalid set attribute request", HttpStatus.BAD_REQUEST);
		}
		SetAttributeProto setAttributeProto = setAttributeBuilder.build();

		try {
			Registry registry = LocateRegistry.getRegistry(agentIp.toString());
			ClientInterface stub = (ClientInterface) registry.lookup("Client");
			// Send SetAttributeProto via RMI, encoded to wireformat byte array.
			// It could be changed if protobuf jar was in RMI codebase.
			SetAttributeClientResponse responseRMI = stub.setAttribute(setAttributeProto.toByteArray());

			switch (responseRMI.getResponse()) {
				case OK:
					logger.info("Set attribute: Attributes set.");
					return emptyOk;
				case BAD_PATH:
					logger.error("Set attribute: Bad path.");
					return jsonError("Bad path", HttpStatus.BAD_REQUEST);
				case NOT_KEPT:
					logger.error("Set attribute: Path Not Kept.");
					return jsonError("Path not kept", HttpStatus.BAD_REQUEST);
				case PARSE_ERROR:
					logger.error("Set attribute: Parse Error.");
					return jsonError("Parse error", HttpStatus.BAD_REQUEST);
				case NOT_LEAF:
					logger.error("Set attribute: path does not refer to a leaf.");
					return jsonError("Path does not refer to a leaf", HttpStatus.BAD_REQUEST);
				default:
					logger.error("Set attribute: Client Bad Request.");
					return jsonError("Client bad request", HttpStatus.BAD_REQUEST);
			}

		} catch (Exception e) {
			logger.error("Set contacts:  Client RMI exception:");
			e.printStackTrace();
			return jsonError("Client RMI exception", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


	/**
	 * Take query from request body, send it to Query Signer via HTTP.
	 * After Query Signer response, send that response to Agent via RMI.
	 * After that, respond to the original request.
	 * @param agentIp Destination agent's IP address.
	 * @param querySignRequest Query sign request.
	 * @return Response entity containing JSON response encoded as a string.
	 */
	@RequestMapping(value = "/agent/{agent_ip}/query",
			method = RequestMethod.POST,
			produces="application/json")
	public ResponseEntity<String> changeQuery(
			@PathVariable("agent_ip") String agentIp,
			@RequestBody QuerySignerSignRequest querySignRequest)  {

		logger.info("Change query: Change query called");
		String url = "http://" + ClientStartArguments.getQuerySignerUrl() + "/signer/query";

		String querySignerResponseJson = "";

		try {
			HttpClient client = HttpClientBuilder.create().build();
			HttpPost post = new HttpPost(url);
			StringEntity params = new StringEntity(mapper.writeValueAsString(querySignRequest));

			post.addHeader("Content-Type", "application/json");
			post.setEntity(params);
			HttpResponse response = client.execute(post);

			BufferedReader rd = new BufferedReader(
                    new InputStreamReader(response.getEntity().getContent()));

			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}

			querySignerResponseJson = result.toString();
			if (response.getStatusLine().getStatusCode() == 400){
				logger.error("Change query: query signer rejected query.");
				return jsonError("Query Signer rejected query", HttpStatus.BAD_REQUEST);
			}

		} catch (IOException e) {
			logger.error("Change query: query signer connection error.");
			e.printStackTrace();
			return jsonError("Query Signer connection error", HttpStatus.BAD_REQUEST);
		}

		QuerySignerSignResponse querySignerResponse;
		try {
			querySignerResponse = mapper.readValue(querySignerResponseJson, QuerySignerSignResponse.class);
		} catch (IOException e) {
			logger.error("Change query: Query Signer's response cannot be mapped into an object.");
			e.printStackTrace();
			return jsonError("Query Signer's response cannot be mapped into an object", HttpStatus.BAD_REQUEST);
		}

		QueryProto queryProto =
				QueryProto.newBuilder()
					.setName(querySignerResponse.getInitialRequest().getQueryName())
					.setContent(querySignerResponse.getInitialRequest().getQueryContent())
					.setTimestamp(querySignerResponse.getTimestamp())
					.setSignature(ByteString.copyFrom(querySignerResponse.getSignature()))
					.build();

		logger.info("Change query: Query signed");

		try {
			// Send request via RMI.
			Registry registry = LocateRegistry.getRegistry(agentIp.toString());
			ClientInterface stub = (ClientInterface) registry.lookup("Client");
			RegisterQueryAgentResponse rmiResponse = stub.registerQuery(queryProto.toByteArray());

			switch (rmiResponse.getResponse()) {
				case OK:
					logger.info("Change query: Query installed.");
					return emptyOk;
				case BAD_ALGORITHM:
					logger.error("Change query: Bad algorithm.");
					return jsonError("Bad algorithm", HttpStatus.BAD_REQUEST);
				case BAD_SIGNATURE:
					logger.error("Change query: Bad signature.");
					return jsonError("Bad signature", HttpStatus.BAD_REQUEST);
				case PARSE_ERROR:
					logger.error("Change query: Parse Error.");
					return jsonError("Parse error", HttpStatus.BAD_REQUEST);
				default:
					logger.error("Change query: Client Bad Request.");
					return jsonError("Client bad request", HttpStatus.BAD_REQUEST);
			}

		} catch (Exception e) {
			logger.error("Change query:  Client RMI exception:");
			e.printStackTrace();
			return jsonError("Client RMI exception", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Get all queries from given agent.
	 * @param agentIp Destination agent's IP address.
	 * @return Response entity containing JSON response encoded as a string, compatible with QueryProtoList.
	 */
	@RequestMapping(value = "/agent/{agent_ip}/query",
			method = RequestMethod.GET,
			produces="application/json")
	public ResponseEntity<String> getQueries(
			@PathVariable("agent_ip") String agentIp) {

		logger.info("Get queries: Get queries called");
		byte[] rmiResponse;

		try {
			// Send request via RMI.
			Registry registry = LocateRegistry.getRegistry(agentIp.toString());
			ClientInterface stub = (ClientInterface) registry.lookup("Client");
			rmiResponse = stub.getQueries();
			logger.info("Get queries: Message received.");
		} catch (Exception e) {
			logger.error("Get queries: Client RMI exception:");
			e.printStackTrace();
			return jsonError("Client RMI exception", HttpStatus.INTERNAL_SERVER_ERROR);
		}

		QueryProtoList queryProtoList;

		// Convert byte[] to QueryProtoList
		try {
			queryProtoList = QueryProtoList.parseFrom(rmiResponse);
		} catch (InvalidProtocolBufferException e) {
			logger.error("Error parsing QueryProto: " + e.getMessage());
			e.printStackTrace();
			return jsonError("QuerySigner response parsing error", HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<String>(jsonFormat.printToString(queryProtoList), HttpStatus.OK);
	}

}

