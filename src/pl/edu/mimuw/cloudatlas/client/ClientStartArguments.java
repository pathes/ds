package pl.edu.mimuw.cloudatlas.client;


public class ClientStartArguments {
	String querySignerUrl;
	private final static ClientStartArguments instance = new ClientStartArguments();

	public static ClientStartArguments getInstance() {
	    return instance;
	}

	public static String getQuerySignerUrl() {
		return instance.querySignerUrl;
	}

	public static void setQuerySignerUrl(String querySignerUrl) {
		instance.querySignerUrl = querySignerUrl;
	}
}


