package pl.edu.mimuw.cloudatlas.message;

import java.util.Collection;

import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.Message;

public interface MessageConsumer {
	public void run();

	public void stop();

	public void consume(Message msg);

	public Collection<String> getFinalConsumerTypes();
}
