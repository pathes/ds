package pl.edu.mimuw.cloudatlas.agent;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.google.protobuf.TextFormat;
import com.google.protobuf.TextFormat.ParseException;

import pl.edu.mimuw.cloudatlas.agent.CloudAtlasAgentConfiguration.AgentConfiguration;

public class ConfigValidator {

	public static void main(String[] args) {
		if (args.length < 1) {
			System.err.println("Required argument: config_path");
			System.exit(1);
		}

		try {
			readConfiguration(args[0]);
			System.out.println("Config is VALID");
		} catch (Exception e) {
			System.out.println("Config is INVALID");
			e.printStackTrace(System.out);
		}
	}

	private static void readConfiguration(String configPath) throws IOException, ParseException {
		String protoString;
		AgentConfiguration.Builder builder;

		try {
			protoString = new String(
				Files.readAllBytes(Paths.get(configPath)),
				StandardCharsets.UTF_8);
		} catch (IOException e) {
			throw e;
		}

		builder = AgentConfiguration.newBuilder();

		try {
			TextFormat.getParser().merge(protoString, builder);
			builder.build();
		} catch (ParseException e) {
			throw e;
		}
	}

}
