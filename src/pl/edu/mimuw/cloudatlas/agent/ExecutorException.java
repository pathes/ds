package pl.edu.mimuw.cloudatlas.agent;

@SuppressWarnings("serial")
public class ExecutorException extends RuntimeException {
	public ExecutorException(String message) {
		super(message);
	}
}
