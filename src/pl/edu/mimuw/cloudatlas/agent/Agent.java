package pl.edu.mimuw.cloudatlas.agent;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.protobuf.TextFormat;
import com.google.protobuf.TextFormat.ParseException;

import pl.edu.mimuw.cloudatlas.agent.CloudAtlasAgentConfiguration.AgentConfiguration;
import pl.edu.mimuw.cloudatlas.agent.CloudAtlasAgentConfiguration.AgentConfiguration.GossipLevelStrategy;
import pl.edu.mimuw.cloudatlas.agent.CloudAtlasAgentConfiguration.AgentConfiguration.GossipNeighborStrategy;
import pl.edu.mimuw.cloudatlas.model.CloudAtlasModels.QueryProto;
import pl.edu.mimuw.cloudatlas.model.CloudAtlasModels.ValueContactProto;
import pl.edu.mimuw.cloudatlas.model.PathName;
import pl.edu.mimuw.cloudatlas.model.ValueContact;
import pl.edu.mimuw.cloudatlas.model.ZMI;
import pl.edu.mimuw.cloudatlas.module.ClientModule;
import pl.edu.mimuw.cloudatlas.module.FetcherModule;
import pl.edu.mimuw.cloudatlas.module.QueryGossipModule;
import pl.edu.mimuw.cloudatlas.module.TimerModule;
import pl.edu.mimuw.cloudatlas.module.UdpCommunicationModule;
import pl.edu.mimuw.cloudatlas.module.ZmiGossipModule;
import pl.edu.mimuw.cloudatlas.module.ZmiModule;
import pl.edu.mimuw.cloudatlas.module.gossip.ExponentialLevelStrategy;
import pl.edu.mimuw.cloudatlas.module.gossip.GossipStrategy;
import pl.edu.mimuw.cloudatlas.module.gossip.LevelStrategy;
import pl.edu.mimuw.cloudatlas.module.gossip.LinearLevelStrategy;
import pl.edu.mimuw.cloudatlas.module.gossip.NeighborStrategy;
import pl.edu.mimuw.cloudatlas.module.gossip.RandomNeighborStrategy;
import pl.edu.mimuw.cloudatlas.module.gossip.RoundRobinNeighborStrategy;


public class Agent {
	protected Logger logger;
	protected AgentConfiguration config;
	protected Executor mainExecutor;

	String pathname;
	String querySignerPublicKeyFile;
	int zmiRecalculateIntervalMs;
	int zmiMaxAgeMs;
	int gossipIntervalMs;
	ZMI zmi;
	List<QueryProto> queries;
	GossipStrategy gossipStrategy;
	List<ValueContact> emergencyContacts;

	public static void main(String[] args) {
		if (args.length < 1) {
			System.err.println("Required argument: config_path");
			System.exit(1);
		}

		@SuppressWarnings("unused")
		Agent agent = new Agent(args[0]);
	}

	public Agent(String configPath) {
		this.logger = Logger.getLogger("agent");
		logger.setLevel(Level.INFO);
		config = readConfiguration(configPath);
		parseConfiguration();
		runExecutors();
	}

	private AgentConfiguration readConfiguration(String configPath) {
		String protoString;
		AgentConfiguration.Builder builder;

		try {
			protoString = new String(
				Files.readAllBytes(Paths.get(configPath)),
				StandardCharsets.UTF_8);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}

		builder = AgentConfiguration.newBuilder();

		try {
			TextFormat.getParser().merge(protoString, builder);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}

		return builder.build();
	}

	private void parseConfiguration() {
		// TODO(pathes): make use of emergency_contact list

		// Read values from configuration or use defaults
		if (config.hasZmiPathname()) {
			pathname = config.getZmiPathname();
		} else {
			logger.log(Level.SEVERE, "Missing zmi_pathname in configuration");
			return;
		}

		if (config.hasZmiRecalculateIntervalMs()) {
			zmiRecalculateIntervalMs = config.getZmiRecalculateIntervalMs();
		} else {
			logger.log(Level.WARNING, "Missing zmi_recalculate_interval_ms in configuration, using default: 5000");
			zmiRecalculateIntervalMs = 5000;
		}

		if (config.hasZmiMaxAgeMs()) {
			zmiMaxAgeMs = config.getZmiMaxAgeMs();
		} else {
			logger.log(Level.WARNING, "Missing zmi_max_age_ms in configuration, using default: 60000");
			zmiMaxAgeMs = 60000;
		}


		if (config.hasGossipIntervalMs()) {
			gossipIntervalMs = config.getGossipIntervalMs();
		} else {
			logger.log(Level.WARNING, "Missing gossip_interval_ms in configuration, using default: 10000");
			gossipIntervalMs = 10000;
		}

		GossipLevelStrategy levelStrategySetting;
		if (config.hasGossipLevelStrategy()) {
			levelStrategySetting = config.getGossipLevelStrategy();
		} else {
			logger.log(Level.WARNING, "Missing gossip_level_strategy in configuration, using default: LINEAR");
			levelStrategySetting = GossipLevelStrategy.LINEAR;
		}

		GossipNeighborStrategy neighborStrategySetting;
		if (config.hasGossipNeighborStrategy()) {
			neighborStrategySetting = config.getGossipNeighborStrategy();
		} else {
			logger.log(Level.WARNING, "Missing gossip_neighbor_strategy in configuration, using default: RANDOM");
			neighborStrategySetting = GossipNeighborStrategy.RANDOM;
		}

		if (config.hasInitialZmi()) {
			zmi = ZMI.fromProto(config.getInitialZmi());
		} else {
			logger.log(Level.WARNING, "Missing initial_zmi in configuration, using default: an empty ZMI");
			zmi = new ZMI(null);
		}

		queries = config.getQueryList();

		emergencyContacts = new ArrayList<ValueContact>();
		for (ValueContactProto contactProto : config.getEmergencyContactList()) {
			emergencyContacts.add(new ValueContact(
					contactProto.getPathName(),
					contactProto.getInetAddress()));
		}

		// Prepare gossip strategy
		LevelStrategy levelStrategy = null;
		switch (levelStrategySetting) {
			case EXPONENTIAL:
				levelStrategy = new ExponentialLevelStrategy(0.5);
				break;
			case LINEAR:
				levelStrategy = new LinearLevelStrategy();
				break;
			default:
				break;
		}
		NeighborStrategy neighborStrategy = null;
		switch (neighborStrategySetting) {
			case RANDOM:
				neighborStrategy = new RandomNeighborStrategy();
			case ROUND_ROBIN:
				neighborStrategy = new RoundRobinNeighborStrategy();
			default:
				break;
		}
		gossipStrategy = new GossipStrategy(
			levelStrategy,
			neighborStrategy);

		//Load query signer private Key
		if (config.hasQuerySignerPublicKeyFile()) {
			querySignerPublicKeyFile = config.getQuerySignerPublicKeyFile();
		} else {
			querySignerPublicKeyFile = "";
			logger.log(Level.WARNING, "Missing query_signer_public_key in configuration, using default: empty string");
		}
	}

	private void runExecutors() {
		// Default logging level
		Level level = Level.FINE;

		// Main executor that's binding the other ones
		mainExecutor = new Executor(level, null);

		// ZMI
		Executor zmiExecutor = new Executor(level, mainExecutor);
		@SuppressWarnings("unused")
		ZmiModule zmiModule = new ZmiModule(
				zmiExecutor, level, zmi, queries, zmiRecalculateIntervalMs, zmiMaxAgeMs);
		@SuppressWarnings("unused")
		ZmiGossipModule zmiGossipModule = new ZmiGossipModule(
				zmiExecutor, level, new PathName(pathname), zmi.clone(),
				gossipStrategy, gossipIntervalMs, emergencyContacts);
		@SuppressWarnings("unused")
		QueryGossipModule queryGossipModule = new QueryGossipModule(
				zmiExecutor, level, new PathName(pathname), zmi.clone(),
				gossipStrategy, gossipIntervalMs, emergencyContacts, queries);
		mainExecutor.registerExecutor(zmiExecutor);

		// RMI
		Executor rmiExecutor = new Executor(level, mainExecutor);
		@SuppressWarnings("unused")
		ClientModule clientModule = new ClientModule(
				rmiExecutor, level, zmi, emergencyContacts, queries, querySignerPublicKeyFile);
		@SuppressWarnings("unused")
		FetcherModule fetcherModule = new FetcherModule(
				rmiExecutor, level, pathname);
		mainExecutor.registerExecutor(rmiExecutor);

		// UDP
		Executor udpExecutor = new Executor(level, mainExecutor);
		int listenerPort = 9900;
		int senderPort = 9901;
		@SuppressWarnings("unused")
		UdpCommunicationModule udpModule = new UdpCommunicationModule(
				udpExecutor, level, pathname,
				listenerPort, listenerPort, senderPort);
		mainExecutor.registerExecutor(udpExecutor);

		// Timer
		Executor timerExecutor = new Executor(level, mainExecutor);
		@SuppressWarnings("unused")
		TimerModule timerModule = new TimerModule(
				timerExecutor, level);
		mainExecutor.registerExecutor(timerExecutor);

		// Run main executor in this thread
		mainExecutor.run();
	}

}
