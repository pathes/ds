package pl.edu.mimuw.cloudatlas.agent;

import java.util.Map;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Logger;
import java.util.logging.Level;

import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.Message;
import pl.edu.mimuw.cloudatlas.message.MessageConsumer;
import pl.edu.mimuw.cloudatlas.module.Module;

public class Executor implements Runnable, MessageConsumer {

	protected Map<String, MessageConsumer> consumerMap;
	protected BlockingQueue<Message> inputMessageQueue;
	protected List<Module> ownModules;
	protected List<Executor> subExecutors;
	protected boolean alive;
	protected Logger logger;
	protected Executor parent;

	public Executor(Level loggingLevel, Executor parent) {
		alive = false;
		this.parent = parent;
		consumerMap = new HashMap<String, MessageConsumer>();
		inputMessageQueue = new LinkedBlockingQueue<Message>();
		ownModules = new ArrayList<Module>();
		subExecutors = new ArrayList<Executor>();
		// Set up logger
		logger = Logger.getLogger("executor");
		logger.setLevel(loggingLevel);
	}

	public void registerModule(Module module) {
		String moduleType = module.getModuleType();
		if (consumerMap.containsKey(moduleType)) {
			throw new ExecutorException("Module " + moduleType + " already registered");
		}
		consumerMap.put(moduleType, module);
		ownModules.add(module);
	}

	public void registerExecutor(Executor executor) {
		Collection<String> moduleTypes = executor.getFinalConsumerTypes();
		for (String moduleType : moduleTypes) {
			if (consumerMap.containsKey(moduleType)) {
				throw new ExecutorException("Module " + moduleType + " already registered");
			}
			consumerMap.put(moduleType, executor);
		}
		subExecutors.add(executor);
	}

	@Override
	public void run() {
		logger.log(Level.INFO, "run");
		for (Module module : ownModules) {
			module.run();
		}
		for (Executor executor : subExecutors) {
			(new Thread(executor)).start();
		}
		alive = true;
		while (alive) {
			try {
				consume(inputMessageQueue.take());
			} catch (Exception e) {
				if (e instanceof InterruptedException) {
					alive = false;
					stop();
				} else {
					logger.log(
						Level.SEVERE,
						"Uncaught exception: " + e.getMessage(),
						e.getStackTrace());
					e.printStackTrace();
				}
			}
		}
	}

	public void enqueue(Message msg) {
		logger.log(Level.FINE, "\n" + msg.toString());
		inputMessageQueue.add(msg);
	}

	@Override
	public void consume(Message msg) {
		String moduleType = msg.getDestinationModule();
		if (consumerMap.containsKey(moduleType)) {
			MessageConsumer consumer = consumerMap.get(moduleType);
			consumer.consume(msg);
		} else if (parent != null) {
			parent.enqueue(msg);
		} else {
			throw new ExecutorException(
				"Module " + moduleType + " instance not present in executor\n"
				+ "Present modules: " + consumerMap.keySet());
		}
	}

	@Override
	public void stop() {
		logger.log(Level.INFO, "stop");
		alive = false;
		for (Module module : ownModules) {
			module.stop();
		}
		for (Executor executor : subExecutors) {
			executor.stop();
		}
	}

	@Override
	public Collection<String> getFinalConsumerTypes() {
		List<String> result = new ArrayList<String>();
		for (MessageConsumer consumer : consumerMap.values()) {
			result.addAll(consumer.getFinalConsumerTypes());
		}
		return result;
	}
}
