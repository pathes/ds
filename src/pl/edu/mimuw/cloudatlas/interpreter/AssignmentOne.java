package pl.edu.mimuw.cloudatlas.interpreter;

import java.util.AbstractMap.SimpleEntry;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Map.Entry;

import pl.edu.mimuw.cloudatlas.interpreter.query.Yylex;
import pl.edu.mimuw.cloudatlas.interpreter.query.parser;
import pl.edu.mimuw.cloudatlas.model.Attribute;
import pl.edu.mimuw.cloudatlas.model.PathName;
import pl.edu.mimuw.cloudatlas.model.Value;
import pl.edu.mimuw.cloudatlas.model.ValueString;
import pl.edu.mimuw.cloudatlas.model.ZMI;

public class AssignmentOne extends Main {

	private enum DfsStrategy {
		ALL_NODES, IGNORE_LEAVES
	}

	public static void main(String[] args) throws Exception {
		root = createTestHierarchy();
		List<ZMI> nonSingletonZones = postOrderDfs(root, DfsStrategy.IGNORE_LEAVES);
		List<ZMI> allZones = postOrderDfs(root, DfsStrategy.ALL_NODES);
		// The interpreter should be a command-line program that:
		// 1. reads a sequence of CloudAtlas queries from the standard input
		Scanner scanner = new Scanner(System.in);
		scanner.useDelimiter("\\n");
		// We need to preserve the order of queries, so we keep a list of them.
		ArrayList<Entry<Attribute, Value>> queryList = new ArrayList<Entry<Attribute, Value>>();
		while (scanner.hasNext()) {
			String queryString = scanner.next();
			// Ignore empty lines
			if (queryString.length() == 0) {
				continue;
			}
			Entry<Attribute, Value> newQuery = parseAssignmentQuery(queryString);
			queryList.add(newQuery);
		}
		scanner.close();
		// 2. installs the queries in all but singleton zones
		for (ZMI zmi : nonSingletonZones) {
			for (Entry<Attribute, Value> query : queryList) {
				installQuery(zmi, query);
			}
		}
		// 3. processes them on a fixed hierarchy
		for (ZMI zmi : nonSingletonZones) {
			for (Entry<Attribute, Value> query : queryList) {
				processQuery(zmi, query.getKey());
			}
		}
		// 4. prints the types and values of all attributes of all ZMIs to the standard output
		for (ZMI zmi : allZones) {
			printZmi(zmi);
		}
		// 5. uninstalls the queries
		for (ZMI zmi : nonSingletonZones) {
			for (Entry<Attribute, Value> query : queryList) {
				uninstallQuery(zmi, query.getKey());
			}
		}
	}

	private static List<ZMI> postOrderDfs(ZMI start, DfsStrategy strategy) {
		ArrayList<ZMI> result = new ArrayList<ZMI>();
		for (ZMI child : start.getSons()) {
			result.addAll(postOrderDfs(child, strategy));
		}
		switch (strategy) {
		case ALL_NODES:
			result.add(start);
			break;
		case IGNORE_LEAVES:
			if (start.getSons().size() > 0) {
				result.add(start);
			}
			break;
		}
		return result;
	}

	private static Entry<Attribute, Value> parseAssignmentQuery(String queryString) {
		String[] splitColon = queryString.split(":");
		if (splitColon.length != 2) {
			throw new IllegalArgumentException("Query should consist of attribute name and list of statements");
		}
		splitColon[0] = splitColon[0].trim();
		splitColon[1] = splitColon[1].trim();
		if (splitColon[0].charAt(0) != '&') {
			throw new IllegalArgumentException("Query attribute should start with '&'");
		}
		return new SimpleEntry<Attribute, Value>(new Attribute(splitColon[0]), new ValueString(splitColon[1]));
	}

	private static void installQuery(ZMI zmi, Entry<Attribute, Value> query) {
		zmi.getAttributes().add(query);
	}

	private static void uninstallQuery(ZMI zmi, Attribute queryAttribute) {
		zmi.getAttributes().remove(queryAttribute);
	}

	private static void processQuery(ZMI zmi, Attribute queryAttribute) throws Exception {
		String query = ((ValueString) zmi.getAttributes().get(queryAttribute)).getValue();
		Interpreter interpreter = new Interpreter(zmi);
		Yylex lex = new Yylex(new ByteArrayInputStream(query.getBytes()));
		try {
			List<QueryResult> resultList = interpreter.interpretProgram((new parser(lex)).pProgram());
			for (QueryResult result : resultList) {
				zmi.getAttributes().addOrChange(result.getName(), result.getValue());
			}
		} catch(InterpreterException exception) {}
	}

	private static void printZmi(ZMI zmi) {
		System.out.println(getPathName(zmi));
		for (Entry<Attribute, Value> entry : zmi.getAttributes()) {
			System.out.println("  " + entry.getKey() + " : " + entry.getValue().getType() + " = " + entry.getValue());
		}
		System.out.println();
	}
}