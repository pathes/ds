package pl.edu.mimuw.cloudatlas.interpreter;

import java.util.ArrayList;

import org.junit.*;
import static org.junit.Assert.*;

import pl.edu.mimuw.cloudatlas.model.TypePrimitive;
import pl.edu.mimuw.cloudatlas.model.Value;
import pl.edu.mimuw.cloudatlas.model.ValueInt;
import pl.edu.mimuw.cloudatlas.model.ValueList;

public class FunctionsTest {

	private ArrayList<Value> intValues;
	private ArrayList<Value> distinctIntValues;
	private Functions f;


	@Before
	public void setUp() {
		intValues = new ArrayList<Value>();
		intValues.add(new ValueInt(3l));
		intValues.add(new ValueInt(5l));
		intValues.add(new ValueInt(7l));
		intValues.add(new ValueInt(3l));
		intValues.add(new ValueInt(-15l));
		intValues.add(new ValueInt(57l));
		intValues.add(new ValueInt(3l));
		intValues.add(new ValueInt(7l));

		// Should be an effect of applying distinct() to `intValues`
		distinctIntValues = new ArrayList<Value>();
		distinctIntValues.add(new ValueInt(3l));
		distinctIntValues.add(new ValueInt(5l));
		distinctIntValues.add(new ValueInt(7l));
		distinctIntValues.add(new ValueInt(-15l));
		distinctIntValues.add(new ValueInt(57l));

		f = Functions.getInstance();
	}

	@Test
	public void testMin() {
		ResultList A = new ResultList(intValues);
		ArrayList<Result> args = new ArrayList<Result>();
		args.add(A);
		ResultSingle minA = (ResultSingle) f.evaluate("min", args);
		assertEquals(new ValueInt(-15l), minA.getValue());
	}

	@Test
	public void testMax() {
		ResultList A = new ResultList(intValues);
		ArrayList<Result> args = new ArrayList<Result>();
		args.add(A);
		ResultSingle maxA = (ResultSingle) f.evaluate("max", args);
		assertEquals(new ValueInt(57l), maxA.getValue());
	}

	@Test
	public void testDistinct() {
		ResultList A = new ResultList(intValues);
		ArrayList<Result> args = new ArrayList<Result>();
		args.add(A);
		ResultList distA = (ResultList) f.evaluate("distinct", args);
		assertEquals(new ValueList(distinctIntValues, TypePrimitive.INTEGER), distA.getValue());
	}

	@Test
	public void testCountWithNull() {
		ArrayList<Value> mixedValues = new ArrayList<Value>();
		mixedValues.add(new ValueInt(1l));
		mixedValues.add(new ValueInt(2l));
		mixedValues.add(new ValueInt(null));  // Shouldn't be counted
		mixedValues.add(new ValueInt(3l));

		ResultList A = new ResultList(mixedValues);
		ArrayList<Result> args = new ArrayList<Result>();
		args.add(A);
		ResultSingle countA = (ResultSingle) f.evaluate("count", args);
		assertEquals(new ValueInt(3l), countA.getValue());
	}
}