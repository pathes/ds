package pl.edu.mimuw.cloudatlas.interpreter;

import java.util.ArrayList;
import java.util.HashSet;

import org.junit.*;
import static org.junit.Assert.*;

import pl.edu.mimuw.cloudatlas.model.TypePrimitive;
import pl.edu.mimuw.cloudatlas.model.Value;
import pl.edu.mimuw.cloudatlas.model.ValueBoolean;
import pl.edu.mimuw.cloudatlas.model.ValueInt;
import pl.edu.mimuw.cloudatlas.model.ValueList;
import pl.edu.mimuw.cloudatlas.model.ValueSet;
import pl.edu.mimuw.cloudatlas.model.ValueString;

public class ResultListTest {

	private ArrayList<Value> stringValues;
	private ValueList stringValueList;
	private ValueSet stringValueSet;

	private ArrayList<Value> intValuesA;
	private ArrayList<Value> intValuesB;


	@Before
	public void setUp() {
		stringValues = new ArrayList<Value>();
		stringValues.add(new ValueString("foo"));
		stringValues.add(new ValueString("bar"));
		stringValues.add(new ValueString("spam"));
		stringValues.add(new ValueString("ham"));
		stringValues.add(new ValueString("eggs"));

		stringValueList = new ValueList(stringValues, TypePrimitive.STRING);
		stringValueSet = new ValueSet(new HashSet<Value>(stringValues), TypePrimitive.STRING);

		intValuesA = new ArrayList<Value>();
		intValuesA.add(new ValueInt(3l));
		intValuesA.add(new ValueInt(5l));
		intValuesA.add(new ValueInt(7l));

		intValuesB = new ArrayList<Value>();
		intValuesB.add(new ValueInt(10l));
		intValuesB.add(new ValueInt(20l));
		intValuesB.add(new ValueInt(30l));
	}

	@Test
	public void testFromList() {
		ResultList resultList = new ResultList(stringValues);
		assertEquals(5, resultList.getList().size());
		assertEquals(new ValueString("spam"), resultList.getList().get(2));
	}

	@Test
	public void testFromValueList() {
		ResultList resultList = new ResultList(stringValueList);
		assertEquals(5, resultList.getList().size());
		assertEquals(new ValueString("spam"), resultList.getList().get(2));
	}

	@Test
	public void testFromValueSet() {
		ResultList resultList = new ResultList(stringValueSet);
		assertEquals(5, resultList.getList().size());
		assertEquals(true, stringValueSet.contains(resultList.getList().get(2)));
	}

	@Test
	public void testAddInts() {
		ResultList A = new ResultList(intValuesA);
		ResultList B = new ResultList(intValuesB);
		ResultList C = (ResultList) A.addValue(B);
		assertEquals(new ValueInt(13l), C.getList().get(0));
		assertEquals(new ValueInt(25l), C.getList().get(1));
		assertEquals(new ValueInt(37l), C.getList().get(2));
	}

	@Test
	public void testDeepCompare() {
		ResultList A = new ResultList(intValuesA);
		ResultSingle B = new ResultSingle(new ValueInt(4l));
		ResultList C = (ResultList) A.isLowerThan(B);
		assertEquals(new ValueBoolean(true), C.getList().get(0));
		assertEquals(new ValueBoolean(false), C.getList().get(1));
		assertEquals(new ValueBoolean(false), C.getList().get(2));
	}
}