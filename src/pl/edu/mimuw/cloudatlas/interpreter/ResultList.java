package pl.edu.mimuw.cloudatlas.interpreter;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import pl.edu.mimuw.cloudatlas.model.Type;
import pl.edu.mimuw.cloudatlas.model.TypePrimitive;
import pl.edu.mimuw.cloudatlas.model.Value;
import pl.edu.mimuw.cloudatlas.model.ValueBoolean;
import pl.edu.mimuw.cloudatlas.model.ValueList;
import pl.edu.mimuw.cloudatlas.model.ValueNull;
import pl.edu.mimuw.cloudatlas.model.ValueSet;

class ResultList extends Result {
	private final List<Value> values;

	public ResultList(List<Value> values) {
		this.values = values;
	}

	public ResultList(ValueList valueList) {
		this.values = valueList.getValue();
	}

	public ResultList(ValueSet valueSet) {
		this.values = new ArrayList<Value>(valueSet.getValue());
	}

	private void AssertSameSize(ResultList other) {
		if (values.size() != other.values.size()) {
			throw new InvalidResultCountException();
		}
	}

	private static Random random = new Random();

	@Override
	protected Result binaryOperationTyped(BinaryOperation operation, Result right) {
		ArrayList<Value> resultValues = new ArrayList<Value>();
		if (right instanceof ResultSingle) {
			Value otherValue = ((ResultSingle) right).getValue();
			if (otherValue instanceof ValueNull) {
				// According to semantics this every operation with NULL returns NULL
				return new ResultSingle(otherValue);
			}
			for (Value myValue : values) {
				resultValues.add(operation.perform(myValue, otherValue));
			}
			return new ResultList(resultValues);
		} else if (right instanceof ResultList) {
			//NOTE(pawelk) zip is not supported operation
			AssertSameSize((ResultList) right);
			List<Value> otherValues = ((ResultList) right).values;
			for (int i = 0; i < values.size(); ++i) {
				resultValues.add(operation.perform(values.get(i), otherValues.get(i)));
			}
			return new ResultList(resultValues);
		}
		throw new UnsupportedOperationException("Result type not compatible.");
	}

	@Override
	public ResultList unaryOperation(UnaryOperation operation) {
		ArrayList<Value> resultValues = new ArrayList<Value>();
		for (Value myValue : values) {
			resultValues.add(operation.perform(myValue));
		}
		return new ResultList(resultValues);
	}

	@Override
	protected Result callMe(BinaryOperation operation, Result left) {
		return left.binaryOperationTyped(operation, this);
	}

	/*
	 * Necessary to print Values
	 */
	@Override
	public Value getValue() {
		return getList();
	}


	@Override
	public ValueList getList() {
		if (values.size() > 0) {
			return new ValueList(values, values.get(0).getType());
		} else {
			return new ValueList(new ArrayList<Value>(), TypePrimitive.NULL);
		}
	}

	@Override
	public ValueList getColumn() {
		// TODO(pathes): what's the difference between list and column?
		return getList();
	}

	@Override
	public ResultSingle aggregationOperation(AggregationOperation operation) {
		return new ResultSingle(operation.perform(getList()));
	}

	@Override
	public Result transformOperation(TransformOperation operation) {
		return new ResultList(operation.perform(getList()));
	}

	@Override
	public Result filterNulls() {
		ArrayList<Value> resultValues = new ArrayList<Value>();
		for (Value myValue : values) {
			if (!myValue.isNull()) {
				resultValues.add(myValue);
			}
		}
		return new ResultList(resultValues);
	}

	@Override
	public Result first(int size) {
		if (size < 1) {
			return new ResultSingle(new ValueList(new ArrayList<Value>(), TypePrimitive.NULL));
		}
		ArrayList<Value> resultValues = new ArrayList<Value>();
		if (values.size() < size) {
			resultValues.addAll(values);
		} else {
			for (int i = 0; i < size; ++i) {
				resultValues.add(values.get(i));
			}
		}
		if (resultValues.size() > 0) {
			return new ResultSingle(new ValueList(resultValues, resultValues.get(0).getType()));
		} else {
			return new ResultSingle(new ValueList(new ArrayList<Value>(), TypePrimitive.NULL));
		}
	}

	@Override
	public Result last(int size) {
		if (size < 1) {
			return new ResultSingle(new ValueList(new ArrayList<Value>(), TypePrimitive.NULL));
		}
		ArrayList<Value> resultValues = new ArrayList<Value>();

		int collectionSize = values.size();
		if (collectionSize < size) {
			resultValues.addAll(values);
		} else {
			for (int i = 0; i < size; ++i) {
				resultValues.add(values.get(collectionSize - size + i));
			}
		}
		if (resultValues.size() > 0) {
			return new ResultSingle(new ValueList(resultValues, resultValues.get(0).getType()));
		} else {
			return new ResultSingle(new ValueList(new ArrayList<Value>(), TypePrimitive.NULL));
		}
	}

	@Override
	public Result random(int size) {
		// http://stackoverflow.com/a/23720337/2595489
		if (size < 1) {
			return new ResultSingle(new ValueList(new ArrayList<Value>(), TypePrimitive.NULL));
		}
		int N = values.size();
		ArrayList<Value> resultValues = new ArrayList<Value>();
		if (values.size() < size) {
			resultValues.addAll(values);
		} else {
			for (int i = 0; i < N; ++i) {
				if (random.nextInt(N - i) < size) {
					resultValues.add(values.get(i));
					size--;
				}
			}
		}
		if (resultValues.size() > 0) {
			return new ResultSingle(new ValueList(resultValues, resultValues.get(0).getType()));
		} else {
			return new ResultSingle(new ValueList(new ArrayList<Value>(), TypePrimitive.NULL));
		}
	}

	@Override
	public Result convertTo(Type to) {
		ArrayList<Value> resultValues = new ArrayList<Value>();
		for (Value myValue : values) {
			resultValues.add(myValue.convertTo(to));
		}
		return new ResultList(resultValues);
	}

	@Override
	public ResultSingle isNull() {
		// TODO(pathes): it's rather true, but should it be this way?
		return new ResultSingle(new ValueBoolean(false));
	}

	@Override
	public Type getType() {
		if (values.size() > 0) {
			return values.get(0).getType();
		}
		return TypePrimitive.NULL;
	}

}