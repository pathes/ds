/**
 * Copyright (c) 2014, University of Warsaw
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted
 * provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 * conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package pl.edu.mimuw.cloudatlas.interpreter;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import pl.edu.mimuw.cloudatlas.model.Value;
import pl.edu.mimuw.cloudatlas.model.ValueList;
import pl.edu.mimuw.cloudatlas.model.ValueNull;
import pl.edu.mimuw.cloudatlas.model.ValueSet;

class Environment {
	private final TableRow row;
	private final Map<String, Integer> columns = new HashMap<String, Integer>();

	public Environment(TableRow row, List<String> columns) {
		this.row = row;
		int i = 0;
		for(String c : columns) {
			this.columns.put(c, i++);
		}
	}

	public Environment(Table table) {
		List<String> columns = table.getColumns();
		int i = 0;
		for(String c : columns) {
			this.columns.put(c, i++);
		}

		int numberOfAttributes = columns.size();
		ValueList[] finalRow = new ValueList[numberOfAttributes];

		Iterator<TableRow> tableIterator = table.iterator();

		if (!tableIterator.hasNext()){
			// so columns are empty to
			row = null;
			return;
		}

		//NOTE Determine types to create proper ValueList
		TableRow firstRow = tableIterator.next();
		for (i=0; i<numberOfAttributes; i++) {
			finalRow[i] = new ValueList(firstRow.getIth(i).getType());
		}

		for(TableRow row: table){
			for (i=0; i<numberOfAttributes; i++) {
				finalRow[i].add(row.getIth(i));
			}
		}
		this.row = new TableRow(finalRow);
	}

	public Result getIdent(String ident) {
		try {
			Value val = row.getIth(columns.get(ident));
			if (val instanceof ValueList) {
				return new ResultList((ValueList)val);
			}

			if (val instanceof ValueSet) {
				return new ResultList((ValueSet)val);
			}
			return new ResultSingle(val);
		} catch(NullPointerException exception) {
			return new ResultSingle(ValueNull.getInstance());
		}
	}
}
