package pl.edu.mimuw.cloudatlas.fetcher;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import pl.edu.mimuw.cloudatlas.RMIinterface.AttributesFetcherInterface;
import pl.edu.mimuw.cloudatlas.fetcher.creator.ValueCreator;
import pl.edu.mimuw.cloudatlas.fetcher.creator.ValueCreatorDouble;
import pl.edu.mimuw.cloudatlas.fetcher.creator.ValueCreatorInt;
import pl.edu.mimuw.cloudatlas.fetcher.creator.ValueCreatorSetOfStrings;
import pl.edu.mimuw.cloudatlas.fetcher.creator.ValueCreatorString;
import pl.edu.mimuw.cloudatlas.fetcher.creator.ValueCreatorTime;
import pl.edu.mimuw.cloudatlas.model.AttributesMap;
import pl.edu.mimuw.cloudatlas.model.CloudAtlasModels.ZmiProto;

public class AttributesFetcherClient {


	private static class TimerFetcherTask extends TimerTask {
		private String registryHostName, scriptsPath;
		private Logger logger;
		private static final ValueCreatorInt creatorInt = new ValueCreatorInt();
		/*
		 *  unfortunately it's java
		 *  http://stackoverflow.com/questions/6802483/how-to-directly-initialize-a-hashmap-in-a-literal-way
		 *  if value is not in the map, then it's called as int
		 */
		private static final HashMap <String, ValueCreator> attributeNameToValueCreatorMap;
		static {
			attributeNameToValueCreatorMap = new HashMap <String, ValueCreator>();
			attributeNameToValueCreatorMap.put("timestamp", new ValueCreatorTime());
			attributeNameToValueCreatorMap.put("cpu_load", new ValueCreatorDouble());
			attributeNameToValueCreatorMap.put("dns_names", new ValueCreatorSetOfStrings());
			attributeNameToValueCreatorMap.put("kernel_ver", new ValueCreatorString());
		}

		public TimerFetcherTask(String registryHostName, String scriptsPath) {
			this.registryHostName = registryHostName;
			this.scriptsPath = scriptsPath;
			this.logger = Logger.getLogger("fetcher");
			this.logger.setLevel(Level.INFO);
		}

		@Override
		/**
		 * Pick attributes values from shell and pass them to Agent via RMI.
		 *
		 */
		public void run() {
			try {
				// Pick values from shell.
				AttributesMap resultValues = new AttributesMap();
				String line, fileName, attributeName;
				String[] command;
				ArrayList<String> shellOutputList;

				// It's pretty strange, but File object might be directory.
				File scriptDirectory = new File(this.scriptsPath);
				for(File file : scriptDirectory.listFiles()) {
			        fileName = file.getName();
			        command = new String [] {this.scriptsPath+ "/"+ fileName,};
			        logger.log(Level.INFO, "run command: " + fileName);

			        Process process = Runtime.getRuntime().exec(command);
			        BufferedReader reader = new BufferedReader(new InputStreamReader(
			            process.getInputStream()));

			        shellOutputList = new ArrayList<String>();
			        while ((line = reader.readLine()) != null) {
			        	shellOutputList.add(line);
			        }

			        // Removes '.sh' from file name (rest of name is ZMI attribute name)
			        attributeName = fileName.substring(0, fileName.length()-3);

			        if (attributeNameToValueCreatorMap.containsKey(attributeName)) {
			        	resultValues.add(attributeName,
			        			attributeNameToValueCreatorMap.get(attributeName).
			        			convertStringListToValue(shellOutputList));
			        } else {
			        	resultValues.add(attributeName,
			        			creatorInt.convertStringListToValue(shellOutputList));
			        }
			    }
				ZmiProto.Builder resultBuilder = ZmiProto.newBuilder();
				resultBuilder.addAllAttributesMapEntries(resultValues.entryProtoList());

				// Send values via RMI.
				Registry registry = LocateRegistry.getRegistry(registryHostName);
				AttributesFetcherInterface stub = (AttributesFetcherInterface) registry.lookup("Fetcher");
				Boolean response = stub.setLowLevelAttributes(resultBuilder.build().toByteArray());
				if (response) {
					logger.log(Level.INFO, "Fetcher message send.");
				} else {
					logger.log(Level.SEVERE, "Fetcher failed.");
				}
			} catch (Exception e) {
				System.err.println("Fetcher exception:");
				e.printStackTrace();
			}
		}

	}

	public static void main(String[] args) {
		// Fetcher interval in miliseconds
		String registryHostName = args[0];
		int fetcherInterval = Integer.parseInt(args[1])*1000;
		String scriptsPath = args[2];
		Timer timer = new Timer();
		TimerFetcherTask timerTask = new TimerFetcherTask(registryHostName, scriptsPath);
		timer.schedule(timerTask, 0, fetcherInterval);

	}
}
