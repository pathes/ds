package pl.edu.mimuw.cloudatlas.fetcher.creator;

import java.util.ArrayList;

import pl.edu.mimuw.cloudatlas.model.Value;
import pl.edu.mimuw.cloudatlas.model.ValueInt;
import pl.edu.mimuw.cloudatlas.model.ValueNull;

public class ValueCreatorInt implements ValueCreator {

	@Override
	public Value convertStringListToValue(ArrayList<String> shellOutput) {
		if (shellOutput.size() != 1){
			return ValueNull.getInstance();
		}
		return new ValueInt(Long.parseLong(shellOutput.get(0).trim()));
	}

}
