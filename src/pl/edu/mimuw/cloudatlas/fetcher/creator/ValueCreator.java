package pl.edu.mimuw.cloudatlas.fetcher.creator;

import java.util.ArrayList;

import pl.edu.mimuw.cloudatlas.model.Value;

/**
 *
 * @author Paweł Kamiński
 * convertStringListToValue method converts shell output to attribute value.
 */
public interface ValueCreator {
	public Value convertStringListToValue(ArrayList<String> shellOutput);
}
