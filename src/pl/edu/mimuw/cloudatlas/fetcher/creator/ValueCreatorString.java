package pl.edu.mimuw.cloudatlas.fetcher.creator;

import java.util.ArrayList;

import pl.edu.mimuw.cloudatlas.model.Value;
import pl.edu.mimuw.cloudatlas.model.ValueString;

public class ValueCreatorString implements ValueCreator {
	@Override
	public Value convertStringListToValue(ArrayList<String> shellOutput) {
		if (shellOutput.size() != 1){
			 return new ValueString("");
		}
		return new ValueString(shellOutput.get(0));
	}
}
