package pl.edu.mimuw.cloudatlas.fetcher.creator;

import java.util.ArrayList;

import pl.edu.mimuw.cloudatlas.model.TypePrimitive;
import pl.edu.mimuw.cloudatlas.model.Value;
import pl.edu.mimuw.cloudatlas.model.ValueSet;
import pl.edu.mimuw.cloudatlas.model.ValueString;

public class ValueCreatorSetOfStrings implements ValueCreator {

	@Override
	public Value convertStringListToValue(ArrayList<String> shellOutput) {

		ValueSet returnedSet = new ValueSet(TypePrimitive.STRING);
		for (String line: shellOutput) {
			returnedSet.add(new ValueString(line));
		}
		return returnedSet;
	}

}
