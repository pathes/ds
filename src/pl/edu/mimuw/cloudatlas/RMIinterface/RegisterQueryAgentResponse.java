package pl.edu.mimuw.cloudatlas.RMIinterface;

import java.io.Serializable;

public class RegisterQueryAgentResponse implements Serializable {

	private static final long serialVersionUID = -4686991771592974369L;

	public enum AgentResponse {
		OK,
		PARSE_ERROR,
		BAD_SIGNATURE,
		BAD_ALGORITHM,
	}

	private AgentResponse response;

	public RegisterQueryAgentResponse (AgentResponse response) {
		this.response = response;
	}

	public AgentResponse getResponse() {
		return response;
	}
}
