package pl.edu.mimuw.cloudatlas.RMIinterface;

import java.io.Serializable;

public class GetAttributesFromZoneClientResponse implements Serializable{

	private static final long serialVersionUID = -3038187737602639701L;

	public enum ClientResponse {
		OK,
		BAD_PATH,
		NOT_KEPT,
	}


	private ClientResponse response;
	private byte[] zmiProtoBytes;

	public GetAttributesFromZoneClientResponse (ClientResponse response, byte[] zmiProtoBytes) {
		this.response = response;
		this.zmiProtoBytes = zmiProtoBytes;
	}

	public GetAttributesFromZoneClientResponse (ClientResponse response) {
		this.response = response;
	}

	public ClientResponse getResponse() {
		return response;
	}

	public byte[] getZmiProtoBytes() {
		return zmiProtoBytes;
	}
}
