package pl.edu.mimuw.cloudatlas.RMIinterface;
import java.rmi.Remote;
import java.rmi.RemoteException;


public interface AttributesFetcherInterface extends Remote {
	public Boolean setLowLevelAttributes(byte[] zmiProtoBytes) throws  RemoteException;
}
