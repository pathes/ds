package pl.edu.mimuw.cloudatlas.RMIinterface;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Set;

import pl.edu.mimuw.cloudatlas.model.ValueContact;

public interface ClientInterface extends Remote {
	public GetAttributesFromZoneClientResponse getAttributesFromZone(String zmiPath) throws RemoteException;
	public Set<String> getAllZonesPathnames() throws RemoteException;
	public List<ValueContact> getFallbackContacts() throws RemoteException;
	public Boolean setFallbackContacts(List<ValueContact> contacts) throws RemoteException;
	// TODO(pathes): change byte[] to SetAttributeProto when protobuf jar is in RMI codeBase.
	public SetAttributeClientResponse setAttribute(byte[] encodedSetAttributeProto) throws RemoteException;
	// TODO(pathes): change byte[] to QueryProto when protobuf jar is in RMI codeBase.
	public RegisterQueryAgentResponse registerQuery(byte[] encodedQueryProto) throws RemoteException;
	// TODO(pathes): change byte[] to QueryProtoList when protobuf jar is in RMI codeBase.
	public byte[] getQueries() throws RemoteException;
}
