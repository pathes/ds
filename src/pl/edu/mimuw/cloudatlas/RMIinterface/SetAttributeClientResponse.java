package pl.edu.mimuw.cloudatlas.RMIinterface;

import java.io.Serializable;

public class SetAttributeClientResponse implements Serializable{

	private static final long serialVersionUID = 2L;

	public enum ClientResponse {
		OK,
		BAD_PATH,
		NOT_KEPT,
		PARSE_ERROR,
		NOT_LEAF,
	}

	private ClientResponse response;

	public SetAttributeClientResponse (ClientResponse response) {
		this.response = response;
	}

	public ClientResponse getResponse() {
		return response;
	}


}
