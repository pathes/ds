package pl.edu.mimuw.cloudatlas.module;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import pl.edu.mimuw.cloudatlas.agent.Executor;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.Message;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.QueryGossipMessage;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.QueryUpdateRequestMessage;
import pl.edu.mimuw.cloudatlas.model.CloudAtlasModels.QueryProto;
import pl.edu.mimuw.cloudatlas.model.PathName;
import pl.edu.mimuw.cloudatlas.model.ValueContact;
import pl.edu.mimuw.cloudatlas.model.ZMI;
import pl.edu.mimuw.cloudatlas.module.gossip.GossipStrategy;

public class QueryGossipModule extends GossipModule {

	protected ArrayList<QueryProto> ownQueries;

	public QueryGossipModule(Executor executor, Level loggingLevel, PathName pathname,
			ZMI zmi, GossipStrategy gossipStrategy, int gossipIntervalMs,
			List<ValueContact> emergencyContacts,
			List<QueryProto> queries) {
		super(executor, loggingLevel, pathname, zmi, gossipStrategy, gossipIntervalMs, emergencyContacts);
		if (queries != null) {
			ownQueries = new ArrayList<QueryProto>(queries);
		} else {
			ownQueries = new ArrayList<QueryProto>();
		}
	}

	@Override
	public String getModuleType() {
		return "query_gossip";
	}

	@Override
	protected void startGossipWith(ValueContact target) {
		if (target == null) {
			logger.log(Level.SEVERE, "No contact to gossip with");
			return;
		}
		List<QueryProto> freshnessQueries = getFreshnessQueries();
		// Build ZMI gossip message
		Message requestMessage = buildGossipMessage(0, freshnessQueries, null);
		// Build communication message
		Message communicationMessage = buildCommunicationMessage(target.getAddress(), requestMessage);
		// Send
		executor.enqueue(communicationMessage);
	}

	@Override
	protected void handleQueryUpdateRequest(QueryUpdateRequestMessage queryUpdateRequestMessage) {
		tryUpdateQueries(queryUpdateRequestMessage.getQueryList());
		// Update in ZMI module
		Message updateRequest = buildUpdateRequestMessage(ownQueries, "zmi");
		executor.enqueue(updateRequest);
		// Update in agent module
		updateRequest = buildUpdateRequestMessage(ownQueries, "client");
		executor.enqueue(updateRequest);
	}

	@Override
	protected void handleQueryGossip(QueryGossipMessage queryGossipMessage, String sourceInetAddressString) {
		// Determine sender's InetAddress
		InetAddress senderAddress;
		try {
			senderAddress = InetAddress.getByName(sourceInetAddressString);
		} catch (UnknownHostException e) {
			logger.log(
				Level.SEVERE,
				"InetAddress resolving error while gossiping\n" + queryGossipMessage,
				e);
			return;
		}

		if (!queryGossipMessage.hasPhase()) {
			logger.severe("Missing gossip phase");
			return;
		}

		// The phase of upcoming message, if any
		int receivedPhase = queryGossipMessage.getPhase();

		// Try to update queries with received data
		if (receivedPhase > 0) {
			tryUpdateQueries(queryGossipMessage.getFullQueryList());
		}

		// Prepare for sending the reply
		int sendingPhase = receivedPhase + 1;
		// We allow only phases 0, 1, 2
		if (sendingPhase < 1 || sendingPhase > 2) {
			return;
		}

		List<QueryProto> responseFreshnessQueries;
		List<QueryProto> responseFullQueries;

		if (sendingPhase == 1) {
			// Phase 1: send freshness
			responseFreshnessQueries = getFreshnessQueries();
		} else {
			// Phase 2: no freshness required
			responseFreshnessQueries = Collections.<QueryProto>emptyList();
		}

		// Phase 1 and 2: send actual queries
		responseFullQueries = getFullQueriesFresherThan(queryGossipMessage.getFreshnessQueryList());

		// Build ZMI gossip message
		Message requestMessage = buildGossipMessage(
				sendingPhase,
				responseFreshnessQueries,
				responseFullQueries);
		// Build communication message
		Message communicationMessage = buildCommunicationMessage(senderAddress, requestMessage);
		// Send
		executor.enqueue(communicationMessage);
	}

	private List<QueryProto> getFreshnessQueries() {
		List<QueryProto> result = new ArrayList<QueryProto>();
		for (QueryProto query : ownQueries) {
			QueryProto stripedQuery = QueryProto.newBuilder(query)
					.clearContent()
					.clearSignature()
					.build();
			result.add(stripedQuery);
		}
		return result;
	}

	private List<QueryProto> getFullQueriesFresherThan(List<QueryProto> receivedFreshnessQueries) {
		Map<String, Long> freshnessMap = new HashMap<String, Long>();
		for (QueryProto receivedFreshnessQuery : receivedFreshnessQueries) {
			freshnessMap.put(receivedFreshnessQuery.getName(), receivedFreshnessQuery.getTimestamp());
		}
		List<QueryProto> result = new ArrayList<QueryProto>();
		for (QueryProto ownQuery : ownQueries) {
			if (
				!freshnessMap.containsKey(ownQuery.getName())
				|| freshnessMap.get(ownQuery.getName()) < ownQuery.getTimestamp()
			) {
				result.add(ownQuery);
			}
		}
		return result;
	}

	private void tryUpdateQueries(List<QueryProto> candidateQueries) {
		Map<String, QueryProto> candidateMap = new HashMap<String, QueryProto>();
		for (QueryProto candidateQuery : candidateQueries) {
			candidateMap.put(candidateQuery.getName(), candidateQuery);
		}
		List<QueryProto> newOwnQueries = new ArrayList<QueryProto>();
		// First, add the queries that existed previously - or their newer versions
		for (QueryProto ownQuery : ownQueries) {
			if (candidateMap.containsKey(ownQuery.getName())) {
				if (candidateMap.get(ownQuery.getName()).getTimestamp() > ownQuery.getTimestamp()) {
					newOwnQueries.add(candidateMap.get(ownQuery.getName()));
				} else {
					newOwnQueries.add(ownQuery);
				}
				// Remove candidate query
				candidateMap.remove(ownQuery.getName());
			} else {
				newOwnQueries.add(ownQuery);
			}
		}
		// Second, put queries that didn't exist before.
		// These are only remaining in candidateMap
		for (QueryProto newQuery : candidateMap.values()) {
			newOwnQueries.add(newQuery);
		}
		// Update locally
		ownQueries = new ArrayList<QueryProto>(newOwnQueries);
		// Update in ZMI module
		Message updateRequest = buildUpdateRequestMessage(newOwnQueries, "zmi");
		executor.enqueue(updateRequest);
		// Update in client module
		updateRequest = buildUpdateRequestMessage(newOwnQueries, "client");
		executor.enqueue(updateRequest);
	}

	private Message buildGossipMessage(
			int phase,
			List<QueryProto> freshnessQueries,
			List<QueryProto> fullQueries) {
		QueryGossipMessage.Builder builder =
			QueryGossipMessage.newBuilder()
				.setPhase(phase);
		if (freshnessQueries != null) {
			builder.addAllFreshnessQuery(freshnessQueries);
		}
		if (fullQueries != null) {
			builder.addAllFullQuery(fullQueries);
		}
		Message requestMessage =
			Message.newBuilder()
				.setSourceModule(getModuleType())
				.setDestinationModule(getModuleType())
				.setQueryGossipMessage(builder.build())
				.build();
		return requestMessage;
	}

	private Message buildUpdateRequestMessage(List<QueryProto> queries, String targetModule) {
		QueryUpdateRequestMessage requestMessageBody =
			QueryUpdateRequestMessage.newBuilder()
				.addAllQuery(queries)
				.build();
		Message requestMessage =
			Message.newBuilder()
				.setSourceModule(getModuleType())
				.setDestinationModule(targetModule)
				.setQueryUpdateRequestMessage(requestMessageBody)
				.build();
		return requestMessage;
	}

}
