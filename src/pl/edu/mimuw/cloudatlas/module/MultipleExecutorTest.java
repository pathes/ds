package pl.edu.mimuw.cloudatlas.module;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.logging.Level;

import org.junit.Before;
import org.junit.Test;

import pl.edu.mimuw.cloudatlas.agent.Executor;

public class MultipleExecutorTest extends TimerModuleTest {

	protected Executor timerOwner, pingerOwner;

	@Override
	@Before
	public void setUp() {
		results = new ArrayList<Integer>();
		// Set up executors
		executor = new Executor(Level.OFF, null);
		timerOwner = new Executor(Level.OFF, executor);
		pingerOwner = new Executor(Level.OFF, executor);
		// Set up modules
		timerModule = new TimerModule(timerOwner, Level.OFF);
		pingerModule = new PingerModule(pingerOwner, Level.OFF, this);
		// Register sub-executors
		executor.registerExecutor(timerOwner);
		executor.registerExecutor(pingerOwner);
	}

	@Override
	@Test
	public void testTimerWithPinger() throws InterruptedException {
		(new Thread(executor)).start();
		Thread.sleep(10);
		pingerModule.doPing(5);
		Thread.sleep(200);
		assertEquals(6, results.size());
		assertEquals(
			new ArrayList<Integer>(asList(5, 4, 3, 2, 1, 0)),
			results);
		executor.stop();
	}
}