package pl.edu.mimuw.cloudatlas.module;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.protobuf.InvalidProtocolBufferException;

import pl.edu.mimuw.cloudatlas.agent.Executor;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.CommunicationMessage;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.Message;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.TimerMessage;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.ZmiGossipMessage;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.ZmiRequestMessage;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.ZmiResponseMessage;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.ZmiResponseMessage.ZmiResponseStatus;
import pl.edu.mimuw.cloudatlas.model.CloudAtlasModels.ZmiProto;
import pl.edu.mimuw.cloudatlas.model.Value;
import pl.edu.mimuw.cloudatlas.model.ValueContact;
import pl.edu.mimuw.cloudatlas.model.ValueTime;
import pl.edu.mimuw.cloudatlas.model.ZMI;


abstract class UdpWrapper {
	protected boolean isChannelOpen;

	protected int port;
	protected DatagramChannel channel;
	protected ByteBuffer buffer;
	protected Logger logger;

	protected final static int DATAGRAM_MAX_BYTES = 65536;

	public UdpWrapper(int port, Logger logger) {
		this.port = port;
		this.logger = logger;
	}

	protected long timeMs() {
		return System.currentTimeMillis();
	}

	protected void openChannel() throws IOException {
		logger.log(Level.INFO, "Opening channel on port " + port);
		try {
			channel = DatagramChannel.open();
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Failed opening channel on port " + port + "\n" + e.getMessage());
			throw e;
		}
		try {
			channel.socket().bind(new InetSocketAddress(port));
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Failed binding socket to port " + port + "\n" + e.getMessage());
			throw e;
		}
		buffer = ByteBuffer.allocate(DATAGRAM_MAX_BYTES);
		buffer.clear();
		isChannelOpen = true;
	}

	protected void closeChannel() {
		logger.log(Level.INFO, "Closing channel on port " + port);
		try {
			channel.close();
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Failed closing channel on port " + port + "\n" + e.getMessage());
		}
		isChannelOpen = false;
	}
}


class UdpListener extends UdpWrapper implements Runnable {
	private boolean alive;
	private Executor executor;
	private long receiveTime;

	public UdpListener(int port, Executor executor, Logger logger) {
		super(port, logger);
		this.executor = executor;
	}

	@Override
	public void run() {
		InetSocketAddress senderSocketAddress = null;
		alive = true;
		isChannelOpen = false;
		while (alive) {
			try {
				if (!isChannelOpen) {
					openChannel();
				}
				senderSocketAddress = (InetSocketAddress) channel.receive(buffer);
				receiveTime = timeMs();
				parseBuffer(senderSocketAddress.getAddress());
			} catch (IOException e) {
				closeChannel();
			}
		}
		if (isChannelOpen) {
			closeChannel();
		}
	}

	public void stop() {
		alive = false;
	}

	private void parseBuffer(InetAddress senderAddress) throws InvalidProtocolBufferException {
		buffer.flip();
		// Read only the bytes that make the message
		byte[] protobufBytes = Arrays.copyOf(buffer.array(), buffer.remaining());
		try {
			// Parse message proto
			Message parsedMessage = Message.parseFrom(protobufBytes);
			// Add information on the source of message
			Message.Builder builder = Message.newBuilder(parsedMessage);
			builder.setSourceInetAddress(senderAddress.getCanonicalHostName());
			// Handle GTP if necessary
			if (parsedMessage.hasZmiGossipMessage()) {
				builder.setZmiGossipMessage(handleGtp(parsedMessage));
			}
			// Assumption: if we received a message, it's a message dedicated for us.
			// TODO(pathes): if we want to allow indirect message sending, this should be changed.
			executor.enqueue(builder.build());
		} catch (InvalidProtocolBufferException e) {
			logger.log(Level.SEVERE, "Could not parse message\n" + Arrays.toString(protobufBytes));
		} finally {
			buffer.clear();
		}
	}

	private ZmiGossipMessage handleGtp(Message parsedMessage) {
		List<ZmiProto> fullAttrZmis;
		// Offset for updating full-attribute ZMIs, where we have enough data for predictions to be accurate.
		long gtpOffset;
		// Offset for updating freshness ZMIs. Based only on two timestamps, it can be inaccurate.
		long offsetUpperBound;

		ZmiGossipMessage originalGossip = parsedMessage.getZmiGossipMessage();
		ZmiGossipMessage.Builder gossipBuilder =
			ZmiGossipMessage.newBuilder(originalGossip);

		if (originalGossip.getT1B() == 0) {
			long t1b = receiveTime;
			gossipBuilder.setT1B(t1b);
			offsetUpperBound = calculateOffsetUpperBound(
					gossipBuilder.getT1A(),
					t1b);
			// Update freshness ZMI
			gossipBuilder.clearFreshnessZmi();
			gossipBuilder.setFreshnessZmi(
				recalculateTimestamp(originalGossip.getFreshnessZmi(), offsetUpperBound));
		} else if (originalGossip.getT2A() == 0) {
			long t2a = receiveTime;
			gtpOffset = calculateGtpOffset(
					gossipBuilder.getT1A(),
					gossipBuilder.getT1B(),
					gossipBuilder.getT2B(),
					t2a);
			// Update freshness ZMI
			gossipBuilder.clearFreshnessZmi();
			gossipBuilder.setFreshnessZmi(
				recalculateTimestamp(originalGossip.getFreshnessZmi(), gtpOffset));
			// Update full-attribute ZMI
			fullAttrZmis = originalGossip.getFullAttributeZmiList();
			gossipBuilder.clearFullAttributeZmi();
			gossipBuilder.addAllFullAttributeZmi(
				recalculateTimestamps(fullAttrZmis, gtpOffset));
			gossipBuilder.setT2A(t2a);
		} else {
			long t3b = receiveTime;
			gtpOffset = calculateGtpOffset(
				gossipBuilder.getT2B(),
				gossipBuilder.getT2A(),
				gossipBuilder.getT3A(),
				t3b);
			// Update full-attribute ZMI
			fullAttrZmis = originalGossip.getFullAttributeZmiList();
			gossipBuilder.clearFullAttributeZmi();
			gossipBuilder.addAllFullAttributeZmi(
				recalculateTimestamps(fullAttrZmis, gtpOffset));
			// No need to set T3B as it's last gossip message
		}
		return gossipBuilder.build();
	}

	/**
	 * Calculate offset according to GTP formula.
	 * @param firstSendMs Timestamp of sending the first message (A -> B) in ms.
	 * @param firstReceiveMs Timestamp of receiving the first message (A -> B) in ms.
	 * @param secondSendMs Timestamp of sending the second message (A <- B) in ms.
	 * @param secondReceiveMs Timestamp of receiving the second message (A <- B) in ms.
	 * @return Offset in ms.
	 */
	private long calculateGtpOffset(long firstSendMs, long firstReceiveMs, long secondSendMs, long secondReceiveMs) {
		long roundTripDelayMs = (firstReceiveMs - firstSendMs) + (secondReceiveMs - secondSendMs);
		long offsetMs = secondSendMs - secondReceiveMs + roundTripDelayMs / 2;
		return offsetMs;
	}

	/**
	 * Calculate "offset upper-bound" basing only on two timestamps.
	 * Intended to use for updating freshness ZMIs.
	 * @param sendMs Timestamp of sending the message in ms.
	 * @param receiveMs Timestamp of receiving the message in ms.
	 * @return Offset in ms.
	 */
	private long calculateOffsetUpperBound(long sendMs, long receiveMs) {
		long timestampDifferenceMs = receiveMs - sendMs;
		final long assumedUdpDelayMs = 100L;
		return timestampDifferenceMs + assumedUdpDelayMs;
	}

	private List<ZmiProto> recalculateTimestamps(List<ZmiProto> zmiProtos, long offset) {
		List<ZmiProto> result = new ArrayList<ZmiProto>();
		for (ZmiProto zmiProto : zmiProtos) {
			result.add(recalculateTimestamp(zmiProto, offset));
		}
		return result;
	}

	private ZmiProto recalculateTimestamp(ZmiProto zmiProto, long offset) {
		ZMI zmi = ZMI.fromProto(zmiProto);
		recalculateTimestampHelper(zmi, offset);
		return zmi.toProto(null, -1);
	}

	private void recalculateTimestampHelper(ZMI zmi, long offset) {
		Value timestampValue = zmi.getAttributes().getOrNull("timestamp");
		if (timestampValue == null) {
			logger.severe("Cannot recalculate timestamp of ZMI: " + zmi);
		} else {
			long timestamp = ((ValueTime) timestampValue).getValue();
			zmi.getAttributes().addOrChange("timestamp", new ValueTime(timestamp - offset));
		}
		for (ZMI child : zmi.getSons()) {
			recalculateTimestampHelper(child, offset);
		}
	}
}

class UdpSender extends UdpWrapper {

	public UdpSender(int port, Logger logger) {
		super(port, logger);
	}

	public int sendBytes(byte[] bytes, InetSocketAddress address) {
		int bytesSent = 0;
		try {
			if (!isChannelOpen) {
				openChannel();
			}
			buffer.clear();
			buffer.put(bytes);
			buffer.flip();
			bytesSent = channel.send(buffer, address);
		} catch (IOException e) {
			closeChannel();
		}
		return bytesSent;
	}

	public int sendMessage(Message message, InetSocketAddress address) {
		// Handle GTP if necessary
		if (message.hasZmiGossipMessage()) {
			ZmiGossipMessage originalGossip = message.getZmiGossipMessage();
			ZmiGossipMessage.Builder builder =
				ZmiGossipMessage.newBuilder(originalGossip);
			if (originalGossip.getT1A() == 0) {
				builder.setT1A(timeMs());
			} else if (originalGossip.getT2B() == 0) {
				builder.setT2B(timeMs());
			} else if (originalGossip.getT3A() == 0) {
				builder.setT3A(timeMs());
			}
			Message newMessage =
				Message.newBuilder(message)
					.setZmiGossipMessage(builder.build())
					.build();
			return sendBytes(newMessage.toByteArray(), address);
		}
		return sendBytes(message.toByteArray(), address);
	}

	public void stop() {
		if (isChannelOpen) {
			closeChannel();
		}
	}
}

public class UdpCommunicationModule extends Module {

	protected Thread listenerThread;
	protected UdpListener udpListener;
	protected UdpSender udpSender;
	protected String agentId;
	protected Map<String, InetAddress> contacts;
	protected int listenerPort;
	protected int remoteListenerPort;
	protected int senderPort;

	protected final static int ZMI_CONTACT_DELAY_MS = 100;

	public UdpCommunicationModule(
			Executor executor, Level loggingLevel, String agentId,
			int listenerPort, int remoteListenerPort, int senderPort) {
		super(executor, loggingLevel);
		this.agentId = agentId;
		this.contacts = new HashMap<String, InetAddress>();
		this.listenerPort = listenerPort;
		this.remoteListenerPort = remoteListenerPort;
		this.senderPort = senderPort;
	}

	@Override
	public String getModuleType() {
		return "udp_communication";
	}

	@Override
	public void run() {
		udpListener = new UdpListener(listenerPort, executor, logger);
		udpSender = new UdpSender(senderPort, logger);
		listenerThread = new Thread(udpListener);
		listenerThread.start();
	}

	@Override
	public void stop() {
		udpSender.stop();
		udpListener.stop();
	}

	@Override
	protected void handleCommunication(CommunicationMessage communicationMessage) {
		String commPathname = communicationMessage.getAgentId();
		String commInetAddressString = communicationMessage.getInetAddress();
		InetAddress targetAddress = null;

		// If neither pathname nor InetAddress are set
		if (commPathname == null && commInetAddressString == null) {
			logger.log(Level.SEVERE, "Communication message without recipient\n" + communicationMessage);
			return;
		}

		// It can be a loopback message for us
		if (commPathname.equals(agentId)) {
			executor.enqueue(communicationMessage.getSubmessage());
			return;
		}

		logger.fine("Trying to send message\n" + communicationMessage);

		// If the pathname is set, we can retrieve InetAddress instance from the map.
		if (commPathname != null && contacts.containsKey(commPathname)) {
			// If the InetAddress is mapped, retrieve it
			targetAddress = contacts.get(commPathname);
		} else {
			// If the InetAddress isn't mapped, we should try recreating it from string
			if (commInetAddressString != null) {
				try {
					targetAddress = InetAddress.getByName(commInetAddressString);
				} catch (UnknownHostException e) {
					logger.log(
						Level.SEVERE,
						"InetAddress resolving error while sending message\n" + communicationMessage,
						e);
					return;
				}
			} else {
				// We don't have InetAddress in map and it can't be recreated from string,
				// so we ask ZMI if it has some new contacts.
				askForContacts();
				delayMessage(communicationMessage);
				return;
			}
		}

		InetSocketAddress targetSocketAddress = new InetSocketAddress(targetAddress, remoteListenerPort);
		udpSender.sendMessage(communicationMessage.getSubmessage(), targetSocketAddress);
	}

	private void askForContacts() {
		logger.log(Level.INFO, "Retrieving contacts from ZMI");
		// Prepare ZMI requesting message
		ZmiRequestMessage messageBody =
			ZmiRequestMessage.newBuilder()
				// Ask for contacts of itself
				.setPathname(agentId)
				.addAttribute("contacts")
				.build();
		Message message =
			Message.newBuilder()
				.setSourceModule(getModuleType())
				.setDestinationModule("zmi")
				.setZmiRequestMessage(messageBody)
				.build();
		// Enqueue the message
		this.executor.enqueue(message);
	}

	@Override
	protected void handleZmiResponse(ZmiResponseMessage zmiResponseMessage) {
		if (zmiResponseMessage.getStatus() == ZmiResponseStatus.FAIL) {
			logger.log(Level.WARNING, "Failed to retrieve contacts");
			return;
		}
		for (ValueContact value : ZMI.fromProto(zmiResponseMessage.getZmi()).getContacts()) {
			contacts.put(value.getName().toString(), value.getAddress());
		}
	}

	private void delayMessage(CommunicationMessage communicationMessage) {
		logger.info("Delaying message");
		logger.fine("\n" + communicationMessage.toString());
		// Prepare wrapped communication message
		Message wrappedMessage =
			Message.newBuilder()
				.setDestinationModule(getModuleType())
				.setCommunicationMessage(communicationMessage)
				.build();
		// Prepare wrapping timer message
		TimerMessage wrappingMessageBody =
			TimerMessage.newBuilder()
				.setMilliseconds(ZMI_CONTACT_DELAY_MS)
				.setSubmessage(wrappedMessage)
				.build();
		Message wrappingMessage =
			Message.newBuilder()
				.setSourceModule(getModuleType())
				.setDestinationModule("timer")
				.setTimerMessage(wrappingMessageBody)
				.build();
		// Enqueue the message
		this.executor.enqueue(wrappingMessage);
	}

}
