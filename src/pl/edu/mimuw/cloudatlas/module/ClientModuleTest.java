package pl.edu.mimuw.cloudatlas.module;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Level;

import org.junit.Test;

import pl.edu.mimuw.cloudatlas.RMIinterface.GetAttributesFromZoneClientResponse;
import pl.edu.mimuw.cloudatlas.agent.Executor;
import pl.edu.mimuw.cloudatlas.model.AttributesMap;
import pl.edu.mimuw.cloudatlas.model.TypePrimitive;
import pl.edu.mimuw.cloudatlas.model.Value;
import pl.edu.mimuw.cloudatlas.model.ValueDouble;
import pl.edu.mimuw.cloudatlas.model.ValueInt;
import pl.edu.mimuw.cloudatlas.model.ValueList;
import pl.edu.mimuw.cloudatlas.model.ValueString;
import pl.edu.mimuw.cloudatlas.model.ValueTime;
import pl.edu.mimuw.cloudatlas.model.ZMI;
import pl.edu.mimuw.cloudatlas.model.CloudAtlasModels.ZmiProto;


class AccessibleClientModule extends ClientModule {

	public AccessibleClientModule(Executor executor, Level loggingLevel,  String publicKeyQuerySignerString) {
		super(executor, loggingLevel, null, null, null, "");
		// Faster ZMI updates
		this.refreshZMIIntervalMs = 200;
	}

	public ClientServer getServer() {
		return serverClientObject;
	}

	@Override
	public void run() {
		// Do nothing with rmiregistry, only set the timer up.
		schedulePing(refreshZMIIntervalMs);
	}
}

class ClientModuleHelper {
	private Executor executor;
	@SuppressWarnings("unused")
	private TimerModule timerModule;
	@SuppressWarnings("unused")
	private UdpCommunicationModule udpCommunicationModule;
	private ZmiModule zmiModule;
	private ZmiHelperModule zmiHelperModule;
	private AccessibleClientModule clientModule;
	private CountDownLatch latch;

	public ClientModuleHelper(Level logLevel, ZMI root)
			throws ParseException, IOException {
		latch = new CountDownLatch(1);
		executor = new Executor(logLevel, null);
		// Timer module is required for communication retry
		timerModule = new TimerModule(executor, logLevel);
		zmiModule = new ZmiModule(
				executor, logLevel, root,
				null, 100000, 60000);
		zmiHelperModule = new ZmiHelperModule(executor, logLevel);
		clientModule = new AccessibleClientModule(executor, logLevel, "");
	}

	public CountDownLatch getLatch() {
		return latch;
	}

	public Executor getExecutor() {
		return executor;
	}

	public ZmiModule getZmiModule() {
		return zmiModule;
	}

	public ZmiHelperModule getZmiHelperModule() {
		return zmiHelperModule;
	}

	public AccessibleClientModule getClientModule() {
		return clientModule;
	}
}


public class ClientModuleTest {

	private ZMI hierarchyHelper(ZMI parent, String name, String owner) {
		ZMI zmi = new ZMI(parent);
		if (parent != null) {
			parent.addSon(zmi);
		}
		zmi.getAttributes().add("name", new ValueString(name));
		zmi.getAttributes().add("owner", new ValueString(owner));
		zmi.getAttributes().add("timestamp", new ValueTime(0L));
		zmi.getAttributes().add("x", new ValueInt(0L));
		return zmi;
	}

	/**
	 * Simple ZMI hierarchy for testing
	 */
	private ZMI createSmallHierarchy() throws ParseException, UnknownHostException {
		ZMI root = hierarchyHelper(null, "", "/foo");
		hierarchyHelper(root, "foo", "/foo");
		hierarchyHelper(root, "bar", "/bar");
		return root;
	}

	@Test
	public void testGetAttributesFromZone() throws UnknownHostException, ParseException, IOException, InterruptedException {
		ClientModuleHelper helper = new ClientModuleHelper(Level.OFF, createSmallHierarchy());
		// Update previously existing values in /foo
		AttributesMap attributesMap = new AttributesMap();
		attributesMap.add("x", new ValueInt(42L));
		attributesMap.add("timestamp", new ValueTime(13000L));
		// Create new values
		attributesMap.add("newValue", new ValueDouble(137.0));
		attributesMap.add("quux", new ValueList(Arrays.<Value>asList(new ValueString("aaa"), new ValueString("bbb")), TypePrimitive.STRING));
		// Commit
		helper.getZmiHelperModule().setAttributes("/foo", attributesMap.entryProtoList());
		// Run thread
		(new Thread(helper.getExecutor())).start();
		Thread.sleep(500);
		ClientModule.ClientServer server = helper.getClientModule().getServer();
		GetAttributesFromZoneClientResponse response = server.getAttributesFromZone("/foo");
		assertEquals(
				GetAttributesFromZoneClientResponse.ClientResponse.OK,
				response.getResponse());
		byte [] responseBytes = response.getZmiProtoBytes();
		ZmiProto responseProto = ZmiProto.parseFrom(responseBytes);
		ZMI responseZmi = ZMI.fromProto(responseProto);
		assertEquals(42L, ((ValueInt) responseZmi.getAttributes().get("x")).getValue().longValue());
		assertEquals(13000L, ((ValueTime) responseZmi.getAttributes().get("timestamp")).getValue().longValue());
		assertEquals(137.0, ((ValueDouble) responseZmi.getAttributes().get("newValue")).getValue().doubleValue(), 1E-10);
		ValueList quuxList = (ValueList) responseZmi.getAttributes().get("quux");
		assertEquals("aaa", ((ValueString) quuxList.get(0)).getValue());
		assertEquals("bbb", ((ValueString) quuxList.get(1)).getValue());
		helper.getExecutor().stop();
	}

}
