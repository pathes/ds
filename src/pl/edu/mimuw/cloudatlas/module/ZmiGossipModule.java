package pl.edu.mimuw.cloudatlas.module;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import pl.edu.mimuw.cloudatlas.agent.Executor;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.Message;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.ZmiGossipMessage;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.ZmiUpdateRequestMessage;
import pl.edu.mimuw.cloudatlas.model.CloudAtlasModels.AttributesMapEntryProto;
import pl.edu.mimuw.cloudatlas.model.CloudAtlasModels.ZmiProto;
import pl.edu.mimuw.cloudatlas.model.PathName;
import pl.edu.mimuw.cloudatlas.model.TreeTraversalException;
import pl.edu.mimuw.cloudatlas.model.Type;
import pl.edu.mimuw.cloudatlas.model.TypeCollection;
import pl.edu.mimuw.cloudatlas.model.TypePrimitive;
import pl.edu.mimuw.cloudatlas.model.Value;
import pl.edu.mimuw.cloudatlas.model.ValueContact;
import pl.edu.mimuw.cloudatlas.model.ValueList;
import pl.edu.mimuw.cloudatlas.model.ValueSet;
import pl.edu.mimuw.cloudatlas.model.ValueString;
import pl.edu.mimuw.cloudatlas.model.ValueTime;
import pl.edu.mimuw.cloudatlas.model.ZMI;
import pl.edu.mimuw.cloudatlas.module.gossip.GossipStrategy;

public class ZmiGossipModule extends GossipModule {

	private static final String PATHNAME_KEY = "pathname__";

	public ZmiGossipModule(
			Executor executor, Level loggingLevel, PathName pathname,
			ZMI zmi, GossipStrategy gossipStrategy, int gossipIntervalMs,
			List<ValueContact> emergencyContacts) {
		super(executor, loggingLevel, pathname, zmi, gossipStrategy, gossipIntervalMs, emergencyContacts);
	}

	@Override
	public String getModuleType() {
		return "zmi_gossip";
	}

	@Override
	protected List<String> requiredAttributeList() {
		// An empty list means that we require all attributes.
		// All attributes aren't necessary for determining freshness,
		// but they are necessary for exchanging actual data.
		return new ArrayList<String>();
	}

	/**
	 * Gossip with one particular agent.
	 * @param target ValueContact representing gossip target agent.
	 */
	@Override
	protected void startGossipWith(ValueContact target) {
		if (target == null) {
			logger.log(Level.SEVERE, "No contact to gossip with");
			return;
		}
		ZmiProto freshnessZmi = getFreshnessZmi(target.getName());
		// Build ZMI gossip message
		Message requestMessage = buildGossipMessage(freshnessZmi, new ArrayList<ZmiProto>(), null);
		// Build communication message
		Message communicationMessage = buildCommunicationMessage(target.getAddress(), requestMessage);
		// Send
		executor.enqueue(communicationMessage);
	}

	/**
	 * Handle ZmiGossip message.
	 * Communication goes as follows:
	 *
	 *       Agent A                                     Agent B
	 *                          freshness ZMI
	 * startGossipWith(B) >------------------------>  handleZmiGossip
	 *
	 *                          freshness ZMI
	 *                      full-attribute ZMI list
	 *   handleZmiGossip  <------------------------<
	 *
	 *
	 *                      full-attribute ZMI list
	 *                    >------------------------>  handleZmiGossip
	 *
	 * @param zmiGossipMessage Message received from the other agent.
	 */
	@Override
	protected void handleZmiGossip(ZmiGossipMessage zmiGossipMessage, String sourceInetAddressString) {
		PathName senderPathname;
		// Determine sender's pathname
		if (zmiGossipMessage.hasSenderPathname()) {
			senderPathname = new PathName(zmiGossipMessage.getSenderPathname());
		} else {
			logger.log(Level.WARNING, "Gossiping with anonymous agent");
			senderPathname = new PathName("/");
		}

		// Determine sender's InetAddress
		InetAddress senderAddress;
		try {
			senderAddress = InetAddress.getByName(sourceInetAddressString);
		} catch (UnknownHostException e) {
			logger.log(
				Level.SEVERE,
				"InetAddress resolving error while gossiping\n" + zmiGossipMessage,
				e);
			return;
		}

		// Prepare full-attribute ZMI list for response
		List<ZmiProto> fullAttributeZmiList;
		if (zmiGossipMessage.hasFreshnessZmi()) {
			// If there is freshness ZMI sent, sender wants us to send back ZMIs with full attributes.
			fullAttributeZmiList = getFresherZmiThan(ZMI.fromProto(zmiGossipMessage.getFreshnessZmi()));
		} else {
			fullAttributeZmiList = new ArrayList<ZmiProto>();
		}

		// Prepare freshness ZMI for response
		ZmiProto freshnessZmi = null;
		if (zmiGossipMessage.getFullAttributeZmiCount() == 0) {
			// If there are no full-attribute ZMI sent, generate freshness ZMI.
			freshnessZmi = getFreshnessZmi(senderPathname);
		} else {
			// Otherwise, we have to update our ZMIs
			for (ZmiProto candidateZmiProto : zmiGossipMessage.getFullAttributeZmiList()) {
				tryUpdateZmi(candidateZmiProto);
			}
		}

		// Send information if there is any needed
		if (fullAttributeZmiList.size() > 0 || freshnessZmi != null) {
			// Build ZMI gossip message
			Message requestMessage = buildGossipMessage(freshnessZmi, fullAttributeZmiList, zmiGossipMessage);
			// Build communication message
			Message communicationMessage = buildCommunicationMessage(senderAddress, requestMessage);
			// Send
			executor.enqueue(communicationMessage);
		}
	}

	/**
	 * @param targetPathname Pathname of target agent.
	 * @return ZMI subtree common to our agent and the gossip target agent, limited to a few attributes.
	 */
	private ZmiProto getFreshnessZmi(PathName targetPathname) {
		PathName lca = pathname.lowestCommonAncestor(targetPathname);
		// +2 - account for the root and for the children of LCA
		int depthLimit = lca.getComponents().size() + 2;
		// Get the tree with limited depth and limited attributes. We need only name and timestamp.
		return rootZmi.toProto(new HashSet<String>(Arrays.asList("name", "timestamp")), depthLimit);
	}

	/**
	 * Get all our ZMI that are fresher than declared by received freshness ZMI.
	 * For convenience, store them as a list of single-level (childless) ZmiProto.
	 * @param freshnessZmi Received freshness ZMI.
	 * @return List of single-level ZmiProto.
	 */
	private List<ZmiProto> getFresherZmiThan(ZMI freshnessZmi) {
		List<ZmiProto> result = new ArrayList<ZmiProto>();
		getFresherZmiThanHelper(result, new PathName("/"), rootZmi, freshnessZmi);
		return result;
	}

	/**
	 *
	 * @param acc Accumulator ZmiProto list.
	 * @param prefixPathname Pathname prefix determined previously by caller.
	 * @param localZmi Current node in local ZMI tree.
	 * @param freshnessZmi Current node in received freshness ZMI tree.
	 */
	private void getFresherZmiThanHelper(List<ZmiProto> acc, PathName prefixPathname, ZMI localZmi, ZMI freshnessZmi) {
		Map<String, ZMI> freshnessChildren = new HashMap<String, ZMI>();
		String childName;
		ZMI localZmiCopy, foundFreshnessChild;
		List<String> childPathnameComponents;
		PathName childPathname;
		boolean addResult;

		if (freshnessZmi == null) {
			// If we have no-one to compare with, just add localZmi to results.
			addResult = true;
		} else {
			// If we can compare, we want to add localZmi only if it's fresher.
			Value freshnessValue = freshnessZmi.getAttributes().getOrNull("timestamp");
			long freshnessTimestamp = 0L;
			if (freshnessValue != null && freshnessValue instanceof ValueTime) {
				freshnessTimestamp = ((ValueTime) freshnessValue).getValue();
			} else {
				logger.warning("Received freshness ZMI without timestamp");
			}

			Value localValue = localZmi.getAttributes().getOrNull("timestamp");
			long localTimestamp = 0L;
			if (localValue != null && localValue instanceof ValueTime) {
				localTimestamp = ((ValueTime) localValue).getValue();
			} else {
				logger.warning("Local ZMI without timestamp");
			}

			addResult = localTimestamp > freshnessTimestamp;
		}

		if (addResult) {
			localZmiCopy = localZmi.clone();
			localZmiCopy.getAttributes().add(PATHNAME_KEY, new ValueString(prefixPathname.toString()));
			acc.add(localZmiCopy.toProto(null, 0));
		}

		// We shouldn't go deeper if freshnessZmi doesn't have any children.
		// That avoids sending unnecessary ZmiProto.
		if (freshnessZmi == null || freshnessZmi.getSons().size() == 0) {
			return;
		}

		// Map freshnessZmi children by name
		for (ZMI freshnessChild : freshnessZmi.getSons()) {
			childName = ((ValueString) freshnessChild.getAttributes().get("name")).getValue();
			freshnessChildren.put(childName, freshnessChild);
		}

		// For each localZmi child, find its freshnessZmi equivalent
		for (ZMI localChild : localZmi.getSons()) {
			childName = ((ValueString) localChild.getAttributes().get("name")).getValue();
			foundFreshnessChild = freshnessChildren.get(childName);
			childPathnameComponents = new ArrayList<String>(prefixPathname.getComponents());
			childPathnameComponents.add(childName);
			childPathname = new PathName(childPathnameComponents);
			getFresherZmiThanHelper(acc, childPathname, localChild, foundFreshnessChild);
		}
	}

	/**
	 * Check if received candidate ZMI is fresher than ours corresponding proto.
	 * If true, update locally and in ZmiModule.
	 * Also, when we didn't know anything about the ZMI represented by candidate, we alter the tree structure.
	 * @param candidateZmiProto Candidate ZMI proto received from another agent.
	 */
	private void tryUpdateZmi(ZmiProto candidateZmiProto) {
		ZMI candidateZmi = ZMI.fromProto(candidateZmiProto);
		String candidatePathnameString = ((ValueString) candidateZmi.getAttributes().get(PATHNAME_KEY)).getValue();
		PathName candidatePathname = new PathName(candidatePathnameString);

		// If the ZMI wasn't even present in our data, we need to create new nodes in tree.
		// This can update local tree structure. Update in ZmiModule is handled separately.
		ZMI localZmi = null;
		try {
			localZmi = rootZmi.findDescendant(candidatePathname, true);
		} catch (TreeTraversalException e) {
			logger.log(Level.SEVERE, e.getMessage(), e.getStackTrace());
		}
		if (localZmi == null) {
			logger.log(Level.SEVERE, "ZMI was not created by findDescendant, aborting");
			return;
		}

		long candidateTimestamp = ((ValueTime) candidateZmi.getAttributes().get("timestamp")).getValue();
		long localTimestamp = ((ValueTime) localZmi.getAttributes().get("timestamp")).getValue();
		// Enough data to decide if we update the ZMI
		if (localTimestamp < candidateTimestamp) {
			// Remove value behind PATHNAME_KEY
			candidateZmi.getAttributes().remove(PATHNAME_KEY);
			// Create new ZmiProto
			ZmiProto safeCandidateZmiProto = candidateZmi.toProto(null, 0);
			ZmiProto.Builder updateProtoBuilder = ZmiProto.newBuilder(safeCandidateZmiProto).clearAttributesMapEntries();
			// Update local structure - the fastest way is from the proto
			for (AttributesMapEntryProto entry : safeCandidateZmiProto.getAttributesMapEntriesList()) {
				if (entry.getKey().equals("contacts")) {
					// We treat contacts in a special way - they are concatenated instead of replaced
					Value mergedContacts = mergeContacts(localZmi.getAttributes().getOrNull("contacts"), Value.fromProto(entry.getValue()));
					localZmi.getAttributes().addOrChange("contacts", mergedContacts);
					updateProtoBuilder.addAttributesMapEntries(
							AttributesMapEntryProto.newBuilder()
								.setKey("contacts")
								.setValue(mergedContacts.toProto())
								.build());
				} else {
					localZmi.getAttributes().addOrChange(entry.getKey(), Value.fromProto(entry.getValue()));
					updateProtoBuilder.addAttributesMapEntries(entry);
				}
			}
			ZmiProto updateProto = updateProtoBuilder.build();
			logger.fine("Updating ZMI " + candidatePathname + " with values:\n" + updateProto);
			// Send ZMI update message
			Message zmiUpdateMessage = buildZmiUpdateMessage(candidatePathnameString, updateProto);
			executor.enqueue(zmiUpdateMessage);
		} else {
			logger.fine("Updating ZMI " + candidatePathname + " ignored due to older timestamp");
		}
	}

	private Value mergeContacts(Value currentValue, Value candidateValue) {
		boolean currentOk = isCollectionOfContacts(currentValue);
		boolean candidateOk = isCollectionOfContacts(candidateValue);
		if (currentOk && candidateOk) {
			Map<String, Value> merged = new HashMap<String, Value>();
			for (Value v : toValueCollection(currentValue)) {
				merged.put(((ValueContact) v).getName().getName(), v);
			}
			for (Value v : toValueCollection(candidateValue)) {
				merged.put(((ValueContact) v).getName().getName(), v);
			}
			Value result = new ValueSet(new HashSet<Value>(merged.values()), TypePrimitive.CONTACT);
			logger.fine("Merged contacts: " + currentValue + " + " + candidateValue + " = " + result);
			return result;
		} else if (currentOk && !candidateOk) {
			return currentValue;
		} else if (!currentOk && candidateOk) {
			return candidateValue;
		} else {
			return new ValueSet(TypePrimitive.CONTACT);
		}
	}

	private boolean isCollectionOfContacts(Value checkedValue) {
		Type t;
		Type elemT = null;
		if (checkedValue instanceof ValueList) {
			t = ((ValueList) checkedValue).getType();
			elemT = ((TypeCollection) t).getElementType();
		}
		if (checkedValue instanceof ValueSet) {
			t = ((ValueSet) checkedValue).getType();
			elemT = ((TypeCollection) t).getElementType();
		}
		return elemT == TypePrimitive.CONTACT;
	}

	private Collection<Value> toValueCollection(Value v) {
		if (v instanceof ValueList) {
			return (ValueList) v;
		}
		if (v instanceof ValueSet) {
			return (ValueSet) v;
		}
		return null;
	}

	private Message buildGossipMessage(
			ZmiProto freshnessZmi,
			List<ZmiProto> fullAttributeZmiList,
			ZmiGossipMessage originalGossip) {

		ZmiGossipMessage.Builder builder =
			ZmiGossipMessage.newBuilder()
				.setSenderPathname(pathname.toString())
				.addAllFullAttributeZmi(fullAttributeZmiList);
		if (freshnessZmi != null) {
			builder.setFreshnessZmi(freshnessZmi);
		}
		if (originalGossip != null) {
			builder.setT1A(originalGossip.getT1A());
			builder.setT1B(originalGossip.getT1B());
			builder.setT2B(originalGossip.getT2B());
			builder.setT2A(originalGossip.getT2A());
			builder.setT3A(originalGossip.getT3A());
		} else {
			builder.setT1A(0);
			builder.setT1B(0);
			builder.setT2B(0);
			builder.setT2A(0);
			builder.setT3A(0);
		}
		Message requestMessage =
			Message.newBuilder()
				.setSourceModule(getModuleType())
				.setDestinationModule(getModuleType())
				.setZmiGossipMessage(builder.build())
				.build();
		return requestMessage;
	}

	private Message buildZmiUpdateMessage(String pathname, ZmiProto zmiProto) {
		ZmiUpdateRequestMessage zmiUpdateRequestMessageBody =
			ZmiUpdateRequestMessage.newBuilder()
				.setPathname(pathname)
				.addAllAttributesMapEntries(zmiProto.getAttributesMapEntriesList())
				.build();
		Message zmiUpdateRequestMessage =
			Message.newBuilder()
				.setSourceModule(getModuleType())
				.setDestinationModule("zmi")
				.setZmiUpdateRequestMessage(zmiUpdateRequestMessageBody)
				.build();
		return zmiUpdateRequestMessage;
	}
}
