package pl.edu.mimuw.cloudatlas.module.gossip;

import java.util.List;

import pl.edu.mimuw.cloudatlas.model.ZMI;

public interface NeighborStrategy {
	public ZMI pickZone(ZMI targetParent, List<ZMI> consideredTargetsOwners);
}
