package pl.edu.mimuw.cloudatlas.module.gossip;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import pl.edu.mimuw.cloudatlas.model.ZMI;

public class RandomNeighborStrategy implements NeighborStrategy {

	@Override
	public ZMI pickZone(ZMI targetParent, List<ZMI> consideredTargetsOwners) {
		int index = ThreadLocalRandom.current().nextInt(consideredTargetsOwners.size());
		return consideredTargetsOwners.get(index);
	}
}
