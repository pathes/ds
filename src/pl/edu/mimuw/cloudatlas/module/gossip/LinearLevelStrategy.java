package pl.edu.mimuw.cloudatlas.module.gossip;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

import pl.edu.mimuw.cloudatlas.model.PathName;

public class LinearLevelStrategy implements LevelStrategy {

	@Override
	public PathName pickLevel(PathName ourPathname) {
		// For a path /foo/bar/baz, its components will be ["foo", "bar", "baz"].
		// We are interested in picking a random level, from which we would pick a child node.
		// That is, we want to pick randomly with equal probability from following levels:
		//   /
		//   /foo
		//   /foo/bar
		ArrayList<String> components = new ArrayList<String>(ourPathname.getComponents());
		int newSize = ThreadLocalRandom.current().nextInt(components.size());
		return new PathName(components.subList(0, newSize));
	}
}
