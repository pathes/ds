package pl.edu.mimuw.cloudatlas.module.gossip;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;

import pl.edu.mimuw.cloudatlas.model.PathName;
import pl.edu.mimuw.cloudatlas.model.TreeTraversalException;
import pl.edu.mimuw.cloudatlas.model.ValueContact;
import pl.edu.mimuw.cloudatlas.model.ZMI;

public class GossipStrategy {

	private LevelStrategy levelStrategy;
	private NeighborStrategy neighborStrategy;
	private Logger logger;

	public GossipStrategy(LevelStrategy levelStrategy, NeighborStrategy neighborStrategy, Level level) {
		// Every gossip strategy can be divided into two parts.
		this.levelStrategy = levelStrategy;
		this.neighborStrategy = neighborStrategy;
		this.logger = Logger.getLogger("gossip_strategy");
		logger.setLevel(level);
	}

	public GossipStrategy(LevelStrategy levelStrategy, NeighborStrategy neighborStrategy) {
		this(levelStrategy, neighborStrategy, Level.WARNING);
	}

	/**
	 * Pick a gossiping target.
	 * @param emergencyContacts
	 * @param zmiProto Root ZMI as a ZmiProto.
	 * @param pathname Pathname of this agent.
	 * @return Pathname for gossiping target.
	 */
	public ValueContact pickTarget(ZMI root, PathName ourPathname, List<ValueContact> emergencyContacts) {
		ValueContact pickedTarget = null;
		String ourPathnameString = ourPathname.toString();
		// First of all, we should check if we can extract any contact from the tree.
		if (!anyContactExistInTree(root, true, ourPathnameString)) {
			if ((emergencyContacts != null) && (!emergencyContacts.isEmpty())) {
				// If there are no sibling zones with some contacts at any level,
				// one of the fallback contacts is selected instead.
				int randomEmergencyContactIndex = ThreadLocalRandom.current().nextInt(emergencyContacts.size());
				pickedTarget = emergencyContacts.get(randomEmergencyContactIndex);
				logger.warning("Picking emergency contact for " + ourPathname + ": " + pickedTarget);
				return pickedTarget;
			} else {
				logger.warning("Tried to pick emergency contact for " + ourPathname + ", but there are none");
				return null;
			}

		}

		PathName levelPathname = null;
		ZMI targetParent = null;
		List<ZMI> consideredZones = Collections.<ZMI>emptyList();

		// If there is at least one contact, perform strategy until we pick it
		// TODO(pathes): it can be done more efficient if we filter first the levels with at least one contact
		while (consideredZones.isEmpty()) {
			// Periodically, each node selects the level at which gossiping will be performed.
			// There can be different strategies for level selection. The top level must never be selected.
			// To make level uniquely identifiable, we pick its parent.
			levelPathname = levelStrategy.pickLevel(ourPathname);

			try {
				targetParent = root.findDescendant(levelPathname, false);
			} catch (TreeTraversalException e) {
				logger.log(Level.SEVERE, e.getMessage(), e.getStackTrace());
				break;
			}
			if (targetParent == null) {
				logger.severe("Cannot find requested ZMI " + levelPathname);
				break;
			}

			// Then, we select zones that make up the level.
			consideredZones = getConsideredZones(targetParent, ourPathnameString);
		}

		// Select the zone that will give us contacts
		ZMI pickedZone = neighborStrategy.pickZone(targetParent, consideredZones);

		// From the selected zone, one of the contacts is selected at random.
		//
		// Modification: filter out a contact leading to us
		// cf. Lab 06, "a sibling zone of the node's own zone with a nonempty contact set is selected at random"
		// instead, we allow us to be the donor of contacts, but we don't want us to be picked in the "random contact" phase.
		List<ValueContact> filteredContacts = new ArrayList<ValueContact>();
		for (ValueContact candidateContact : pickedZone.getContacts()) {
			if (!candidateContact.getName().toString().equals(ourPathnameString)) {
				System.out.println("Candidate contact: " + candidateContact.getName().toString());
				filteredContacts.add(candidateContact);
			}
		}
		int index = ThreadLocalRandom.current().nextInt(filteredContacts.size());
		pickedTarget = filteredContacts.get(index);
		logger.info("Picking contact for " + ourPathname + ": " + pickedTarget);
		return pickedTarget;
	}

	private List<ZMI> getConsideredZones(ZMI targetParent, String ourPathnameString) {
		List<ZMI> consideredZones = new ArrayList<ZMI>();
		for (ZMI child : targetParent.getSons()) {
			List<ValueContact> childContacts = child.getContacts();
			// Ignore nodes that don't have contacts
			if (childContacts.isEmpty()) {
				continue;
			}
			// Ignore nodes that have only a contact to us
			if (
					childContacts.size() == 1
					&& childContacts.get(0).getName().toString().equals(ourPathnameString)
			) {
				continue;
			}
			// Add that son to considered targets
			consideredZones.add(child);
		}
		return consideredZones;
	}

	private boolean anyContactExistInTree(ZMI currentZmi, boolean isRoot, String ourPathnameString) {
		List<ValueContact> contacts = currentZmi.getContacts();
		// "/" is never considered as a gossip target - ignore its contacts
		if (!contacts.isEmpty() && !isRoot) {
			for (ValueContact contact : contacts) {
				if (!contact.getName().toString().equals(ourPathnameString)) {
					return true;
				}
			}
		}
		for (ZMI child : currentZmi.getSons()) {
			if (anyContactExistInTree(child, false, ourPathnameString)) {
				return true;
			}
		}
		return false;
	}
}
