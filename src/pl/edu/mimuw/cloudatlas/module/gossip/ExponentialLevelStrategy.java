package pl.edu.mimuw.cloudatlas.module.gossip;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

import pl.edu.mimuw.cloudatlas.model.PathName;

public class ExponentialLevelStrategy implements LevelStrategy {

	private double lambda;
	private ArrayList<Double> memoizedPowers;
	private ArrayList<Double> memoizedPowersSum;

	/**
	 * Create new ExponentialLevelStrategy.
	 * @param lambda Exponential decay factor.
	 *               For example, lambda = 0.5 means that level N is picked
	 *               twice more frequent than level N+1.
	 */
	public ExponentialLevelStrategy(double lambda) {
		this.lambda = lambda;
		memoizedPowers = new ArrayList<Double>();
		memoizedPowers.add(1.0);
		memoizedPowersSum = new ArrayList<Double>();
		memoizedPowersSum.add(1.0);
	}

	@Override
	public PathName pickLevel(PathName ourPathname) {
		// For a path /foo/bar/baz, its components will be ["foo", "bar", "baz"].
		// We are interested in picking a random level, from which we would pick a child node.
		// That is, we want to pick randomly with exponentially decaying probability from following levels:
		//   /
		//   /foo
		//   /foo/bar
		ArrayList<String> components = new ArrayList<String>(ourPathname.getComponents());
		double cap = getNthPowersSum(components.size() - 1);
		double random = ThreadLocalRandom.current().nextDouble(cap);
		int level = 0;
		while (random > getNthPowersSum(level)) {
			level++;
		}
		return new PathName(components.subList(0, components.size() - level - 1));
	}

	private double getNthPowersSum(int n) {
		if (n > memoizedPowers.size()) {
			int powersCount = memoizedPowers.size();
			for (int i = powersCount; i <= n; ++i) {
				// Probabilities: p_i = lambda * p_{i-1}
				// Partial sums: s_i = s_{i-1} + p_i
				double newPower = memoizedPowers.get(i - 1) * lambda;
				memoizedPowers.add(newPower);
				memoizedPowersSum.add(memoizedPowersSum.get(i - 1) + newPower);
			}
		}
		return memoizedPowersSum.get(n);
	}
}
