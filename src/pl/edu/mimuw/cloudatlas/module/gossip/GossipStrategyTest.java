package pl.edu.mimuw.cloudatlas.module.gossip;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import org.junit.Before;
import org.junit.Test;

import pl.edu.mimuw.cloudatlas.model.PathName;
import pl.edu.mimuw.cloudatlas.model.TypePrimitive;
import pl.edu.mimuw.cloudatlas.model.Value;
import pl.edu.mimuw.cloudatlas.model.ValueContact;
import pl.edu.mimuw.cloudatlas.model.ValueList;
import pl.edu.mimuw.cloudatlas.model.ValueString;
import pl.edu.mimuw.cloudatlas.model.ZMI;

public class GossipStrategyTest {

	private HashMap<String, Integer> m;
	private ZMI root;

	private ValueContact createContact(String path) {
		try {
			return new ValueContact(
					new PathName(path),
					InetAddress.getByAddress(new byte[] {(byte)127, (byte)0, (byte)0, (byte)1}));
		} catch (UnknownHostException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Test hierarchy for testing retrieving actual contacts.
	 */
	private ZMI createTestHierarchy() {
		// Create contacts
		List<Value> contactListFoo = Arrays.asList(new Value[] {createContact("/fooA"), createContact("/fooB")});
		List<Value> contactListBar = Arrays.asList(new Value[] {createContact("/barA"), createContact("/barB"), createContact("/foo")});
		List<Value> contactListBaz = Arrays.asList(new Value[] {createContact("/bazA"), createContact("/bazB")});

		// Create /
		ZMI root = new ZMI();
		root.getAttributes().add("owner", new ValueString("/foo"));

		// Create /foo
		ZMI foo = new ZMI(root);
		root.addSon(foo);
		foo.getAttributes().add("name", new ValueString("foo"));
		foo.getAttributes().add("owner", new ValueString("/foo"));
		foo.getAttributes().add("contacts", new ValueList(new ArrayList<Value>(contactListFoo), TypePrimitive.CONTACT));

		// Create /bar
		ZMI bar = new ZMI(root);
		root.addSon(bar);
		bar.getAttributes().add("name", new ValueString("bar"));
		bar.getAttributes().add("owner", new ValueString("/bar"));
		bar.getAttributes().add("contacts", new ValueList(new ArrayList<Value>(contactListBar), TypePrimitive.CONTACT));

		// Create /baz
		ZMI baz = new ZMI(root);
		root.addSon(baz);
		baz.getAttributes().add("name", new ValueString("baz"));
		baz.getAttributes().add("owner", new ValueString("/baz"));
		baz.getAttributes().add("contacts", new ValueList(new ArrayList<Value>(contactListBaz), TypePrimitive.CONTACT));

		return root;
	}

	/**
	 * Test hierarchy for testing retrieving emergency contacts.
	 */
	private ZMI createEmergencyHierarchy() {
		// Create /
		ZMI root = new ZMI();
		root.getAttributes().add("owner", new ValueString("/foo"));

		// Create /foo
		ZMI foo = new ZMI(root);
		root.addSon(foo);
		foo.getAttributes().add("name", new ValueString("foo"));
		foo.getAttributes().add("owner", new ValueString("/foo"));

		// Create /bar
		ZMI bar = new ZMI(root);
		root.addSon(bar);
		bar.getAttributes().add("name", new ValueString("bar"));
		bar.getAttributes().add("owner", new ValueString("/bar"));

		return root;
	}

	/**
	 * Test hierarchy with only one contact
	 */
	private ZMI createOneContactHierarchy() {
		// Create /
		ZMI root = new ZMI();
		root.getAttributes().add("owner", new ValueString("/foo/A/one"));

		// Create /foo
		ZMI foo = new ZMI(root);
		root.addSon(foo);
		foo.getAttributes().add("name", new ValueString("foo"));
		foo.getAttributes().add("owner", new ValueString("/foo/A/one"));

		// Create /bar
		ZMI bar = new ZMI(root);
		root.addSon(bar);
		bar.getAttributes().add("name", new ValueString("bar"));
		bar.getAttributes().add("owner", new ValueString("/bar/F"));

		// Create /baz
		ZMI baz = new ZMI(root);
		root.addSon(baz);
		baz.getAttributes().add("name", new ValueString("baz"));
		baz.getAttributes().add("owner", new ValueString("/baz/X"));

		// Create /foo/A
		ZMI fooA = new ZMI(foo);
		foo.addSon(fooA);
		fooA.getAttributes().add("name", new ValueString("A"));
		fooA.getAttributes().add("owner", new ValueString("/foo/A/one"));

		// Create /foo/B
		ZMI fooB = new ZMI(foo);
		foo.addSon(fooB);
		fooB.getAttributes().add("name", new ValueString("B"));
		fooB.getAttributes().add("owner", new ValueString("/foo/B/seven"));
		// Let him have a contact
		List<Value> contactListFooB = Arrays.asList(new Value[] {createContact("/foo/C/nine")});
		fooB.getAttributes().add("contacts", new ValueList(new ArrayList<Value>(contactListFooB), TypePrimitive.CONTACT));

		// Create /foo/A/one
		ZMI fooAOne = new ZMI(fooA);
		fooA.addSon(fooAOne);
		fooAOne.getAttributes().add("name", new ValueString("one"));
		fooAOne.getAttributes().add("owner", new ValueString("/foo/A/one"));

		// Create /foo/A/two
		ZMI fooATwo = new ZMI(fooA);
		fooA.addSon(fooATwo);
		fooATwo.getAttributes().add("name", new ValueString("two"));
		fooATwo.getAttributes().add("owner", new ValueString("/foo/A/two"));

		return root;
	}

	/**
	 * Test hierarchy with only one contact
	 */
	private ZMI createOneContactInRootHierarchy() {
		// Create /
		ZMI root = new ZMI();
		root.getAttributes().add("owner", new ValueString("/foo/A/one"));
		// Let him have a contact
		List<Value> contactListRoot = Arrays.asList(new Value[] {createContact("/foo/C/nine")});
		root.getAttributes().add("contacts", new ValueList(new ArrayList<Value>(contactListRoot), TypePrimitive.CONTACT));

		// Create /foo
		ZMI foo = new ZMI(root);
		root.addSon(foo);
		foo.getAttributes().add("name", new ValueString("foo"));
		foo.getAttributes().add("owner", new ValueString("/foo/A/one"));

		// Create /bar
		ZMI bar = new ZMI(root);
		root.addSon(bar);
		bar.getAttributes().add("name", new ValueString("bar"));
		bar.getAttributes().add("owner", new ValueString("/bar/F"));

		// Create /baz
		ZMI baz = new ZMI(root);
		root.addSon(baz);
		baz.getAttributes().add("name", new ValueString("baz"));
		baz.getAttributes().add("owner", new ValueString("/baz/X"));

		// Create /foo/A
		ZMI fooA = new ZMI(foo);
		foo.addSon(fooA);
		fooA.getAttributes().add("name", new ValueString("A"));
		fooA.getAttributes().add("owner", new ValueString("/foo/A/one"));

		// Create /foo/B
		ZMI fooB = new ZMI(foo);
		foo.addSon(fooB);
		fooB.getAttributes().add("name", new ValueString("B"));
		fooB.getAttributes().add("owner", new ValueString("/foo/B/seven"));

		// Create /foo/A/one
		ZMI fooAOne = new ZMI(fooA);
		fooA.addSon(fooAOne);
		fooAOne.getAttributes().add("name", new ValueString("one"));
		fooAOne.getAttributes().add("owner", new ValueString("/foo/A/one"));

		// Create /foo/A/two
		ZMI fooATwo = new ZMI(fooA);
		fooA.addSon(fooATwo);
		fooATwo.getAttributes().add("name", new ValueString("two"));
		fooATwo.getAttributes().add("owner", new ValueString("/foo/A/two"));

		return root;
	}

	@Before
	public void setUp() {
		m = new HashMap<String, Integer>();
	}

	@Test
	public void testLinearLevelRandomNeighbor() {
		root = createTestHierarchy();

		// Combine strategies
		GossipStrategy gs = new GossipStrategy(
			new LinearLevelStrategy(),
			new RandomNeighborStrategy(),
			Level.OFF);

		// Collect all results
		final int expectedTargetCount = 6;
		final int iterationsPerLevel = 2500;
		final int iterations = iterationsPerLevel * expectedTargetCount;
		final int variation = 200;

		for (int i = 0; i < iterations; i++) {
			ValueContact pickedTarget = gs.pickTarget(root, new PathName("/foo"), null);
			assertFalse(pickedTarget == null);
			String pickedName = pickedTarget.getName().toString();
			int count = 0;
			if (m.containsKey(pickedName)) {
				count = m.get(pickedName).intValue();
			}
			m.put(pickedName, count + 1);
		}

		// We don't want to gossip with ourselves
		assertFalse(m.containsKey("/foo"));
		// We expect equal probabilities for all /foo, /bar and /baz contacts, except the /foo itself
		for (String s : Arrays.asList("/fooA", "/fooB", "/barA", "/barB", "/bazA", "/bazB")) {
			assertTrue(m.containsKey(s));
			assertTrue("'" + s + "' frequency too low", m.get(s) > iterationsPerLevel - variation);
			assertTrue("'" + s + "' frequency too high", m.get(s) < iterationsPerLevel + variation);
		}
	}

	@Test
	public void testEmergencyContacts() {
		root = createEmergencyHierarchy();

		// Combine strategies
		GossipStrategy gs = new GossipStrategy(
			new LinearLevelStrategy(),
			new RandomNeighborStrategy(),
			Level.OFF);

		ValueContact pickedTarget = gs.pickTarget(
				root,
				new PathName("/foo"),
				Arrays.asList(new ValueContact(new PathName("/emergency"), null)));

		assertEquals("/emergency", pickedTarget.getName().toString());
	}

	@Test
	public void testOneContact() {
		root = createOneContactHierarchy();

		// Combine strategies
		GossipStrategy gs = new GossipStrategy(
			new LinearLevelStrategy(),
			new RandomNeighborStrategy(),
			Level.OFF);

		final int iterations = 1000;

		for (int i = 0; i < iterations; i++) {
			ValueContact pickedTarget = gs.pickTarget(root, new PathName("/foo/A/one"), null);
			assertFalse(pickedTarget == null);
			String pickedName = pickedTarget.getName().toString();
			int count = 0;
			if (m.containsKey(pickedName)) {
				count = m.get(pickedName).intValue();
			}
			m.put(pickedName, count + 1);
		}

		// Let's see if contact was picked every time
		assertEquals(1, m.size());
		assertTrue(m.containsKey("/foo/C/nine"));
		assertEquals(iterations, m.get("/foo/C/nine").intValue());
	}

	@Test(timeout=1000)
	public void testOneContactInRoot() {
		root = createOneContactInRootHierarchy();

		// Combine strategies
		GossipStrategy gs = new GossipStrategy(
			new LinearLevelStrategy(),
			new RandomNeighborStrategy(),
			Level.OFF);

		final int iterations = 1000;

		for (int i = 0; i < iterations; i++) {
			ValueContact pickedTarget = gs.pickTarget(
					root,
					new PathName("/foo/A/one"),
					Arrays.asList(new ValueContact(new PathName("/emergency"), null)));
			assertFalse(pickedTarget == null);
			String pickedName = pickedTarget.getName().toString();
			int count = 0;
			if (m.containsKey(pickedName)) {
				count = m.get(pickedName).intValue();
			}
			m.put(pickedName, count + 1);
		}

		// Let's see if emergency contact was picked every time
		assertEquals(1, m.size());
		assertTrue(m.containsKey("/emergency"));
		assertEquals(iterations, m.get("/emergency").intValue());
	}
}
