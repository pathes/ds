package pl.edu.mimuw.cloudatlas.module.gossip;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import pl.edu.mimuw.cloudatlas.model.ValueString;
import pl.edu.mimuw.cloudatlas.model.ZMI;

import java.util.Arrays;
import java.util.List;

public class RoundRobinNeighborStrategyTest {

	private ZMI root, foo, bar, baz, aa, bb;
	private NeighborStrategy ns;
	private List<ZMI> rootLeaves, bazLeaves;

	@Before
	public void setUp() {
		// Create /
		root = new ZMI();
		root.getAttributes().add("owner", new ValueString("/foo"));

		// Create /foo
		foo = new ZMI(root);
		root.addSon(foo);
		foo.getAttributes().add("name", new ValueString("foo"));
		foo.getAttributes().add("owner", new ValueString("/foo"));

		// Create /bar
		bar = new ZMI(root);
		root.addSon(bar);
		bar.getAttributes().add("name", new ValueString("bar"));
		bar.getAttributes().add("owner", new ValueString("/bar"));

		// Create /baz
		baz = new ZMI(root);
		root.addSon(baz);
		baz.getAttributes().add("name", new ValueString("baz"));
		baz.getAttributes().add("owner", new ValueString("/baz"));

		// Create /baz/aa
		aa = new ZMI(baz);
		baz.addSon(aa);
		aa.getAttributes().add("name", new ValueString("aa"));
		aa.getAttributes().add("owner", new ValueString("/baz/aa"));

		// Create /baz/bb
		bb = new ZMI(baz);
		baz.addSon(bb);
		bb.getAttributes().add("name", new ValueString("bb"));
		bb.getAttributes().add("owner", new ValueString("/baz/bb"));

		// Leaves of whole tree
		rootLeaves = Arrays.asList(foo, bar, aa, bb);
		// Leaves of the /baz subtree
		bazLeaves = Arrays.asList(aa, bb);

		// Initialize the strategy
		ns = new RoundRobinNeighborStrategy();
	}

	@Test
	public void testRoundRobinCycling() {
		// Check round-robin a few times
		for (int i = 0; i < 5; ++i) {
			assertEquals(foo, ns.pickZone(root, rootLeaves));
			assertEquals(bar, ns.pickZone(root, rootLeaves));
			assertEquals(aa, ns.pickZone(root, rootLeaves));
			assertEquals(bb, ns.pickZone(root, rootLeaves));
		}
	}

	@Test
	public void testRoundRobinInterleaving() {
		// First, some rootLeaves round-robin
		assertEquals(foo, ns.pickZone(root, rootLeaves));
		assertEquals(bar, ns.pickZone(root, rootLeaves));
		assertEquals(aa, ns.pickZone(root, rootLeaves));
		// Then, bazLeaves round-robin
		assertEquals(aa, ns.pickZone(baz, bazLeaves));
		assertEquals(bb, ns.pickZone(baz, bazLeaves));
		assertEquals(aa, ns.pickZone(baz, bazLeaves));
		// Again, rootLeaves
		assertEquals(bb, ns.pickZone(root, rootLeaves));
		assertEquals(foo, ns.pickZone(root, rootLeaves));
	}
}
