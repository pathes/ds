package pl.edu.mimuw.cloudatlas.module.gossip;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.edu.mimuw.cloudatlas.model.ZMI;

public class RoundRobinNeighborStrategy implements NeighborStrategy {

	private Map<ZMI, Integer> turnMap;

	public RoundRobinNeighborStrategy() {
		turnMap = new HashMap<ZMI, Integer>();
	}

	@Override
	public ZMI pickZone(ZMI targetParent, List<ZMI> consideredTargetsOwners) {
		if (consideredTargetsOwners.size() == 0) {
			return null;
		}
		int index = 0;
		if (turnMap.containsKey(targetParent)) {
			index = turnMap.get(targetParent);
		}
		// Fetch the result
		ZMI pickedTarget = consideredTargetsOwners.get(index);
		// Update the index
		index = (index + 1) % consideredTargetsOwners.size();
		turnMap.put(targetParent, index);
		return pickedTarget;
	}

}
