package pl.edu.mimuw.cloudatlas.module.gossip;

import pl.edu.mimuw.cloudatlas.model.PathName;

public interface LevelStrategy {
	public PathName pickLevel(PathName ourPathname);
}
