package pl.edu.mimuw.cloudatlas.module.gossip;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashMap;

import org.junit.Test;

import pl.edu.mimuw.cloudatlas.model.PathName;

public class LinearLevelStrategyTest {

	@Test
	public void testExponentialDistribution() {
		final PathName p = new PathName("/foo/bar/baz");
		final int levelCount = p.getComponents().size();
		final int iterationsPerLevel = 40000;
		final int iterations = levelCount * iterationsPerLevel;
		// We don't mock random, rather we assume that with iteration count big enough,
		// the results should be exponential within arbitrary variation.
		final int variation = 500;

		LevelStrategy ls = new LinearLevelStrategy();

		// Collect all results
		HashMap<String, Integer> m = new HashMap<String, Integer>();
		for (int i = 0; i < iterations; i++) {
			String a = ls.pickLevel(p).toString();
			int count = 0;
			if (m.containsKey(a)) {
				count = m.get(a).intValue();
			}
			m.put(a, count + 1);
		}

		for (String s : Arrays.asList("/", "/foo", "/foo/bar")) {
			assertTrue("'" + s + "' frequency too low", m.get(s) > iterationsPerLevel - variation);
			assertTrue("'" + s + "' frequency too high", m.get(s) < iterationsPerLevel + variation);
		}
		assertFalse(m.containsKey("/foo/bar/baz"));
	}
}
