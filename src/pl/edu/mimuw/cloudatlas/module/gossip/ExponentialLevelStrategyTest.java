package pl.edu.mimuw.cloudatlas.module.gossip;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.util.HashMap;

import pl.edu.mimuw.cloudatlas.model.PathName;

public class ExponentialLevelStrategyTest {

	@Test
	public void testExponentialDistribution() {
		final PathName p = new PathName("/foo/bar/baz");
		final int iterations = 175000; // 100000 + 50000 + 25000
		final double lambda = 0.5;
		// We don't mock random, rather we assume that with iteration count big enough,
		// the results should be exponential within arbitrary variation.
		final int variation = 500;

		LevelStrategy ls = new ExponentialLevelStrategy(lambda);

		// Collect all results
		HashMap<String, Integer> m = new HashMap<String, Integer>();
		for (int i = 0; i < iterations; i++) {
			String a = ls.pickLevel(p).toString();
			int count = 0;
			if (m.containsKey(a)) {
				count = m.get(a).intValue();
			}
			m.put(a, count + 1);
		}

		assertTrue("'/' frequency too low", m.get("/") > 25000 - variation);
		assertTrue("'/' frequency too high", m.get("/") < 25000 + variation);
		assertTrue("'/foo' frequency too low", m.get("/foo") > 50000 - variation);
		assertTrue("'/foo' frequency too high", m.get("/foo") < 50000 + variation);
		assertTrue("'/foo/bar' frequency too low", m.get("/foo/bar") > 100000 - variation);
		assertTrue("'/foo/bar' frequency too high", m.get("/foo/bar") < 100000 + variation);
		assertFalse(m.containsKey("/foo/bar/baz"));
	}
}
