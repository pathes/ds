package pl.edu.mimuw.cloudatlas.module;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.protobuf.InvalidProtocolBufferException;

import pl.edu.mimuw.cloudatlas.RMIinterface.AttributesFetcherInterface;
import pl.edu.mimuw.cloudatlas.agent.Executor;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.Message;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.ZmiUpdateRequestMessage;
import pl.edu.mimuw.cloudatlas.model.CloudAtlasModels.ZmiProto;

public class FetcherModule extends Module {

	/**
	 *
	 * @author Paweł Kamiński
	 * Fetcher server should be available from FetcherModule
	 * Fetcher Server should be considered as RMI ending of Fetcher Module
	 */
	public class FetcherServer implements AttributesFetcherInterface {
		private Logger logger;
		private String pathname;

		public FetcherServer(String pathname) {
			 this.logger = Logger.getLogger("fetcherServer");
			 this.pathname = pathname;
		}

		@Override
		/**
		 * Generates and updates lowest level attributes.
		 *
		 * @param zmiProtoBytes - serialized ZmiProto, without any son
		 * @return return if operation has finished properly.
		 */
		public Boolean setLowLevelAttributes(byte[] zmiProtoBytes) throws RemoteException {
			ZmiProto parsedZmi;

			logger.log(Level.INFO, "Fetcher RMI message received");
			try {
				parsedZmi = ZmiProto.parseFrom(zmiProtoBytes);

			} catch (InvalidProtocolBufferException e) {
				logger.log(Level.SEVERE, "Could not parse message\n" + Arrays.toString(zmiProtoBytes));
				return false;
			}

			ZmiUpdateRequestMessage zmiUpdateRequestMessageBody =
					ZmiUpdateRequestMessage.newBuilder()
						.setPathname(pathname)
						.addAllAttributesMapEntries(parsedZmi.getAttributesMapEntriesList())
						.build();

			Message zmiUpdateRequestMessage =
					Message.newBuilder()
						.setSourceModule(getModuleType())
						.setDestinationModule("zmi")
						.setZmiUpdateRequestMessage(zmiUpdateRequestMessageBody)
						.build();

			executor.enqueue(zmiUpdateRequestMessage);
			logger.log(Level.INFO, "Fetcher update enqueued");
			return true;
		}

	}

	public FetcherModule(Executor executor, Level loggingLevel, String pathname) {
		super(executor, loggingLevel);

		// put RMI data to server
		if (System.getSecurityManager() == null) {
			System.setSecurityManager(new SecurityManager());
		}
		try {
			FetcherServer serverFetcherObject = new FetcherServer(pathname);
			AttributesFetcherInterface fetcherStub =
					(AttributesFetcherInterface) UnicastRemoteObject.exportObject(serverFetcherObject, 3377);
			Registry registry = LocateRegistry.getRegistry();
			registry.rebind("Fetcher", fetcherStub);
			logger.log(Level.INFO, "Fetcher bounded");
		} catch (Exception e) {
			System.err.println("Fetcher server exception:");
			e.printStackTrace();
		}
	}

	@Override
	public String getModuleType() {
		return "fetcher";
	}

}
