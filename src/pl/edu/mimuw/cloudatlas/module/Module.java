package pl.edu.mimuw.cloudatlas.module;

import java.util.Arrays;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

import pl.edu.mimuw.cloudatlas.agent.Executor;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.CommunicationMessage;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.FallbackContactsUpdateRequestMessage;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.Message;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.PingMessage;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.QueryGossipMessage;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.QueryUpdateRequestMessage;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.TimerMessage;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.ZmiGossipMessage;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.ZmiRequestMessage;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.ZmiResponseMessage;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.ZmiUpdateRequestMessage;
import pl.edu.mimuw.cloudatlas.message.MessageConsumer;

public abstract class Module implements MessageConsumer {

	protected Executor executor;
	protected Logger logger;

	public Module(Executor executor, Level loggingLevel) {
		this.executor = executor;
		executor.registerModule(this);
		logger = Logger.getLogger(getModuleType());
		logger.setLevel(loggingLevel);
	}

	public abstract String getModuleType();

	@Override
	public void run() {}

	@Override
	public void stop() {}

	@Override
	public Collection<String> getFinalConsumerTypes() {
		return Arrays.asList(getModuleType());
	}

	@Override
	public void consume(Message msg) {
		switch (msg.getBodyOneofCase()) {
			case BODYONEOF_NOT_SET:
				throw new ModuleException("Message body not set");
			case COMMUNICATION_MESSAGE:
				logConsume("CommunicationMessage", msg);
				this.handleCommunication(msg.getCommunicationMessage());
				break;
			case PING_MESSAGE:
				logConsume("PingMessage", msg);
				this.handlePing(msg.getPingMessage());
				break;
			case TIMER_MESSAGE:
				logConsume("TimerMessage", msg);
				this.handleTimer(msg.getTimerMessage());
				break;
			case ZMI_REQUEST_MESSAGE:
				logConsume("ZmiRequestMessage", msg);
				this.handleZmiRequest(
					msg.getZmiRequestMessage(),
					msg.getSourceModule());
				break;
			case ZMI_RESPONSE_MESSAGE:
				logConsume("ZmiResponseMessage", msg);
				this.handleZmiResponse(msg.getZmiResponseMessage());
				break;
			case ZMI_UPDATE_REQUEST_MESSAGE:
				logConsume("ZmiUpdateRequestMessage", msg);
				this.handleZmiUpdateRequest(msg.getZmiUpdateRequestMessage());
				break;
			case ZMI_GOSSIP_MESSAGE:
				logConsume("ZmiGossipMessage", msg);
				this.handleZmiGossip(
					msg.getZmiGossipMessage(),
					msg.getSourceInetAddress());
				break;
			case QUERY_GOSSIP_MESSAGE:
				logConsume("QueryGossipMessage", msg);
				this.handleQueryGossip(
					msg.getQueryGossipMessage(),
					msg.getSourceInetAddress());
				break;
			case QUERY_UPDATE_REQUEST_MESSAGE:
				logConsume("QueryUpdateRequestMessage", msg);
				this.handleQueryUpdateRequest(msg.getQueryUpdateRequestMessage());
				break;
			case FALLBACK_CONTACTS_UPDATE_REQUEST_MESSAGE:
				logConsume("FallbackContactsUpdateRequestMessage", msg);
				this.handleFallbackContactsUpdateRequest(msg.getFallbackContactsUpdateRequestMessage());
				break;
			default:
				break;
		}
	}

	private void logConsume(String messageType, Message msg) {
		logger.info(messageType + " consumed by " + getModuleType() + " module");
		logger.fine("\n" + msg.toString());
	}

	protected void handleTimer(TimerMessage timerMessage) {
		throw new ModuleException("Undefined timer message handle for module " + this.getModuleType());
	}

	protected void handlePing(PingMessage pingMessage) {
		throw new ModuleException("Undefined ping message handle for module " + this.getModuleType());
	}

	protected void handleCommunication(CommunicationMessage communicationMessage) {
		throw new ModuleException("Undefined communication message handle for module " + this.getModuleType());
	}

	protected void handleZmiRequest(ZmiRequestMessage zmiRequestMessage, String sourceModule) {
		throw new ModuleException("Undefined ZMI request message handle for module " + this.getModuleType());
	}

	protected void handleZmiResponse(ZmiResponseMessage zmiResponseMessage) {
		throw new ModuleException("Undefined ZMI response message handle for module " + this.getModuleType());
	}

	protected void handleZmiUpdateRequest(ZmiUpdateRequestMessage zmiUpdateRequestMessage) {
		throw new ModuleException("Undefined ZMI update response message handle for module " + this.getModuleType());
	}

	protected void handleZmiGossip(ZmiGossipMessage zmiGossipMessage, String sourceInetAddressString) {
		throw new ModuleException("Undefined ZMI gossip message handle for module " + this.getModuleType());
	}

	protected void handleQueryGossip(QueryGossipMessage queryGossipMessage, String sourceInetAddressString) {
		throw new ModuleException("Undefined query gossip message handle for module " + this.getModuleType());
	}

	protected void handleQueryUpdateRequest(QueryUpdateRequestMessage queryUpdateRequestMessage) {
		throw new ModuleException("Undefined query update request message handle for module " + this.getModuleType());
	}

	protected void handleFallbackContactsUpdateRequest(FallbackContactsUpdateRequestMessage fallbackContactsUpdateRequestMessage) {
		throw new ModuleException("Undefined fallback contacts update request message handle for module " + this.getModuleType());
	}

	/**
	 * Schedule a simple ping message without any message set.
	 * @param intervalMs Timer delay interval.
	 */
	protected void schedulePing(int intervalMs) {
		// Prepare wrapped ping message
		PingMessage wrappedMessageBody =
			PingMessage.newBuilder()
				.build();
		Message wrappedMessage =
			Message.newBuilder()
				.setDestinationModule(getModuleType())
				.setPingMessage(wrappedMessageBody)
				.build();
		// Prepare wrapping timer message
		TimerMessage wrappingMessageBody =
			TimerMessage.newBuilder()
				.setMilliseconds(intervalMs)
				.setSubmessage(wrappedMessage)
				.build();
		Message wrappingMessage =
			Message.newBuilder()
				.setSourceModule(getModuleType())
				.setDestinationModule("timer")
				.setTimerMessage(wrappingMessageBody)
				.build();
		// Enqueue the message
		this.executor.enqueue(wrappingMessage);
	}
}
