package pl.edu.mimuw.cloudatlas.module;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.Test;

import pl.edu.mimuw.cloudatlas.agent.Executor;
import pl.edu.mimuw.cloudatlas.model.AttributesMap;
import pl.edu.mimuw.cloudatlas.model.PathName;
import pl.edu.mimuw.cloudatlas.model.TypePrimitive;
import pl.edu.mimuw.cloudatlas.model.Value;
import pl.edu.mimuw.cloudatlas.model.ValueContact;
import pl.edu.mimuw.cloudatlas.model.ValueInt;
import pl.edu.mimuw.cloudatlas.model.ValueList;
import pl.edu.mimuw.cloudatlas.model.ValueSet;
import pl.edu.mimuw.cloudatlas.model.ValueString;
import pl.edu.mimuw.cloudatlas.model.ValueTime;
import pl.edu.mimuw.cloudatlas.model.ZMI;
import pl.edu.mimuw.cloudatlas.model.CloudAtlasModels.ZmiProto;
import pl.edu.mimuw.cloudatlas.module.gossip.GossipStrategy;
import pl.edu.mimuw.cloudatlas.module.gossip.LinearLevelStrategy;
import pl.edu.mimuw.cloudatlas.module.gossip.RoundRobinNeighborStrategy;


class ShiftedUdpListener extends UdpListener {
	long shiftMs;

	public ShiftedUdpListener(int port, Executor executor, Logger logger, long shiftMs) {
		super(port, executor, logger);
		this.shiftMs = shiftMs;
	}

	@Override
	protected long timeMs() {
		return super.timeMs() + shiftMs;
	}
}

class ShiftedUdpSender extends UdpSender {
	long shiftMs;

	public ShiftedUdpSender(int port, Logger logger, long shiftMs) {
		super(port, logger);
		this.shiftMs = shiftMs;
	}


	@Override
	protected long timeMs() {
		return super.timeMs() + shiftMs;
	}
}

class ShiftedUdpCommunicationModule extends UdpCommunicationModule {
	long shiftMs;

	public ShiftedUdpCommunicationModule(Executor executor, Level loggingLevel, String agentId, int listenerPort,
			int remoteListenerPort, int senderPort, long shiftMs) {
		super(executor, loggingLevel, agentId, listenerPort, remoteListenerPort, senderPort);
		this.shiftMs = shiftMs;
	}

	@Override
	public void run() {
		udpListener = new ShiftedUdpListener(listenerPort, executor, logger, shiftMs);
		udpSender = new ShiftedUdpSender(senderPort, logger, shiftMs);
		listenerThread = new Thread(udpListener);
		listenerThread.start();
	}
}


class ZmiGossipExecutionHelper {
	private Executor executor;
	@SuppressWarnings("unused")
	private TimerModule timerModule;
	@SuppressWarnings("unused")
	private UdpCommunicationModule udpCommunicationModule;
	private ZmiModule zmiModule;
	private ZmiHelperModule zmiHelperModule;
	private ZmiGossipModule zmiGossipModule;
	private CountDownLatch latch;

	public ZmiGossipExecutionHelper(Level logLevel, ZMI root, String pathname,
			int listenerPort, int remoteListenerPort, int senderPort, long shiftMs)
			throws ParseException, IOException {
		latch = new CountDownLatch(1);
		executor = new Executor(logLevel, null);
		// Timer module is required for communication retry
		timerModule = new TimerModule(executor, logLevel);
		udpCommunicationModule = new ShiftedUdpCommunicationModule(
			executor,
			logLevel,
			pathname,
			listenerPort,
			remoteListenerPort,
			senderPort,
			shiftMs);
		zmiModule = new ZmiModule(
				executor, logLevel, root,
				null, 100000, 60000);
		zmiHelperModule = new ZmiHelperModule(executor, logLevel);
		zmiGossipModule = new ZmiGossipModule(
			executor,
			logLevel,
			new PathName(pathname),
			root,
			new GossipStrategy(
				new LinearLevelStrategy(),
				new RoundRobinNeighborStrategy()),
			500,
			null);
	}

	public CountDownLatch getLatch() {
		return latch;
	}

	public Executor getExecutor() {
		return executor;
	}

	public ZmiModule getZmiModule() {
		return zmiModule;
	}

	public ZmiHelperModule getZmiHelperModule() {
		return zmiHelperModule;
	}

	public ZmiGossipModule getZmiGossipModule() {
		return zmiGossipModule;
	}
}


public class ZmiGossipModuleTest {

	private ValueContact createContact(String path) throws UnknownHostException {
		return new ValueContact(new PathName(path), InetAddress.getByAddress(new byte[] {
			(byte)127, (byte)0, (byte)0, (byte)1
		}));
	}

	private ZMI hierarchyHelper(ZMI parent, String name, String owner, List<Value> contactList) {
		ZMI zmi = new ZMI(parent);
		if (parent != null) {
			parent.addSon(zmi);
		}
		zmi.getAttributes().add("name", new ValueString(name));
		zmi.getAttributes().add("contacts", new ValueList(new ArrayList<Value>(contactList), TypePrimitive.CONTACT));
		zmi.getAttributes().add("owner", new ValueString(owner));
		zmi.getAttributes().add("timestamp", new ValueTime(0L));
		zmi.getAttributes().add("x", new ValueInt(0L));
		return zmi;
	}

	/**
	 * Simple ZMI hierarchy for testing if /foo can gossip with /bar
	 */
	private ZMI createSmallHierarchy() throws ParseException, UnknownHostException {
		ValueContact fooContact = createContact("/foo");
		ValueContact barContact = createContact("/bar");
		List<Value> contactList = Arrays.asList(new Value[] {fooContact, barContact});

		ZMI root = hierarchyHelper(null, "", "/foo", contactList);
		hierarchyHelper(root, "foo", "/foo", contactList);
		hierarchyHelper(root, "bar", "/bar", contactList);

		return root;
	}

	/**
	 * Simple ZMI hierarchy for testing if /foo can gossip with /bar.
	 * This hierarchy is dedicated for agent /bar.
	 * The ZMI tree is truncated (without /foo) comparing with createSmallhierarchy.
	 * Also, there is another contact /xyzzy in /bar for testing contact merging.
	 */
	private ZMI createSmallTruncatedHierarchy() throws ParseException, UnknownHostException {
		ValueContact fooContact = createContact("/foo");
		ValueContact xyzzyContact = createContact("/xyzzy");
		List<Value> contactList = Arrays.asList(new Value[] {fooContact, xyzzyContact});

		ZMI root = hierarchyHelper(null, "", "/foo", contactList);
		hierarchyHelper(root, "bar", "/bar", contactList);

		return root;
	}

	/**
	 * Bigger ZMI hierarchy. /foo/A and /bar/C should retrive contacts from neighboring ZMIs.
	 */
	private ZMI createBigHierarchy() throws ParseException, UnknownHostException {
		// The only viable contacts are /foo/A and /bar/C
		ValueContact fooAContact = createContact("/foo/A");
		ValueContact barCContact = createContact("/bar/C");
		List<Value> contactList = Arrays.asList(new Value[] {fooAContact, barCContact});

		ZMI root = hierarchyHelper(null, "", "/foo/A", contactList);
		ZMI foo = hierarchyHelper(root, "foo", "/foo/A", contactList);
		ZMI bar = hierarchyHelper(root, "bar", "/bar/C", contactList);
		// /foo/A itself doesn't have contacts
		hierarchyHelper(foo, "A", "/foo/A", Collections.<Value>emptyList());
		hierarchyHelper(foo, "B", "/foo/B", contactList);
		// /bar/C itself doesn't have contacts
		hierarchyHelper(bar, "C", "/bar/C", Collections.<Value>emptyList());
		hierarchyHelper(bar, "D", "/bar/D", contactList);

		return root;
	}

	private long fetchAttribute(ZmiGossipExecutionHelper helper, String attribute, String pathname) throws InterruptedException {
		helper.getZmiHelperModule().fetchAttributes(pathname, true, Arrays.asList(attribute));
		Thread.sleep(100);
		ZmiProto fetchedZmiProto = helper.getZmiHelperModule().getZmiProto();
		ZMI fetchedZmi = ZMI.fromProto(fetchedZmiProto);
		Value fetchedValue = fetchedZmi.getAttributes().get(attribute);
		if (fetchedValue instanceof ValueInt) {
			return ((ValueInt) fetchedValue).getValue().longValue();
		} else if (fetchedValue instanceof ValueTime) {
			return ((ValueTime) fetchedValue).getValue().longValue();
		}
		return -1L;
	}

	private Set<String> fetchContactPaths(ZmiGossipExecutionHelper helper, String pathname) throws InterruptedException {
		helper.getZmiHelperModule().fetchAttributes(pathname, true, Arrays.asList("contacts"));
		Thread.sleep(100);
		ZmiProto fetchedZmiProto = helper.getZmiHelperModule().getZmiProto();
		ZMI fetchedZmi = ZMI.fromProto(fetchedZmiProto);
		Value fetchedValue = fetchedZmi.getAttributes().get("contacts");
		// Get all contacts' paths
		Collection<Value> fetchedCol;
		if (fetchedValue instanceof ValueList) {
			fetchedCol = (ValueList) fetchedValue;
		} else if (fetchedValue instanceof ValueSet) {
			fetchedCol = (ValueSet) fetchedValue;
		} else {
			return new HashSet<String>();
		}
		// Convert collection of Values to set of strings
		Set<String> result = new HashSet<String>();
		for (Value v : fetchedCol) {
			result.add(((ValueContact) v).getName().getName());
		}
		return result;
	}

	@Test
	public void testSmallGossip() throws ParseException, IOException, InterruptedException {
		ZmiGossipExecutionHelper helperFoo = new ZmiGossipExecutionHelper(
				Level.OFF, createSmallHierarchy(), "/foo", 9150, 9160, 9151, 0L);
		ZmiGossipExecutionHelper helperBar = new ZmiGossipExecutionHelper(
				Level.OFF, createSmallHierarchy(), "/bar", 9160, 9150, 9161, 0L);
		// Set new values in foo-agent's /foo
		AttributesMap attributesMap = new AttributesMap();
		attributesMap.add("x", new ValueInt(42L));
		attributesMap.add("timestamp", new ValueTime(13000L));
		helperFoo.getZmiHelperModule().setAttributes("/foo", attributesMap.entryProtoList());
		// Set new values in bar-agent's /bar
		attributesMap = new AttributesMap();
		attributesMap.add("x", new ValueInt(21L));
		attributesMap.add("timestamp", new ValueTime(11000L));
		helperBar.getZmiHelperModule().setAttributes("/bar", attributesMap.entryProtoList());
		// Run threads
		(new Thread(helperFoo.getExecutor())).start();
		(new Thread(helperBar.getExecutor())).start();
		try {
			Thread.sleep(1000);
			// Check if foo-agent updated its /bar and kept its /foo
			assertEquals(42L, fetchAttribute(helperFoo, "x", "/foo"));
			assertEquals(21L, fetchAttribute(helperFoo, "x", "/bar"));
			// Check if bar-agent updated its /foo and kept its /bar
			assertEquals(42L, fetchAttribute(helperBar, "x", "/foo"));
			assertEquals(21L, fetchAttribute(helperBar, "x", "/bar"));
		} finally {
			helperFoo.getExecutor().stop();
			helperBar.getExecutor().stop();
			// Allow sockets to be freed
			Thread.sleep(100);
		}
	}

	@Test
	public void testSmallTruncatedGossip() throws ParseException, IOException, InterruptedException {
		ZmiGossipExecutionHelper helperFoo = new ZmiGossipExecutionHelper(
				Level.OFF, createSmallHierarchy(), "/foo", 9150, 9160, 9151, 0L);
		ZmiGossipExecutionHelper helperBar = new ZmiGossipExecutionHelper(
				Level.OFF, createSmallTruncatedHierarchy(), "/bar", 9160, 9150, 9161, 0L);
		// Set new values in foo-agent's /foo
		AttributesMap attributesMap = new AttributesMap();
		attributesMap.add("x", new ValueInt(42L));
		attributesMap.add("timestamp", new ValueTime(13000L));
		helperFoo.getZmiHelperModule().setAttributes("/foo", attributesMap.entryProtoList());
		// Set new values in bar-agent's /bar
		attributesMap = new AttributesMap();
		attributesMap.add("x", new ValueInt(21L));
		attributesMap.add("timestamp", new ValueTime(11000L));
		helperBar.getZmiHelperModule().setAttributes("/bar", attributesMap.entryProtoList());
		// Run threads
		(new Thread(helperFoo.getExecutor())).start();
		(new Thread(helperBar.getExecutor())).start();
		try {
			Thread.sleep(1500);
			// Check for contacts' paths
			Set<String> expectedContactPaths = new HashSet<String>(Arrays.asList("/foo", "/bar", "/xyzzy"));
			assertEquals(expectedContactPaths, fetchContactPaths(helperFoo, "/bar"));
			assertEquals(expectedContactPaths, fetchContactPaths(helperBar, "/bar"));
			// Check if foo-agent updated its /bar and kept its /foo
			assertEquals(42L, fetchAttribute(helperFoo, "x", "/foo"));
			assertEquals(21L, fetchAttribute(helperFoo, "x", "/bar"));
			// Check if bar-agent updated its /foo and kept its /bar
			assertEquals(42L, fetchAttribute(helperBar, "x", "/foo"));
			assertEquals(21L, fetchAttribute(helperBar, "x", "/bar"));
		} finally {
			helperFoo.getExecutor().stop();
			helperBar.getExecutor().stop();
			// Allow sockets to be freed
			Thread.sleep(100);
		}
	}

	@Test
	public void testBigGossip() throws ParseException, IOException, InterruptedException {
		ZmiGossipExecutionHelper helperFooA = new ZmiGossipExecutionHelper(
				Level.OFF, createBigHierarchy(), "/foo/A", 9170, 9180, 9171, 0L);
		ZmiGossipExecutionHelper helperBarC = new ZmiGossipExecutionHelper(
				Level.OFF, createBigHierarchy(), "/bar/C", 9180, 9170, 9181, 0L);
		// Set new values in fooA-agent's /foo/A
		AttributesMap attributesMap = new AttributesMap();
		attributesMap.add("x", new ValueInt(42L));
		attributesMap.add("timestamp", new ValueTime(13000L));
		helperFooA.getZmiHelperModule().setAttributes("/foo/A", attributesMap.entryProtoList());
		// Set new values in barC-agent's /bar/C
		attributesMap = new AttributesMap();
		attributesMap.add("x", new ValueInt(21L));
		attributesMap.add("timestamp", new ValueTime(11000L));
		helperBarC.getZmiHelperModule().setAttributes("/bar/C", attributesMap.entryProtoList());
		// Run threads
		(new Thread(helperFooA.getExecutor())).start();
		(new Thread(helperBarC.getExecutor())).start();
		try {
			Thread.sleep(1000);
			// Check if fooA-agent updated its /bar/C and kept its /foo/A
			assertEquals(42L, fetchAttribute(helperFooA, "x", "/foo/A"));
			assertEquals(21L, fetchAttribute(helperFooA, "x", "/bar/C"));
			// Check if barC-agent updated its /foo/A and kept its /bar/C
			assertEquals(42L, fetchAttribute(helperBarC, "x", "/foo/A"));
			assertEquals(21L, fetchAttribute(helperBarC, "x", "/bar/C"));
		} finally {
			helperFooA.getExecutor().stop();
			helperBarC.getExecutor().stop();
			// Allow sockets to be freed
			Thread.sleep(100);
		}
	}

	@Test
	public void testGtp() throws ParseException, IOException, InterruptedException {
		// bar's clock will be shifted by 5s
		long shiftS = 5L;
		long shiftMs = shiftS * 1000;
		// How much UDP-related time discrepancy we tolerate
		long variationMs = 100L;
		ZmiGossipExecutionHelper helperFoo = new ZmiGossipExecutionHelper(
				Level.OFF, createSmallHierarchy(), "/foo", 9130, 9140, 9131, 0L);
		ZmiGossipExecutionHelper helperBar = new ZmiGossipExecutionHelper(
				Level.OFF, createSmallHierarchy(), "/bar", 9140, 9130, 9141, shiftMs);
		// Set new values in foo-agent's /foo
		AttributesMap attributesMap = new AttributesMap();
		attributesMap.add("x", new ValueInt(42L));
		attributesMap.add("timestamp", new ValueTime(2000L)); // milliseconds
		helperFoo.getZmiHelperModule().setAttributes("/foo", attributesMap.entryProtoList());
		// Set new values in foo-agent's /bar
		attributesMap = new AttributesMap();
		attributesMap.add("x", new ValueInt(137L));
		attributesMap.add("timestamp", new ValueTime(11000L)); // milliseconds
		helperFoo.getZmiHelperModule().setAttributes("/bar", attributesMap.entryProtoList());
		// Set new values in bar-agent's /bar
		attributesMap = new AttributesMap();
		attributesMap.add("x", new ValueInt(21L));
		// bar-agent has the tree with bigger timestamp than foo-agent, but it's clock is shifted by 5s.
		// Therefore gossiping shouldn't overwrite the value that's in foo-agent.
		attributesMap.add("timestamp", new ValueTime(15000L)); // milliseconds
		helperBar.getZmiHelperModule().setAttributes("/bar", attributesMap.entryProtoList());
		// Run threads
		(new Thread(helperFoo.getExecutor())).start();
		(new Thread(helperBar.getExecutor())).start();
		try {
			Thread.sleep(1000);
			// Check if foo-agent kept its /foo and /bar
			assertEquals(42L, fetchAttribute(helperFoo, "x", "/foo"));
			assertEquals(137L, fetchAttribute(helperFoo, "x", "/bar"));
			// Timestamp might have changed a bit due to rounding errors and re-gossiping
			long timestamp = fetchAttribute(helperFoo, "timestamp", "/bar");
			assertTrue("bar agent: /bar timestamp too low: " + Long.toString(timestamp), timestamp > 11000L - variationMs);
			assertTrue("bar agent: /bar timestamp too high: " + Long.toString(timestamp), timestamp < 11000L + variationMs);
			// Check if bar-agent updated its /foo and /bar
			// When bar-agent updated its ZMI with data from foo-agent, the timestamps should have been shifted.
			assertEquals(42L, fetchAttribute(helperBar, "x", "/foo"));
			assertEquals(137L, fetchAttribute(helperBar, "x", "/bar"));
			timestamp = fetchAttribute(helperBar, "timestamp", "/foo");
			assertTrue("bar agent: /foo timestamp too low: " + Long.toString(timestamp), timestamp > 2000L + shiftMs - variationMs);
			assertTrue("bar agent: /foo timestamp too high: " + Long.toString(timestamp), timestamp < 2000L + shiftMs + variationMs);
			timestamp = fetchAttribute(helperBar, "timestamp", "/bar");
			assertTrue("bar agent: /bar timestamp too low: " + Long.toString(timestamp), timestamp > 11000L + shiftMs - variationMs);
			assertTrue("bar agent: /bar timestamp too high: " + Long.toString(timestamp), timestamp < 11000L + shiftMs + variationMs);
		} finally {
			helperFoo.getExecutor().stop();
			helperBar.getExecutor().stop();
			// Allow sockets to be freed
			Thread.sleep(100);
		}
	}
}
