package pl.edu.mimuw.cloudatlas.module;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import org.junit.Test;

import pl.edu.mimuw.cloudatlas.agent.Executor;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.CommunicationMessage;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.Message;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.PingMessage;
import pl.edu.mimuw.cloudatlas.model.PathName;
import pl.edu.mimuw.cloudatlas.model.TypePrimitive;
import pl.edu.mimuw.cloudatlas.model.Value;
import pl.edu.mimuw.cloudatlas.model.ValueContact;
import pl.edu.mimuw.cloudatlas.model.ValueList;
import pl.edu.mimuw.cloudatlas.model.ZMI;


/**
 * Helper class for UDP communication test.
 * It sends a message to itself using UDP.
 */
class UdpPingerModule extends Module {

	private String received;
	private CountDownLatch latch;

	public UdpPingerModule(Executor executor, Level loggingLevel, CountDownLatch latch) {
		super(executor, loggingLevel);
		received = "";
		this.latch = latch;
	}

	@Override
	public String getModuleType() {
		return "__pinger__";
	}

	@Override
	protected void handlePing(PingMessage pingMessage) {
		if (pingMessage.hasContent()) {
			received = pingMessage.getContent();
			latch.countDown();
		}
	}

	public void doPing(String targetAgent) {
		// Prepare wrapped ping message
		PingMessage wrappedMessageBody =
			PingMessage.newBuilder()
				.setContent("success")
				.build();
		Message wrappedMessage =
			Message.newBuilder()
				.setDestinationModule("__pinger__")
				.setPingMessage(wrappedMessageBody)
				.build();
		// Prepare wrapping timer message
		CommunicationMessage wrappingMessageBody =
			CommunicationMessage.newBuilder()
				.setAgentId(targetAgent)
				.setSubmessage(wrappedMessage)
				.build();
		Message wrappingMessage =
			Message.newBuilder()
				.setSourceModule("__pinger__")
				.setDestinationModule("udp_communication")
				.setCommunicationMessage(wrappingMessageBody)
				.build();
		// Enqueue the message
		this.executor.enqueue(wrappingMessage);
	}

	public void setReceived(String received) {
		this.received = received;
	}

	public String getReceived() {
		return received;
	}
}

class UdpExecutionHelper {
	private ZMI root;
	private Executor executor;
	@SuppressWarnings("unused")
	private TimerModule timerModule;
	@SuppressWarnings("unused")
	private UdpCommunicationModule udpCommunicationModule;
	@SuppressWarnings("unused")
	private ZmiModule zmiModule;
	private UdpPingerModule pingerModule;
	private CountDownLatch latch;

	private ValueContact createContact(String path, byte ip1, byte ip2, byte ip3, byte ip4)
			throws UnknownHostException {
		return new ValueContact(new PathName(path), InetAddress.getByAddress(new byte[] {
			ip1, ip2, ip3, ip4
		}));
	}

	private ZMI createTestHierarchy() throws ParseException, UnknownHostException {
		ValueContact selfContact = createContact("/", (byte)127, (byte)0, (byte)0, (byte)1);
		ValueContact aliasContact = createContact("/alias", (byte)127, (byte)0, (byte)0, (byte)1);
		List<Value> list;

		ZMI root = new ZMI();
		list = Arrays.asList(new Value[] {
				selfContact, aliasContact,
			});
		root.getAttributes().add("contacts", new ValueList(new ArrayList<Value>(list), TypePrimitive.CONTACT));

		return root;
	}

	public UdpExecutionHelper(int listenerPort, int remoteListenerPort, int senderPort)
			throws ParseException, IOException {
		latch = new CountDownLatch(1);
		root = createTestHierarchy();
		Level logLevel = Level.OFF;
		executor = new Executor(logLevel, null);
		// Timer module is required for communication retry
		timerModule = new TimerModule(executor, logLevel);
		zmiModule = new ZmiModule(
				executor, logLevel, root,
				null, 100000, 60000);
		udpCommunicationModule = new UdpCommunicationModule(
				executor, logLevel, "/", listenerPort, remoteListenerPort, senderPort);
		pingerModule = new UdpPingerModule(executor, logLevel, latch);
	}

	public CountDownLatch getLatch() {
		return latch;
	}

	public Executor getExecutor() {
		return executor;
	}

	public UdpPingerModule getPingerModule() {
		return pingerModule;
	}
}

public class UdpCommunicationModuleTest {
	/**
	 * Check if sending message from agent "/" to itself works.
	 */
	@Test
	public void testLoopback() throws IOException, InterruptedException, ParseException {
		UdpExecutionHelper helper = new UdpExecutionHelper(9100, 9100, 9101);
		helper.getPingerModule().setReceived("failure");
		(new Thread(helper.getExecutor())).start();
		try {
			Thread.sleep(10);
			helper.getPingerModule().doPing("/");
			helper.getLatch().await(1000L, TimeUnit.MILLISECONDS);
			assertEquals("success", helper.getPingerModule().getReceived());
		} finally {
			helper.getExecutor().stop();
			// Allow sockets to be freed
			Thread.sleep(100);
		}
	}

	/**
	 * Check if sending message from one host to another works.
	 */
	@Test
	public void testLocalUdp() throws IOException, InterruptedException, ParseException {
		UdpExecutionHelper helper = new UdpExecutionHelper(9200, 9200, 9201);
		helper.getPingerModule().setReceived("failure");
		(new Thread(helper.getExecutor())).start();
		try{
			Thread.sleep(10);
			// Essentially, "/" and "/alias" point to the same IP address,
			// but communication module treats them as a separate agents.
			helper.getPingerModule().doPing("/alias");
			helper.getLatch().await(1000L, TimeUnit.MILLISECONDS);
			assertEquals("success", helper.getPingerModule().getReceived());
		} finally {
			helper.getExecutor().stop();
			// Allow sockets to be freed
			Thread.sleep(100);
		}
	}

	/**
	 * Check if sending message between two UDP modules works.
	 */
	@Test
	public void testDoubleUdp() throws IOException, InterruptedException, ParseException {
		UdpExecutionHelper helperA = new UdpExecutionHelper(9300, 9301, 9310);
		UdpExecutionHelper helperB = new UdpExecutionHelper(9301, 9300, 9311);
		helperA.getPingerModule().setReceived("nothing");
		helperB.getPingerModule().setReceived("failure");
		(new Thread(helperA.getExecutor())).start();
		(new Thread(helperB.getExecutor())).start();
		try {
			Thread.sleep(10);
			helperA.getPingerModule().doPing("/alias");
			helperB.getLatch().await(1000L, TimeUnit.MILLISECONDS);
			assertEquals("nothing", helperA.getPingerModule().getReceived());
			assertEquals("success", helperB.getPingerModule().getReceived());
		} finally {
			helperA.getExecutor().stop();
			helperB.getExecutor().stop();
			// Allow sockets to be freed
			Thread.sleep(100);
		}
	}
}
