package pl.edu.mimuw.cloudatlas.module;

import org.junit.Test;

import pl.edu.mimuw.cloudatlas.agent.Executor;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.Message;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.PingMessage;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.TimerMessage;

import static org.junit.Assert.*;

import org.junit.Before;

import java.util.ArrayList;
import java.util.logging.Level;

import static java.util.Arrays.asList;

/**
 * Helper class for timer module test.
 * It sends recursively a delayed message to itself - the delay is made by Timer module.
 */
class PingerModule extends Module {

	private TimerModuleTest timerModuleTest;

	public PingerModule(Executor executor, Level loggingLevel, TimerModuleTest timerModuleTest) {
		super(executor, loggingLevel);
		this.timerModuleTest = timerModuleTest;
	}

	@Override
	public String getModuleType() {
		return "__pinger__";
	}

	@Override
	protected void handlePing(PingMessage pingMessage) {
		String content = "";
		if (pingMessage.hasContent()) {
			content = pingMessage.getContent();
		}
		int countdown = Integer.parseInt(content);
		timerModuleTest.addResult(countdown);
		if (countdown > 0) {
			doPing(countdown - 1);
		}
	}

	public void doPing(int countdown) {
		// Prepare wrapped ping message
		PingMessage wrappedMessageBody =
			PingMessage.newBuilder()
				.setContent(new Integer(countdown).toString())
				.build();
		Message wrappedMessage =
			Message.newBuilder()
				.setDestinationModule("__pinger__")
				.setPingMessage(wrappedMessageBody)
				.build();
		// Prepare wrapping timer message
		TimerMessage wrappingMessageBody =
			TimerMessage.newBuilder()
				.setMilliseconds(10)
				.setSubmessage(wrappedMessage)
				.build();
		Message wrappingMessage =
			Message.newBuilder()
				.setSourceModule("__pinger__")
				.setDestinationModule("timer")
				.setTimerMessage(wrappingMessageBody)
				.build();
		// Enqueue the message
		this.executor.enqueue(wrappingMessage);
	}
}


public class TimerModuleTest {

	protected ArrayList<Integer> results;
	protected Executor executor;
	protected TimerModule timerModule;
	protected PingerModule pingerModule;

	public void addResult(int result) {
		results.add(result);
	}

	@Before
	public void setUp() {
		results = new ArrayList<Integer>();
		executor = new Executor(Level.OFF, null);
		timerModule = new TimerModule(executor, Level.OFF);
		pingerModule = new PingerModule(executor, Level.OFF, this);
	}

	@Test
	public void testTimerWithPinger() throws InterruptedException {
		// TODO(pathes): mock Timer to avoid flakiness of the test and speed it up
		(new Thread(executor)).start();
		Thread.sleep(10);
		pingerModule.doPing(5);
		Thread.sleep(100);
		assertEquals(6, results.size());
		assertEquals(
			new ArrayList<Integer>(asList(5, 4, 3, 2, 1, 0)),
			results);
		executor.stop();
	}
}
