package pl.edu.mimuw.cloudatlas.module;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import pl.edu.mimuw.cloudatlas.agent.Executor;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.CommunicationMessage;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.FallbackContactsUpdateRequestMessage;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.Message;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.PingMessage;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.ZmiRequestMessage;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.ZmiResponseMessage;
import pl.edu.mimuw.cloudatlas.model.CloudAtlasModels.ValueContactProto;
import pl.edu.mimuw.cloudatlas.model.CloudAtlasModels.ZmiProto;
import pl.edu.mimuw.cloudatlas.model.PathName;
import pl.edu.mimuw.cloudatlas.model.ValueContact;
import pl.edu.mimuw.cloudatlas.model.ZMI;
import pl.edu.mimuw.cloudatlas.module.gossip.GossipStrategy;

public abstract class GossipModule extends Module {

	// Map from string pathname to counter how many times particular host didn't respond
	protected Map<ValueContact, Integer> noResponseTimeCounter;
	protected ZMI rootZmi;
	protected PathName pathname;
	protected GossipStrategy gossipStrategy;
	protected int gossipIntervalMs;
	protected List<ValueContact> emergencyContacts;

	// Minimal list of required attributes for gossip module to work.
	// Rationale:
	// name - needed for knowing which node is which
	// timestamp - needed for creating the freshness tree
	// contacts - needed for gossiping strategy to work
	protected final static List<String> MINIMAL_ATTRIBUTE_LIST = Arrays.asList("name", "timestamp", "contacts");


	public GossipModule(
			Executor executor, Level loggingLevel, PathName pathname,
			ZMI zmi, GossipStrategy gossipStrategy, int gossipIntervalMs,
			List<ValueContact> emergencyContacts) {

		super(executor, loggingLevel);
		// In order for gossiping strategy to work, we need to know our name.
		this.pathname = pathname;
		this.rootZmi = zmi;
		this.gossipStrategy = gossipStrategy;
		this.gossipIntervalMs = gossipIntervalMs;
		this.noResponseTimeCounter = new HashMap<ValueContact, Integer>();
		this.emergencyContacts = emergencyContacts;
	}

	@Override
	public void run() {
		// Schedule first gossiping
		schedulePing(gossipIntervalMs);
	}

	/**
	 * Determines which attributes to fetch from ZmiModule.
	 * @return List of required attributes' names.
	 */
	protected List<String> requiredAttributeList() {
		return MINIMAL_ATTRIBUTE_LIST;
	}

	/**
	 * @return Max number of gossip retries.
	 */
	protected int maxRetries() {
		return 5;
	}

	public void setGossipStrategy(GossipStrategy gossipStrategy) {
		this.gossipStrategy = gossipStrategy;
	}

	@Override
	protected void handlePing(PingMessage pingMessage) {
		requestZmiUpdate();
		schedulePing(gossipIntervalMs);
	}

	@Override
	protected void handleZmiResponse(ZmiResponseMessage zmiResponseMessage) {
		ZmiProto responseZmiProto = zmiResponseMessage.getZmi();
		rootZmi = ZMI.fromProto(responseZmiProto);
		// Start the gossiping at the moment we have relatively fresh ZMI
		startGossipWithScheduled();
	}

	private void requestZmiUpdate() {
		ZmiRequestMessage zmiRequestBody = ZmiRequestMessage.newBuilder()
			.setPathname("/")
			.setDepthLimit(-1)
			.addAllAttribute(requiredAttributeList())
			.build();
		Message zmiRequest = Message.newBuilder()
			.setZmiRequestMessage(zmiRequestBody)
			.setSourceModule(getModuleType())
			.setDestinationModule("zmi")
			.build();
		executor.enqueue(zmiRequest);
	}

	/**
	 * Gossip with all scheduled contacts.
	 */
	private void startGossipWithScheduled() {
		List<ValueContact> targetsToRemove = new ArrayList<ValueContact>();
		// Gossip with existing targets
		for (Map.Entry<ValueContact, Integer> entry : noResponseTimeCounter.entrySet()) {
			int count = entry.getValue();
			count++;
			if (count > maxRetries()) {
				targetsToRemove.add(entry.getKey());
				continue;
			}
			// Not thrown away? Update and gossip
			entry.setValue(count);
			logger.info("Initiating gossip with " + entry.getKey());
			startGossipWith(entry.getKey());
		}
		// Remove too old targets
		for (ValueContact removedTarget : targetsToRemove) {
			noResponseTimeCounter.remove(removedTarget);
		}
		// Gossip with the new target
		ValueContact newTarget = gossipStrategy.pickTarget(rootZmi, pathname, emergencyContacts);
		if (newTarget != null) {
			noResponseTimeCounter.put(newTarget, 0);
			logger.info("Initiating gossip with " + newTarget);
			startGossipWith(newTarget);
		}
	}

	/**
	 * Gossip with one particular agent.
	 * @param target ValueContact representing gossip target agent.
	 */
	protected abstract void startGossipWith(ValueContact target);

	/**
	 * Generic CommunicationMessage builder.
	 * @param targetAddress Address of target agent.
	 * @param submessage Submessage carried by UDP.
	 * @return CommunicationMessage instance ready for enqueue.
	 */
	protected Message buildCommunicationMessage(InetAddress targetAddress, Message submessage) {
		CommunicationMessage communicationMessageBody =
			CommunicationMessage.newBuilder()
				.setInetAddress(targetAddress.getCanonicalHostName())
				.setSubmessage(submessage)
				.build();
		Message communicationMessage =
			Message.newBuilder()
				.setSourceModule(getModuleType())
				.setDestinationModule("udp_communication")
				.setCommunicationMessage(communicationMessageBody)
				.build();
		return communicationMessage;
	}

	@Override
	protected void handleFallbackContactsUpdateRequest(FallbackContactsUpdateRequestMessage fallbackContactsUpdateRequestMessage) {
		logger.info("Setting fallback contacts");
		List<ValueContact> newContacts = new ArrayList<ValueContact>();
		for (ValueContactProto contactProto : fallbackContactsUpdateRequestMessage.getContactList()) {
			newContacts.add(new ValueContact(
					contactProto.getPathName(),
					contactProto.getInetAddress()));
		}
		emergencyContacts = newContacts;
	}
}
