package pl.edu.mimuw.cloudatlas.module;


import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import java.util.ArrayList;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;


import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import com.google.protobuf.InvalidProtocolBufferException;

import pl.edu.mimuw.cloudatlas.RMIinterface.ClientInterface;
import pl.edu.mimuw.cloudatlas.RMIinterface.GetAttributesFromZoneClientResponse;
import pl.edu.mimuw.cloudatlas.RMIinterface.RegisterQueryAgentResponse;
import pl.edu.mimuw.cloudatlas.RMIinterface.SetAttributeClientResponse;
import pl.edu.mimuw.cloudatlas.agent.Executor;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.FallbackContactsUpdateRequestMessage;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.Message;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.PingMessage;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.QuerySignerSignRequestMessage;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.QueryUpdateRequestMessage;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.ZmiRequestMessage;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.ZmiResponseMessage;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.ZmiUpdateRequestMessage;
import pl.edu.mimuw.cloudatlas.model.CloudAtlasModels.AttributesMapEntryProto;
import pl.edu.mimuw.cloudatlas.model.CloudAtlasModels.QueryProto;
import pl.edu.mimuw.cloudatlas.model.CloudAtlasModels.QueryProtoList;
import pl.edu.mimuw.cloudatlas.model.CloudAtlasModels.SetAttributeProto;
import pl.edu.mimuw.cloudatlas.model.PathName;
import pl.edu.mimuw.cloudatlas.model.TreeTraversalException;
import pl.edu.mimuw.cloudatlas.model.ValueContact;
import pl.edu.mimuw.cloudatlas.model.ZMI;
import pl.edu.mimuw.cloudatlas.model.CloudAtlasModels.ValueContactProto;
import pl.edu.mimuw.cloudatlas.model.CloudAtlasModels.ZmiProto;
import pl.edu.mimuw.cloudatlas.querysigner.QuerySignerSignRequest;

public class ClientModule extends Module {

	protected ZMI rootZmi;
	protected List<ValueContact> fallbackContacts;
	protected List<QueryProto> queries;

	protected int refreshZMIIntervalMs;
	protected ClientServer serverClientObject;
    protected final static String DIGEST_ALGORITHM = "SHA-256";
    protected final static String ENCRYPTION_ALGORITHM = "RSA";
    protected PublicKey publicKeyQuerySigner;


	/**
	 *
	 * @author Paweł Kamiński
	 * Client server should be available from ClientModule
	 * Client Server should be considered as RMI ending of Client Module
	 */
	public class ClientServer implements ClientInterface {

		@Override
		public GetAttributesFromZoneClientResponse getAttributesFromZone(
				String zmiPath) throws RemoteException {


			logger.log(Level.INFO, "Client get attributes from zone:" + zmiPath + " called");
			ZMI localZmi = rootZmi;
			PathName senderPathname;

			try {
				senderPathname = new PathName(zmiPath);
			} catch (IllegalArgumentException e) {
				logger.log(Level.SEVERE, "Bad Path");
				return new GetAttributesFromZoneClientResponse(
						GetAttributesFromZoneClientResponse.ClientResponse.BAD_PATH);
			}

			if (localZmi == null) {
				logger.log(Level.SEVERE, "Not Kept");
				return new GetAttributesFromZoneClientResponse(
						GetAttributesFromZoneClientResponse.ClientResponse.NOT_KEPT);
			}

			ZMI zmiAtLevel = null;
			try {
				zmiAtLevel = localZmi.findDescendant(senderPathname, false);
			} catch (TreeTraversalException e) {
				logger.log(Level.SEVERE, e.getMessage(), e.getStackTrace());
			}
			if (zmiAtLevel == null) {
				logger.log(Level.SEVERE, "Not Kept");
				return new GetAttributesFromZoneClientResponse(
						GetAttributesFromZoneClientResponse.ClientResponse.NOT_KEPT);
			}

			logger.log(Level.INFO, "Client get attributes finished");

			return new GetAttributesFromZoneClientResponse(
					GetAttributesFromZoneClientResponse.ClientResponse.OK,
					zmiAtLevel.toProto(null, 0).toByteArray());
		}

		/**
		 * Assumption that java serialization for Set of strings is good enough.
		 */
		@Override
		public Set<String> getAllZonesPathnames() throws RemoteException {
			logger.log(Level.INFO, "Client get all zones path names called");

			ZMI localZmi = rootZmi;
			HashSet<String> zones = new HashSet<String>();
			zones.add("/");
			receiveZmiZones(zones, localZmi);

			return zones;
		}

		private void receiveZmiZones(HashSet<String> zones, ZMI zmi) {
			for (ZMI son: zmi.getSons()){
				receiveZmiZones(zones, son);
			}
			String zone = zmi.getPathName().getName();
			if (zone != "") {
				zones.add(zone);
			}
		}

		@Override
		public List<ValueContact> getFallbackContacts() throws RemoteException {
			return fallbackContacts;
		}

		@Override
		public Boolean setFallbackContacts(List<ValueContact> contacts)
				throws RemoteException {
			// Set contacts locally
			fallbackContacts = contacts;
			// Set contacts in gossip modules
			updateFallbackContacts("zmi_gossip", contacts);
			updateFallbackContacts("query_gossip", contacts);
			return true;
		}

		@Override
		public SetAttributeClientResponse setAttribute(byte[] encodedSetAttributeProto) throws RemoteException {
			logger.log(Level.INFO, "Client set attribute called");

			ZMI localZmi = rootZmi;
			PathName senderPathname;

			SetAttributeProto setAttributeProto;
			try {
				setAttributeProto = SetAttributeProto.parseFrom(encodedSetAttributeProto);
			} catch (InvalidProtocolBufferException e) {
				logger.log(
						Level.SEVERE,
						"SetAttributeProto parse error: " + e.getMessage(),
						e.getStackTrace());
				return new SetAttributeClientResponse(
						SetAttributeClientResponse.ClientResponse.PARSE_ERROR);
			}

			try {
				senderPathname = new PathName(setAttributeProto.getPathname());
			} catch (IllegalArgumentException e) {
				logger.log(Level.SEVERE, "Bad Path");
				return new SetAttributeClientResponse(
						SetAttributeClientResponse.ClientResponse.BAD_PATH);
			}

			ZMI zmiAtLevel = null;
			try {
				zmiAtLevel = localZmi.findDescendant(senderPathname, false);
			} catch (TreeTraversalException e) {
				logger.log(Level.SEVERE, e.getMessage(), e.getStackTrace());
			}
			if (zmiAtLevel == null) {
				logger.log(Level.SEVERE, "Not Kept");
				return new SetAttributeClientResponse(
						SetAttributeClientResponse.ClientResponse.NOT_KEPT);
			}

			if (!zmiAtLevel.getSons().isEmpty()) {
				logger.log(Level.SEVERE, "Not Leaf");
				return new SetAttributeClientResponse(
						SetAttributeClientResponse.ClientResponse.NOT_LEAF);
			}

			ZmiUpdateRequestMessage zmiUpdateRequestMessageBody =
					ZmiUpdateRequestMessage.newBuilder()
						.setPathname(setAttributeProto.getPathname())
						.addAttributesMapEntries(AttributesMapEntryProto.newBuilder()
								.setKey(setAttributeProto.getKey())
								.setValue(setAttributeProto.getValue())
								.build())
						.build();

			Message zmiUpdateRequestMessage =
					Message.newBuilder()
						.setSourceModule(getModuleType())
						.setDestinationModule("zmi")
						.setZmiUpdateRequestMessage(zmiUpdateRequestMessageBody)
						.build();

			executor.enqueue(zmiUpdateRequestMessage);
			logger.log(Level.INFO, "Client set attribute enqueued");

			return new SetAttributeClientResponse(SetAttributeClientResponse.ClientResponse.OK);
		}


		@Override
		public RegisterQueryAgentResponse registerQuery(byte[] encodedQueryProto) throws RemoteException {

			QueryProto queryProto;
			try {
				queryProto = QueryProto.parseFrom(encodedQueryProto);
			} catch (InvalidProtocolBufferException e) {
				logger.log(
						Level.SEVERE,
						"QueryProto parse error: " + e.getMessage(),
						e.getStackTrace());
				return new RegisterQueryAgentResponse(
						RegisterQueryAgentResponse.AgentResponse.PARSE_ERROR);
			}

			QuerySignerSignRequest request = new QuerySignerSignRequest();
			request.setQueryName(queryProto.getName());
			request.setQueryContent(queryProto.getContent());
			queryProto.getSignature();

			// hash request
			QuerySignerSignRequestMessage querySignerSignRequestProto =
					QuerySignerSignRequestMessage.newBuilder()
						.setName(request.getQueryName())
						.setValue(request.getQueryContent())
						.build();

			byte[] serialized = querySignerSignRequestProto.toByteArray();
			byte[] decryptedSignature, digest;


			try {
	            MessageDigest digestGenerator =
	                    MessageDigest.getInstance(DIGEST_ALGORITHM);
	            digest = digestGenerator.digest(serialized);
	        } catch (NoSuchAlgorithmException e) {
	        	logger.log(Level.SEVERE, "No "+ DIGEST_ALGORITHM + " Algorithm.");
	            e.printStackTrace();
	            return new RegisterQueryAgentResponse(RegisterQueryAgentResponse.AgentResponse.BAD_ALGORITHM);
	        }


			//RSA
			decryptedSignature = queryProto.getSignature().toByteArray();

			try {
				Cipher verifyCipher = Cipher.getInstance(ENCRYPTION_ALGORITHM);
				verifyCipher.init(Cipher.DECRYPT_MODE, publicKeyQuerySigner);
				decryptedSignature = verifyCipher.doFinal(decryptedSignature);
			} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
				logger.log(Level.SEVERE, "No "+ ENCRYPTION_ALGORITHM + " algorithm.");
				e.printStackTrace();
			} catch (IllegalBlockSizeException | BadPaddingException e) {
				logger.log(Level.SEVERE, ENCRYPTION_ALGORITHM + " signature error.");
				e.printStackTrace();
			} catch (InvalidKeyException e) {
				logger.log(Level.SEVERE, ENCRYPTION_ALGORITHM + " key error.");
				e.printStackTrace();
			}

			if (!Arrays.equals(digest, decryptedSignature)) {
				logger.log(Level.SEVERE, "Query signature and query value doesn't match.");
				return new RegisterQueryAgentResponse(RegisterQueryAgentResponse.AgentResponse.BAD_SIGNATURE);
			}

			// Install query - pass it to QueryGossipModule with APPEND mode
			Message updateRequestMessage = buildUpdateRequestMessage(Arrays.asList(queryProto), "query_gossip");
			executor.enqueue(updateRequestMessage);

			logger.log(Level.INFO, "Query passed to installation.");
			return new RegisterQueryAgentResponse(RegisterQueryAgentResponse.AgentResponse.OK);
		}

		@Override
		public byte[] getQueries() throws RemoteException {
			QueryProtoList.Builder builder = QueryProtoList.newBuilder();
			for (QueryProto query : queries) {
				// Client should be not interested in deleted queries (i.e. those with empty content)
				if (query.getContent().length() > 0) {
					// They also shouldn't get signatures, as they're not JSON-friendly
					builder.addQuery(query.toBuilder().clearSignature().build());
				}
			}
			return builder.build().toByteArray();
		}

		private Message buildUpdateRequestMessage(List<QueryProto> queries, String targetModule) {
			QueryUpdateRequestMessage requestMessageBody =
				QueryUpdateRequestMessage.newBuilder()
					.addAllQuery(queries)
					.build();
			Message requestMessage =
				Message.newBuilder()
					.setSourceModule(getModuleType())
					.setDestinationModule(targetModule)
					.setQueryUpdateRequestMessage(requestMessageBody)
					.build();
			return requestMessage;
		}

	}

	public ClientModule(
			Executor executor, Level loggingLevel, ZMI zmi,
			List<ValueContact> fallbackContacts,
			List<QueryProto> queries,
			String querySignerPublicKeyFile) {

		super(executor, loggingLevel);

		// set zmi update interval as at least 4s
		this.refreshZMIIntervalMs = 4000;
		// get the public key

		try {
			// http://stackoverflow.com/questions/11410770/load-rsa-public-key-from-file
			File f = new File(querySignerPublicKeyFile);
			FileInputStream fis = new FileInputStream(f);
			DataInputStream dis = new DataInputStream(fis);
			byte[] keyBytes = new byte[(int)f.length()];
			dis.readFully(keyBytes);
			dis.close();

			X509EncodedKeySpec spec =
					new X509EncodedKeySpec(keyBytes);
			KeyFactory kf = KeyFactory.getInstance("RSA");
			publicKeyQuerySigner = kf.generatePublic(spec);
		} catch (IOException | NoSuchAlgorithmException | InvalidKeySpecException e) {
			logger.log(
				Level.SEVERE,
				"RSA Query Signer exception: " + e.getMessage(),
				e.getStackTrace());
		}

		this.rootZmi = zmi;
		this.queries = queries;

		if (fallbackContacts != null) {
			this.fallbackContacts = fallbackContacts;
		} else {
			this.fallbackContacts = new ArrayList<ValueContact>();
		}

		serverClientObject = new ClientServer();
	}

	@Override
	public void run() {
		// Putting RMI data to server requires security manager
		if (System.getSecurityManager() == null) {
			System.setSecurityManager(new SecurityManager());
		}

		try {
			ClientInterface clientStub =
					 (ClientInterface) UnicastRemoteObject.exportObject(serverClientObject, 3377);
			Registry registry = LocateRegistry.getRegistry();
			registry.rebind("Client", clientStub);
			logger.log(Level.INFO, "Client bounded");
			schedulePing(refreshZMIIntervalMs);
		} catch (Exception e) {
			System.err.println("Client server exception:");
			e.printStackTrace();
		}
	}

	@Override
	public String getModuleType() {
		return "client";
	}

	@Override
	protected void handlePing(PingMessage pingMessage) {
		requestZmiUpdate();
		schedulePing(refreshZMIIntervalMs);
	}

	@Override
	protected void handleZmiResponse(ZmiResponseMessage zmiResponseMessage) {
		ZmiProto responseZmiProto = zmiResponseMessage.getZmi();
		rootZmi = ZMI.fromProto(responseZmiProto);
	}

	@Override
	protected void handleQueryUpdateRequest(QueryUpdateRequestMessage queryUpdateRequestMessage) {
		queries = queryUpdateRequestMessage.getQueryList();
	}

	private void requestZmiUpdate() {
		ZmiRequestMessage zmiRequestBody = ZmiRequestMessage.newBuilder()
			.setPathname("/")
			.setDepthLimit(-1)
			.build();
		Message zmiRequest = Message.newBuilder()
			.setZmiRequestMessage(zmiRequestBody)
			.setSourceModule(getModuleType())
			.setDestinationModule("zmi")
			.build();
		executor.enqueue(zmiRequest);
	}

	private void updateFallbackContacts(String targetModule, List<ValueContact> contacts) {
		List<ValueContactProto> protoList = new ArrayList<ValueContactProto>();
		for (ValueContact contact : contacts) {
			protoList.add(contact.toValueContactProto());
		}
		FallbackContactsUpdateRequestMessage requestBody = FallbackContactsUpdateRequestMessage.newBuilder()
			.addAllContact(protoList)
			.build();
		Message request = Message.newBuilder()
			.setFallbackContactsUpdateRequestMessage(requestBody)
			.setSourceModule(getModuleType())
			.setDestinationModule(targetModule)
			.build();
		executor.enqueue(request);
	}

}
