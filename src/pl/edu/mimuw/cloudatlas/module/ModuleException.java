package pl.edu.mimuw.cloudatlas.module;

@SuppressWarnings("serial")
public class ModuleException extends RuntimeException {
	public ModuleException(String message) {
		super(message);
	}
}
