package pl.edu.mimuw.cloudatlas.module;

import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;

import pl.edu.mimuw.cloudatlas.agent.Executor;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.Message;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.TimerMessage;

public class TimerModule extends Module {

	protected Timer timer;

	public TimerModule(Executor executor, Level loggingLevel) {
		super(executor, loggingLevel);
	}

	@Override
	public String getModuleType() {
		return "timer";
	}

	private class TimerModuleTask extends TimerTask {
		private Message message;

		public TimerModuleTask(Message message) {
			this.message = message;
		}

		@Override
		public void run() {
			TimerModule.this.executor.enqueue(this.message);
		}
	}

	@Override
	protected void handleTimer(TimerMessage timerMessage) {
		Message callbackMessage = timerMessage.getSubmessage();
		if (callbackMessage == null) {
			throw new ModuleException("Undefined timer callback message");
		}
		int delay;
		if (timerMessage.hasMilliseconds()) {
			delay = timerMessage.getMilliseconds();
		} else {
			delay = 1000;
		}
		logger.info("delaying " + Integer.toString(delay) + "ms");
		logger.fine("\n" + callbackMessage.toString());
		this.timer.schedule(new TimerModuleTask(callbackMessage), delay);
	}

	@Override
	public void run() {
		this.timer = new Timer();
	}

	@Override
	public void stop() {
		timer.cancel();
	}
}
