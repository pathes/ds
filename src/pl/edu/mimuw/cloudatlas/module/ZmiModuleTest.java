package pl.edu.mimuw.cloudatlas.module;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;

import org.junit.Before;
import org.junit.Test;

import pl.edu.mimuw.cloudatlas.agent.Executor;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.Message;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.QueryUpdateRequestMessage;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.ZmiRequestMessage;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.ZmiResponseMessage;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.ZmiUpdateRequestMessage;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.ZmiResponseMessage.ZmiResponseStatus;
import pl.edu.mimuw.cloudatlas.model.Attribute;
import pl.edu.mimuw.cloudatlas.model.AttributesMap;
import pl.edu.mimuw.cloudatlas.model.CloudAtlasModels.AttributesMapEntryProto;
import pl.edu.mimuw.cloudatlas.model.CloudAtlasModels.QueryProto;
import pl.edu.mimuw.cloudatlas.model.CloudAtlasModels.ZmiProto;
import pl.edu.mimuw.cloudatlas.model.ValueInt;
import pl.edu.mimuw.cloudatlas.model.ValueString;
import pl.edu.mimuw.cloudatlas.model.ValueTime;
import pl.edu.mimuw.cloudatlas.model.ZMI;

/**
 * Helper class for ZMI test.
 */
class ZmiHelperModule extends Module {

	private ZmiResponseStatus responseStatus;
	private ZmiProto responseZmiProto;

	public ZmiHelperModule(Executor executor, Level loggingLevel) {
		super(executor, loggingLevel);
	}

	@Override
	public String getModuleType() {
		return "__zmi_test__";
	}

	@Override
	protected void handleZmiResponse(ZmiResponseMessage zmiResponseMessage) {
		responseStatus = zmiResponseMessage.getStatus();
		responseZmiProto = zmiResponseMessage.getZmi();
	}

	public void setAttributes(String pathname, List<AttributesMapEntryProto> entries) {
		ZmiUpdateRequestMessage updateMessageBody =
			ZmiUpdateRequestMessage.newBuilder()
				.setPathname(pathname)
				.addAllAttributesMapEntries(entries)
				.build();
		Message updateMessage =
				Message.newBuilder()
					.setDestinationModule("zmi")
					.setZmiUpdateRequestMessage(updateMessageBody)
					.build();
		this.executor.enqueue(updateMessage);
	}

	public void fetchAttributes(String pathname, boolean recursive, List<String> attributes) {
		ZmiRequestMessage fetchMessageBody =
			ZmiRequestMessage.newBuilder()
				.setPathname(pathname)
				.setDepthLimit(-1)
				.addAllAttribute(attributes)
				.build();
		Message fetchMessage =
			Message.newBuilder()
				.setSourceModule("__zmi_test__")
				.setDestinationModule("zmi")
				.setZmiRequestMessage(fetchMessageBody)
				.build();
		this.executor.enqueue(fetchMessage);
	}

	public void updateQueries(List<QueryProto> queries) {
		QueryUpdateRequestMessage requestMessageBody =
			QueryUpdateRequestMessage.newBuilder()
				.addAllQuery(queries)
				.build();
		Message requestMessage =
			Message.newBuilder()
				.setSourceModule("__zmi_test__")
				.setDestinationModule("zmi")
				.setQueryUpdateRequestMessage(requestMessageBody)
				.build();
		this.executor.enqueue(requestMessage);
	}

	public ZmiResponseStatus getResponseStatus() {
		return responseStatus;
	}

	public ZmiProto getZmiProto() {
		return responseZmiProto;
	}
}


public class ZmiModuleTest {

	private ZMI root;
	private Executor executor;
	@SuppressWarnings("unused")
	private TimerModule timerModule;
	@SuppressWarnings("unused")
	private ZmiModule zmiModule;
	private ZmiHelperModule zmiHelperModule;

	private ZMI createTestHierarchy() {
		// Create /
		ZMI root = new ZMI();
		// Create /foo
		ZMI foo = new ZMI(root);
		root.addSon(foo);
		foo.getAttributes().add("name", new ValueString("foo"));
		// Create /bar
		ZMI bar = new ZMI(root);
		root.addSon(bar);
		bar.getAttributes().add("name", new ValueString("bar"));

		return root;
	}

	@Before
	public void setUp() {
		root = createTestHierarchy();
		Level logLevel = Level.OFF;
		executor = new Executor(logLevel, null);
		// Timer module is required for communication retry
		timerModule = new TimerModule(executor, logLevel);
		zmiModule = new ZmiModule(
				executor, logLevel, root,
				null, 100, 60000);
		zmiHelperModule = new ZmiHelperModule(executor, logLevel);
	}

	@Test
	public void testAddAttribute() throws InterruptedException {
		// We need to set ZMI fresh, so that they don't get purged.
		long nowMs = System.currentTimeMillis();
		(new Thread(executor)).start();
		// Set attribute
		AttributesMap attributesMap = new AttributesMap();
		attributesMap.add(new Attribute("answer"), new ValueInt(42L));
		attributesMap.add(new Attribute("timestamp"), new ValueTime(nowMs));
		zmiHelperModule.setAttributes("/", attributesMap.entryProtoList());
		// Get attribute
		zmiHelperModule.fetchAttributes("/", false, Arrays.asList("answer"));
		// TODO(pathes): is it possible to introduce lock / countdown latch?
		Thread.sleep(50);
		// Check if response matches
		assertEquals(ZmiResponseStatus.OK, zmiHelperModule.getResponseStatus());
		ZmiProto zmiProto = zmiHelperModule.getZmiProto();
		List<AttributesMapEntryProto> attributes = zmiProto.getAttributesMapEntriesList();
		assertEquals(1, attributes.size());
		ZMI zmi = ZMI.fromProto(zmiProto);
		assertEquals(42L, ((ValueInt) zmi.getAttributes().get("answer")).getValue().longValue());
		executor.stop();
	}

	@Test
	public void testRecursive() throws InterruptedException {
		// We need to set ZMI fresh, so that they don't get purged.
		long nowMs = System.currentTimeMillis();
		(new Thread(executor)).start();
		AttributesMap attributesMap;
		List<AttributesMapEntryProto> attributes;

		// Set attribute in /
		attributesMap = new AttributesMap();
		zmiHelperModule.setAttributes("/", attributesMap.entryProtoList());
		// Set attribute in /foo
		attributesMap = new AttributesMap();
		attributesMap.add(new Attribute("name"), new ValueString("foo"));
		attributesMap.add(new Attribute("timestamp"), new ValueTime(nowMs));
		zmiHelperModule.setAttributes("/foo", attributesMap.entryProtoList());
		// Set attribute in /bar
		attributesMap = new AttributesMap();
		attributesMap.add(new Attribute("name"), new ValueString("bar"));
		attributesMap.add(new Attribute("timestamp"), new ValueTime(nowMs));
		zmiHelperModule.setAttributes("/bar", attributesMap.entryProtoList());
		// Get attributes of whole tree
		zmiHelperModule.fetchAttributes("/", true, Arrays.asList("name"));
		// TODO(pathes): is it possible to introduce lock / countdown latch?
		Thread.sleep(50);
		// Check if response matches
		assertEquals(ZmiResponseStatus.OK, zmiHelperModule.getResponseStatus());

		// Check /
		ZmiProto rootZmiProto = zmiHelperModule.getZmiProto();
		attributes = rootZmiProto.getAttributesMapEntriesList();
		assertEquals(0, attributes.size());

		// Check /foo
		ZmiProto fooZmiProto = rootZmiProto.getSons(0);
		attributes = fooZmiProto.getAttributesMapEntriesList();
		assertEquals(1, attributes.size());
		ZMI fooZmi = ZMI.fromProto(fooZmiProto);
		assertEquals("foo", ((ValueString) fooZmi.getAttributes().get("name")).getValue());

		// Check /bar
		ZmiProto barZmiProto = rootZmiProto.getSons(1);
		attributes = barZmiProto.getAttributesMapEntriesList();
		assertEquals(1, attributes.size());
		ZMI barZmi = ZMI.fromProto(barZmiProto);
		assertEquals("bar", ((ValueString) barZmi.getAttributes().get("name")).getValue());

		executor.stop();
	}

	@Test
	public void testRecalculate() throws InterruptedException {
		// We need to set ZMI fresh, so that they don't get purged.
		long nowMs = System.currentTimeMillis();
		(new Thread(executor)).start();
		AttributesMap attributesMap;

		// Set attribute in /
		attributesMap = new AttributesMap();
		attributesMap.add(new Attribute("timestamp"), new ValueTime(nowMs - 2000L));
		zmiHelperModule.setAttributes("/", attributesMap.entryProtoList());
		// Set query in /
		QueryProto sumN = QueryProto.newBuilder()
			.setName("sum_n")
			.setContent("SELECT sum(n) AS sum_n")
			.build();
		zmiHelperModule.updateQueries(Arrays.asList(sumN));
		// Set attribute in /foo
		attributesMap = new AttributesMap();
		attributesMap.add(new Attribute("name"), new ValueString("foo"));
		attributesMap.add(new Attribute("n"), new ValueInt(8L));
		attributesMap.add(new Attribute("timestamp"), new ValueTime(nowMs - 1000L));
		zmiHelperModule.setAttributes("/foo", attributesMap.entryProtoList());
		// Set attribute in /bar
		attributesMap = new AttributesMap();
		attributesMap.add(new Attribute("name"), new ValueString("bar"));
		attributesMap.add(new Attribute("n"), new ValueInt(13L));
		attributesMap.add(new Attribute("timestamp"), new ValueTime(nowMs));
		zmiHelperModule.setAttributes("/bar", attributesMap.entryProtoList());
		// Wait for recalculation
		Thread.sleep(500);
		// Get attributes of whole tree
		zmiHelperModule.fetchAttributes("/", true, Collections.<String>emptyList());
		// TODO(pathes): is it possible to introduce lock / countdown latch?
		Thread.sleep(100);
		// Check if response matches
		assertEquals(ZmiResponseStatus.OK, zmiHelperModule.getResponseStatus());

		// Check /
		ZmiProto rootZmiProto = zmiHelperModule.getZmiProto();
		ZMI rootZmi = ZMI.fromProto(rootZmiProto);
		// Check if `sum_n` attribute was synthesized
		assertEquals(21L, ((ValueInt) rootZmi.getAttributes().get("sum_n")).getValue().longValue());
		// Check if timestamp was updated with youngest children timestamp
		assertEquals(nowMs, rootZmi.getTimestamp().longValue());

		executor.stop();
	}

	@Test
	public void testPurge() throws InterruptedException {
		// We need to set some ZMI fresh, so that they don't get purged.
		long nowMs = System.currentTimeMillis();
		(new Thread(executor)).start();
		AttributesMap attributesMap;
		List<AttributesMapEntryProto> attributes;

		// Set attribute in /
		attributesMap = new AttributesMap();
		attributesMap.add(new Attribute("n"), new ValueInt(5L));
		attributesMap.add(new Attribute("timestamp"), new ValueTime(nowMs));
		zmiHelperModule.setAttributes("/", attributesMap.entryProtoList());
		// Set attribute in /foo
		attributesMap = new AttributesMap();
		attributesMap.add(new Attribute("name"), new ValueString("foo"));
		attributesMap.add(new Attribute("n"), new ValueInt(8L));
		attributesMap.add(new Attribute("timestamp"), new ValueTime(nowMs));
		zmiHelperModule.setAttributes("/foo", attributesMap.entryProtoList());
		// Set attribute in /bar
		attributesMap = new AttributesMap();
		attributesMap.add(new Attribute("name"), new ValueString("bar"));
		attributesMap.add(new Attribute("n"), new ValueInt(13L));
		// Set /bar timestamp to be old enough to be purged
		attributesMap.add(new Attribute("timestamp"), new ValueTime(nowMs - 100000L));
		zmiHelperModule.setAttributes("/bar", attributesMap.entryProtoList());
		// Wait for recalculation
		Thread.sleep(500);

		// Get attributes of whole tree
		zmiHelperModule.fetchAttributes("/", true, Collections.<String>emptyList());
		Thread.sleep(50);
		assertEquals(ZmiResponseStatus.OK, zmiHelperModule.getResponseStatus());

		// Check /
		ZmiProto rootZmiProto = zmiHelperModule.getZmiProto();
		ZMI rootZmi = ZMI.fromProto(rootZmiProto);
		// `n` attribute in root shouldn't be purged.
		assertEquals(5L, ((ValueInt) rootZmi.getAttributes().get("n")).getValue().longValue());

		// Check /foo
		ZmiProto fooZmiProto = rootZmiProto.getSons(0);
		attributes = fooZmiProto.getAttributesMapEntriesList();
		assertEquals(3, attributes.size());
		ZMI fooZmi = ZMI.fromProto(fooZmiProto);
		assertEquals("foo", ((ValueString) fooZmi.getAttributes().get("name")).getValue());
		assertEquals(8L, ((ValueInt) fooZmi.getAttributes().get("n")).getValue().longValue());

		// Check if /bar does not exist
		assertEquals(1, rootZmiProto.getSonsCount());

		executor.stop();
	}
}
