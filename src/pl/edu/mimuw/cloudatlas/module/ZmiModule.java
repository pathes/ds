package pl.edu.mimuw.cloudatlas.module;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;


import pl.edu.mimuw.cloudatlas.agent.Executor;
import pl.edu.mimuw.cloudatlas.interpreter.Interpreter;
import pl.edu.mimuw.cloudatlas.interpreter.QueryResult;
import pl.edu.mimuw.cloudatlas.interpreter.query.Yylex;
import pl.edu.mimuw.cloudatlas.interpreter.query.parser;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.Message;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.PingMessage;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.QueryUpdateRequestMessage;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.ZmiRequestMessage;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.ZmiResponseMessage;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.ZmiResponseMessage.ZmiResponseStatus;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.ZmiUpdateRequestMessage;
import pl.edu.mimuw.cloudatlas.model.CloudAtlasModels.AttributesMapEntryProto;
import pl.edu.mimuw.cloudatlas.model.CloudAtlasModels.QueryProto;
import pl.edu.mimuw.cloudatlas.model.CloudAtlasModels.ZmiProto;
import pl.edu.mimuw.cloudatlas.model.PathName;
import pl.edu.mimuw.cloudatlas.model.TreeTraversalException;
import pl.edu.mimuw.cloudatlas.model.Value;
import pl.edu.mimuw.cloudatlas.model.ValueNull;
import pl.edu.mimuw.cloudatlas.model.ValueString;
import pl.edu.mimuw.cloudatlas.model.ValueTime;
import pl.edu.mimuw.cloudatlas.model.ZMI;

public class ZmiModule extends Module {

	private ZMI zmi;
	private Map<String, QueryProto> queryMap;
	private int recalculateIntervalMs;
	private int maxAgeMs;

	public ZmiModule(
			Executor executor, Level loggingLevel, ZMI zmi,
			List<QueryProto> queries,
			int recalculateIntervalMs, int maxAgeMs) {

		super(executor, loggingLevel);
		this.zmi = zmi;
		updateQueries(queries);
		this.recalculateIntervalMs = recalculateIntervalMs;
		this.maxAgeMs = maxAgeMs;
	}

	@Override
	public String getModuleType() {
		return "zmi";
	}

	@Override
	public void run() {
		// Schedule first recalculation
		schedulePing(recalculateIntervalMs);
	}

	@Override
	protected void handleZmiRequest(ZmiRequestMessage zmiRequestMessage, String sourceModule) {
		ZMI sourceZmi = null;
		ZmiResponseMessage messageBody;
		ZmiResponseMessage.Builder messageBodyBuilder = ZmiResponseMessage.newBuilder();
		if (!zmiRequestMessage.hasPathname()) {
			messageBodyBuilder.setStatus(ZmiResponseStatus.FAIL);
			logger.log(Level.WARNING, "Pathname not set");
		} else {
			String pathnameString = zmiRequestMessage.getPathname();
			PathName pathname = new PathName(pathnameString);

			try {
				sourceZmi = zmi.findDescendant(pathname, false);
			} catch (TreeTraversalException e) {
				logger.log(Level.SEVERE, e.getMessage(), e.getStackTrace());
			}
			if (sourceZmi == null) {
				logger.severe("Cannot find requested ZMI " + pathnameString);
				return;
			}
			// Determine depth limit
			int depthLimit = 0;
			if (zmiRequestMessage.hasDepthLimit()) {
				depthLimit = zmiRequestMessage.getDepthLimit();
			}
			// Determine allowed attributes
			HashSet<String> allowedAttributes = new HashSet<String>(zmiRequestMessage.getAttributeList());
			ZmiProto zmiProto = sourceZmi.toProto(allowedAttributes, depthLimit);
			messageBodyBuilder.setZmi(zmiProto);
		}
		messageBody = messageBodyBuilder.build();
		Message message =
			Message.newBuilder()
				.setSourceModule(getModuleType())
				.setDestinationModule(sourceModule)
				.setZmiResponseMessage(messageBody)
				.build();
		executor.enqueue(message);
	}

	@Override
	protected void handleZmiUpdateRequest(ZmiUpdateRequestMessage zmiUpdateRequestMessage) {
		if (!zmiUpdateRequestMessage.hasPathname()) {
			logger.log(Level.WARNING, "Pathname not set");
			return;
		}
		ZMI updatedZmi = null;
		String pathnameString = zmiUpdateRequestMessage.getPathname();
		PathName pathname = new PathName(pathnameString);

		// If the ZMI wasn't even present in our data, we need to create new nodes in tree
		try {
			updatedZmi = zmi.findDescendant(pathname, true);
		} catch (TreeTraversalException e) {
			logger.log(Level.SEVERE, e.getMessage(), e.getStackTrace());
		}
		if (updatedZmi == null) {
			logger.severe("Cannot find requested ZMI " + pathnameString);
			return;
		}

		// Go through attributes and set them
		for (AttributesMapEntryProto entry : zmiUpdateRequestMessage.getAttributesMapEntriesList()) {
			if (entry.getValue() == null) {
				updatedZmi.getAttributes().addOrChange(entry.getKey(), ValueNull.getInstance());
			} else {
				updatedZmi.getAttributes().addOrChange(entry.getKey(), Value.fromProto(entry.getValue()));
			}
		}
	}

	@Override
	protected void handleQueryUpdateRequest(QueryUpdateRequestMessage queryUpdateRequestMessage) {
		updateQueries(queryUpdateRequestMessage.getQueryList());
	}

	private void updateQueries(List<QueryProto> queries) {
		Map<String, QueryProto> newQueryMap = new HashMap<String, QueryProto>();
		if (queries != null) {
			for (QueryProto query : queries) {
				newQueryMap.put(query.getName(), query);
			}
		}
		queryMap = newQueryMap;
	}

	@Override
	protected void handlePing(PingMessage pingMessage) {
		recalculateZmi(zmi);
		purgeOldZmi(zmi, System.currentTimeMillis());
		schedulePing(recalculateIntervalMs);
	}

	private boolean purgeOldZmi(ZMI currentZmi, long referenceTime) {
		List<ZMI> purgedChildren = new ArrayList<ZMI>();

		for (ZMI child : currentZmi.getSons()) {
			if (purgeOldZmi(child, referenceTime)) {
				purgedChildren.add(child);
			}
		}

		for (ZMI purgedChild : purgedChildren) {
			logger.warning("Purging ZMI node: " + ((ValueString) purgedChild.getAttributes().get("name")).getValue());
			currentZmi.removeSon(purgedChild);
		}

		boolean needToPurge;
		Long timestamp = currentZmi.getTimestamp();

		if (timestamp == null) {
			logger.severe("Invalid timestamp");
			needToPurge = true;
		} else {
			needToPurge = referenceTime - maxAgeMs > timestamp;
		}

		return needToPurge;
	}

	/**
	 * Recalculate ZMI.
	 * @return Timestamp of recalculated ZMI.
	 */
	private Long recalculateZmi(ZMI currentZmi) {
		// If the node is a leaf node, don't recalculate
		if (currentZmi.getSons().isEmpty()) {
			return currentZmi.getTimestamp();
		}
		// DFS post-order, let children recalculate first.
		// Also, calculate new timestamp - let it be the youngest of children timestamps
		Long newTimestamp = null;
		for (ZMI child : currentZmi.getSons()) {
			Long timestampCandidate = recalculateZmi(child);
			if (timestampCandidate != null) {
				if (newTimestamp == null || newTimestamp < timestampCandidate) {
					newTimestamp = timestampCandidate;
				}
			}
		}
		// Update timestamp only if the update can increase it.
		// That's because we might get parent from gossiping and we didn't obtain all new children.
		if (newTimestamp != null && (currentZmi.getTimestamp() == null || currentZmi.getTimestamp() < newTimestamp)) {
			currentZmi.getAttributes().addOrChange("timestamp", new ValueTime(newTimestamp));
		}
		// Recalculation on this level
		Interpreter interpreter = new Interpreter(currentZmi);
		for (QueryProto query : queryMap.values()) {
			try {
				evaluateQuery(currentZmi, interpreter, query);
			} catch (Exception e) {
				logger.log(Level.WARNING, "Query evaluation failed: " + e.getMessage(), e.getStackTrace());
			}
		}
		return currentZmi.getTimestamp();
	}

	private void evaluateQuery(ZMI currentZmi, Interpreter interpreter, QueryProto query) {
		if (query.getContent().isEmpty()) {
			return;
		}
		Yylex lex = new Yylex(new ByteArrayInputStream(query.getContent().getBytes()));
		try {
			List<QueryResult> resultList = interpreter.interpretProgram((new parser(lex)).pProgram());
			for (QueryResult result : resultList) {
				currentZmi.getAttributes().addOrChange(result.getName(), result.getValue());
				logger.info("Evaluating query " + query.getName() + " gives result: " + result);
			}
		} catch(Exception e) {
			logger.log(
				Level.SEVERE,
				"Failed to execute query: " + e.getMessage() + "\n" + query,
				e.getStackTrace());
		}
	}
}
