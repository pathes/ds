package pl.edu.mimuw.cloudatlas.module;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Level;

import org.junit.Test;

import pl.edu.mimuw.cloudatlas.agent.Executor;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.QueryUpdateRequestMessage;
import pl.edu.mimuw.cloudatlas.model.AttributesMap;
import pl.edu.mimuw.cloudatlas.model.PathName;
import pl.edu.mimuw.cloudatlas.model.TypePrimitive;
import pl.edu.mimuw.cloudatlas.model.Value;
import pl.edu.mimuw.cloudatlas.model.ValueContact;
import pl.edu.mimuw.cloudatlas.model.ValueInt;
import pl.edu.mimuw.cloudatlas.model.ValueList;
import pl.edu.mimuw.cloudatlas.model.ValueString;
import pl.edu.mimuw.cloudatlas.model.ValueTime;
import pl.edu.mimuw.cloudatlas.model.ZMI;
import pl.edu.mimuw.cloudatlas.model.CloudAtlasModels.QueryProto;
import pl.edu.mimuw.cloudatlas.model.CloudAtlasModels.ZmiProto;
import pl.edu.mimuw.cloudatlas.module.gossip.GossipStrategy;
import pl.edu.mimuw.cloudatlas.module.gossip.LinearLevelStrategy;
import pl.edu.mimuw.cloudatlas.module.gossip.RoundRobinNeighborStrategy;


//QueryGossip pushes update to ClientModule. We need to mock it.
class FakeClientModule extends Module {

	public FakeClientModule(Executor executor, Level loggingLevel) {
		super(executor, loggingLevel);
	}

	@Override
	public String getModuleType() {
		return "client";
	}

	@Override
	protected void handleQueryUpdateRequest(QueryUpdateRequestMessage queryUpdateRequestMessage) {
	}
}


class QueryGossipExecutionHelper {
	private Executor executor;
	@SuppressWarnings("unused")
	private TimerModule timerModule;
	@SuppressWarnings("unused")
	private UdpCommunicationModule udpCommunicationModule;
	private ZmiModule zmiModule;
	private ZmiHelperModule zmiHelperModule;
	private QueryGossipModule queryGossipModule;
	@SuppressWarnings("unused")
	private FakeClientModule fakeClientModule;
	private CountDownLatch latch;

	public QueryGossipExecutionHelper(Level logLevel, ZMI root, String pathname,
			int listenerPort, int remoteListenerPort, int senderPort,
			List<QueryProto> queries)
			throws ParseException, IOException {
		latch = new CountDownLatch(1);
		executor = new Executor(logLevel, null);
		// Timer module is required for communication retry
		timerModule = new TimerModule(executor, logLevel);
		udpCommunicationModule = new UdpCommunicationModule(
			executor,
			logLevel,
			pathname,
			listenerPort,
			remoteListenerPort,
			senderPort);
		zmiModule = new ZmiModule(
				executor, logLevel, root,
				null,
				1000,  // recalculate every 1000ms
				60000);
		zmiHelperModule = new ZmiHelperModule(executor, logLevel);
		queryGossipModule = new QueryGossipModule(
			executor,
			logLevel,
			new PathName(pathname),
			root,
			new GossipStrategy(
				new LinearLevelStrategy(),
				new RoundRobinNeighborStrategy()),
			500,
			null,
			queries);
		fakeClientModule = new FakeClientModule(executor, logLevel);
	}

	public CountDownLatch getLatch() {
		return latch;
	}

	public Executor getExecutor() {
		return executor;
	}

	public ZmiModule getZmiModule() {
		return zmiModule;
	}

	public ZmiHelperModule getZmiHelperModule() {
		return zmiHelperModule;
	}

	public QueryGossipModule getQueryGossipModule() {
		return queryGossipModule;
	}
}


public class QueryGossipModuleTest {

	private ValueContact createContact(String path) throws UnknownHostException {
		return new ValueContact(new PathName(path), InetAddress.getByAddress(new byte[] {
			(byte)127, (byte)0, (byte)0, (byte)1
		}));
	}

	private ZMI hierarchyHelper(ZMI parent, String name, String owner, List<Value> contactList) {
		ZMI zmi = new ZMI(parent);
		if (parent != null) {
			parent.addSon(zmi);
		}
		zmi.getAttributes().add("name", new ValueString(name));
		zmi.getAttributes().add("contacts", new ValueList(new ArrayList<Value>(contactList), TypePrimitive.CONTACT));
		zmi.getAttributes().add("owner", new ValueString(owner));
		zmi.getAttributes().add("timestamp", new ValueTime(0L));
		zmi.getAttributes().add("n", new ValueInt(0L));
		return zmi;
	}

	/**
	 * Simple ZMI hierarchy for testing if /foo can gossip with /bar
	 */
	private ZMI createSmallHierarchy() throws ParseException, UnknownHostException {
		ValueContact fooContact = createContact("/foo");
		ValueContact barContact = createContact("/bar");
		List<Value> contactList = Arrays.asList(new Value[] {fooContact, barContact});

		ZMI root = hierarchyHelper(null, "", "/foo", contactList);
		hierarchyHelper(root, "foo", "/foo", contactList);
		hierarchyHelper(root, "bar", "/bar", contactList);

		return root;
	}

	private long fetchAttribute(QueryGossipExecutionHelper helper, String attribute, String pathname) throws InterruptedException {
		helper.getZmiHelperModule().fetchAttributes(pathname, true, Arrays.asList(attribute));
		Thread.sleep(100);
		ZmiProto fetchedZmiProto = helper.getZmiHelperModule().getZmiProto();
		ZMI fetchedZmi = ZMI.fromProto(fetchedZmiProto);
		Value fetchedValue = fetchedZmi.getAttributes().getOrNull(attribute);
		if (fetchedValue instanceof ValueInt) {
			return ((ValueInt) fetchedValue).getValue().longValue();
		} else if (fetchedValue instanceof ValueTime) {
			return ((ValueTime) fetchedValue).getValue().longValue();
		}
		return -1L;
	}


	@Test
	public void testSmallGossip() throws ParseException, IOException, InterruptedException {
		// We need to set ZMI fresh, so that they don't get purged.
		long nowMs = System.currentTimeMillis();
		// Prepare queries
		QueryProto fooSumN = QueryProto.newBuilder()
				.setName("sum_n")
				.setContent("SELECT sum(n) AS sum_n")
				.setTimestamp(15000L)
				.build();
		QueryProto barSumN = QueryProto.newBuilder()
				.setName("sum_n")
				.setContent("SELECT 1+1 AS sum_n")
				.setTimestamp(1000L)
				.build();
		QueryProto barAnswer = QueryProto.newBuilder()
				.setName("answer_query")
				.setContent("SELECT 42 AS answer")
				.setTimestamp(3000L)
				.build();
		QueryGossipExecutionHelper helperFoo = new QueryGossipExecutionHelper(
			Level.OFF, createSmallHierarchy(), "/foo", 9210, 9220, 9211, Arrays.asList(fooSumN));
		QueryGossipExecutionHelper helperBar = new QueryGossipExecutionHelper(
			Level.OFF, createSmallHierarchy(), "/bar", 9220, 9210, 9221, Arrays.asList(barSumN, barAnswer));
		// Set new values in foo-agent's /foo and /bar
		AttributesMap attributesMap = new AttributesMap();
		attributesMap.add("n", new ValueInt(1L));
		attributesMap.add("timestamp", new ValueTime(nowMs));
		helperFoo.getZmiHelperModule().setAttributes("/foo", attributesMap.entryProtoList());
		attributesMap = new AttributesMap();
		attributesMap.add("n", new ValueInt(2L));
		attributesMap.add("timestamp", new ValueTime(nowMs));
		helperFoo.getZmiHelperModule().setAttributes("/bar", attributesMap.entryProtoList());
		// Set new values in bar-agent's /foo and /bar
		attributesMap = new AttributesMap();
		attributesMap.add("n", new ValueInt(7L));
		attributesMap.add("timestamp", new ValueTime(nowMs));
		helperBar.getZmiHelperModule().setAttributes("/foo", attributesMap.entryProtoList());
		attributesMap = new AttributesMap();
		attributesMap.add("n", new ValueInt(8L));
		attributesMap.add("timestamp", new ValueTime(nowMs));
		helperBar.getZmiHelperModule().setAttributes("/bar", attributesMap.entryProtoList());
		// Run threads
		(new Thread(helperFoo.getExecutor())).start();
		(new Thread(helperBar.getExecutor())).start();
		try {
			Thread.sleep(2000);
			// Check foo-agent
			assertEquals(1L, fetchAttribute(helperFoo, "n", "/foo"));
			assertEquals(2L, fetchAttribute(helperFoo, "n", "/bar"));
			assertEquals(3L, fetchAttribute(helperFoo, "sum_n", "/"));
			// `answer_query` should propagate onto foo-agent
			assertEquals(42L, fetchAttribute(helperFoo, "answer", "/"));
			// Check bar-agent
			assertEquals(7L, fetchAttribute(helperBar, "n", "/foo"));
			assertEquals(8L, fetchAttribute(helperBar, "n", "/bar"));
			assertEquals(15L, fetchAttribute(helperBar, "sum_n", "/"));
			assertEquals(42L, fetchAttribute(helperBar, "answer", "/"));
		} finally {
			helperFoo.getExecutor().stop();
			helperBar.getExecutor().stop();
			// Allow sockets to be freed
			Thread.sleep(100);
		}
	}
}
