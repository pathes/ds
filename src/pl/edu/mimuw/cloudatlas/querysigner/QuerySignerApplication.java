package pl.edu.mimuw.cloudatlas.querysigner;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class QuerySignerApplication {
	public static void main(String[] args) throws IOException, InvalidKeySpecException, NoSuchAlgorithmException {
		SpringApplication.run(QuerySignerApplication.class, args);
		QuerySignerPrivateKey.setPrivateKeyFilePath(args[0]);
	}
}
