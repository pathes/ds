package pl.edu.mimuw.cloudatlas.querysigner;

public class QuerySignerSignResponse{
	private QuerySignerSignRequest initialRequest;
	private byte[] signature;
	long timestamp;


	public QuerySignerSignResponse(QuerySignerSignRequest initialRequest,
			byte[] signature, long timestamp) {
		this.initialRequest = initialRequest;
		this.signature = signature;
		this.timestamp = timestamp;
	}

	public QuerySignerSignResponse() {
	}

	public byte[] getSignature() {
		return signature;
	}

	public void setSignature(byte[] signature) {
		this.signature = signature;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public QuerySignerSignRequest getInitialRequest() {
		return initialRequest;
	}

	public void setInitialRequest(QuerySignerSignRequest initialRequest) {
		this.initialRequest = initialRequest;
	}

}
