package pl.edu.mimuw.cloudatlas.querysigner;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;

/**
 *
 * @author Paweł Kamiński
 * essence from http://stackoverflow.com/questions/11410770/load-rsa-public-key-from-file
 */
public class QuerySignerPrivateKey {
	String privateKeyFilePath;
	PrivateKey privateKey;
	private final static QuerySignerPrivateKey instance = new QuerySignerPrivateKey();

	public static QuerySignerPrivateKey getInstance() {
        return instance;
    }

	public static void setPrivateKeyFilePath (String privateKeyFilePath) throws IOException, InvalidKeySpecException, NoSuchAlgorithmException  {
		if (instance.privateKeyFilePath.equals(privateKeyFilePath)){
			return;
		}

		instance.privateKeyFilePath = privateKeyFilePath;

		File f = new File(privateKeyFilePath);
	    FileInputStream fis = new FileInputStream(f);
	    DataInputStream dis = new DataInputStream(fis);
	    byte[] keyBytes = new byte[(int)f.length()];
	    dis.readFully(keyBytes);
	    dis.close();

	    PKCS8EncodedKeySpec spec =
	      new PKCS8EncodedKeySpec(keyBytes);
	    KeyFactory kf = KeyFactory.getInstance("RSA");
	    instance.privateKey = kf.generatePrivate(spec);

	}

	public static PrivateKey getPrivateKey() {
		return instance.privateKey;
	}

	private QuerySignerPrivateKey () {
		privateKeyFilePath = "";
	}

}
