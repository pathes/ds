package pl.edu.mimuw.cloudatlas.querysigner;

import java.io.ByteArrayInputStream;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;

import pl.edu.mimuw.cloudatlas.interpreter.QueryResult;
import pl.edu.mimuw.cloudatlas.interpreter.Table;
import pl.edu.mimuw.cloudatlas.interpreter.query.Yylex;
import pl.edu.mimuw.cloudatlas.interpreter.query.parser;
import pl.edu.mimuw.cloudatlas.interpreter.query.Absyn.AliasedSelItemC;
import pl.edu.mimuw.cloudatlas.interpreter.query.Absyn.Program;
import pl.edu.mimuw.cloudatlas.interpreter.query.Absyn.ProgramC;
import pl.edu.mimuw.cloudatlas.interpreter.query.Absyn.SelItem;
import pl.edu.mimuw.cloudatlas.interpreter.query.Absyn.SelItemC;
import pl.edu.mimuw.cloudatlas.interpreter.query.Absyn.Statement;
import pl.edu.mimuw.cloudatlas.interpreter.query.Absyn.StatementC;
import pl.edu.mimuw.cloudatlas.message.CloudAtlasMessages.QuerySignerSignRequestMessage;
import pl.edu.mimuw.cloudatlas.model.Attribute;
import pl.edu.mimuw.cloudatlas.model.ValueNull;
import pl.edu.mimuw.cloudatlas.model.ZMI;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;


@RestController
public class QuerySignerControler {
    private final Logger logger = Logger.getLogger(QuerySignerControler.class);
    private static HashMap<String, String> attributeNameToQueryName = new HashMap<String, String>();
    private static HashMap<String, Boolean> queryNameToQuery = new HashMap<String, Boolean>();
    private final ObjectMapper mapper = new ObjectMapper();
    private final static String DIGEST_ALGORITHM = "SHA-256";
    private final static String ENCRYPTION_ALGORITHM = "RSA";

    private ResponseEntity<String> jsonError(String message, HttpStatus status) {
    	return new ResponseEntity<String>("{\"error\": \"" + message + "\"}", status);
    }

	private class ProgramChecker implements Program.Visitor<List<QueryResult>, ZMI> {
		@Override
		public List<QueryResult> visit(ProgramC program, ZMI zmi) {
			List<QueryResult> results = new ArrayList<QueryResult>();
			for(Statement s : program.liststatement_) {
				List<QueryResult> l = s.accept(new StatementChecker(), null);
				for(QueryResult qr : l)
					if(qr.getName() == null)
						throw new IllegalArgumentException("All items in top-level SELECT must be aliased.");
				results.addAll(l);
			}
			return results;
		}
	}

	private class StatementChecker  implements Statement.Visitor<List<QueryResult>, ZMI> {
		@Override
		public List<QueryResult> visit(StatementC statement, ZMI zmi) {
			List<QueryResult> ret = new ArrayList<QueryResult>();

			for(SelItem selItem : statement.listselitem_) {
					QueryResult qr = selItem.accept(new SelItemChecker(), null);
					if(qr.getName() != null) {
						for(QueryResult qrRet : ret)
							if(qr.getName().getName().equals(qrRet.getName().getName()))
								throw new IllegalArgumentException("Alias collision.");
					}
					ret.add(qr);

			}

			return ret;
		}
	}

	private class SelItemChecker implements SelItem.Visitor<QueryResult, Table> {
		@Override
		public QueryResult visit(SelItemC selItem, Table table) {
			return null;
		}

		@Override
		public QueryResult visit(AliasedSelItemC selItem, Table table) {
			return new QueryResult(new Attribute(selItem.qident_), ValueNull.getInstance());
		}
	}

	@RequestMapping(value = "/signer/query",
			method = RequestMethod.POST,
			produces="application/json")
	public ResponseEntity<String> signQuery(
			@RequestBody QuerySignerSignRequest query)  {

		logger.info("Sign query called");


		// We accept two situations:
		// - query is empty string
		// - query is a well-formed CloudAtlas query
		List<String> queryAttributeNames = new ArrayList<String>();
		if (!query.getQueryContent().isEmpty()) {
			// Query not empty - check if it's well-formed
			Yylex lex = new Yylex(new ByteArrayInputStream(query.getQueryContent().getBytes()));
			parser p = new parser(lex);
			Program program;
			List<QueryResult> results;
			String attributeName;

		    try {
		    	program = p.pProgram();
		    	logger.info("Query parsed");
		    } catch(Throwable e) {
	        	logger.error("Query parse error: at line " + String.valueOf(lex.line_num()) + ", near \"" + lex.buff() + "\" :");
				return jsonError("Query couldn't be parsed.", HttpStatus.BAD_REQUEST);
		    }
		    try {
		    	results = program.accept(new ProgramChecker(), null);
		    } catch (IllegalArgumentException e) {
	        	logger.error("Query alias error");
				return jsonError("Query alias error.", HttpStatus.BAD_REQUEST);
		    }

		    for (QueryResult result : results){
		    	attributeName = result.getName().getName();
		    	if (attributeNameToQueryName.containsKey(attributeName)
		    			&& !attributeNameToQueryName.get(attributeName).equals(query.getQueryName())) {
		        	logger.error("Query attribute name conflict.");
					return jsonError("Query attribute name conflict.", HttpStatus.BAD_REQUEST);
		    	}
		    	queryAttributeNames.add(attributeName);
		    }

		    //Check if is aggregation function
		    String[] aggregationOperationsList  = {"count", "sum", "avg", "land", "lor", "min", "max", "first", "random", "last", "size"};
		    Boolean isAggregationFunction = false;
		    for (String word: Arrays.asList(aggregationOperationsList)) {
		    	if (query.getQueryContent().contains(word)){
		    		isAggregationFunction = true;
		    	}
		    }

		    if (!isAggregationFunction) {
	        	logger.error("Query is not an aggregation function.");
				return jsonError("Query is not an aggregation function.", HttpStatus.BAD_REQUEST);
		    }

		} else {
			if (!queryNameToQuery.containsKey(query.getQueryName()) || !queryNameToQuery.get(query.getQueryName())) {
	        	logger.error("Couldn't remove uninitialized query");
				return jsonError("Couldn't remove uninitialized query.", HttpStatus.BAD_REQUEST);
			}
		}

		QuerySignerSignRequestMessage querySignerSignRequestProto =
				QuerySignerSignRequestMessage.newBuilder()
					.setName(query.getQueryName())
					.setValue(query.getQueryContent())
					.build();
		byte[] serialized = querySignerSignRequestProto.toByteArray();
		byte[] digest, signature;

		// hash serialized value
		try {
            MessageDigest digestGenerator =
                    MessageDigest.getInstance(DIGEST_ALGORITHM);
            digest = digestGenerator.digest(serialized);
        } catch (NoSuchAlgorithmException e) {
        	logger.error("No "+ DIGEST_ALGORITHM + "Algorithm.");
            e.printStackTrace();
			return jsonError("No "+ DIGEST_ALGORITHM + "Algorithm.", HttpStatus.BAD_REQUEST);
        }

		PrivateKey privateKey = QuerySignerPrivateKey.getPrivateKey();

		//Sign Query with RSA
		try {

			Cipher signCipher = Cipher.getInstance(ENCRYPTION_ALGORITHM);
            signCipher.init(Cipher.ENCRYPT_MODE, privateKey);

            signature = signCipher.doFinal(digest);
		} catch (NoSuchAlgorithmException e) {
			logger.error("No " + ENCRYPTION_ALGORITHM + " Algorithm.");
			e.printStackTrace();
			return jsonError("No " + ENCRYPTION_ALGORITHM + " Algorithm.", HttpStatus.BAD_REQUEST);
		} catch (InvalidKeyException e) {
			logger.error(ENCRYPTION_ALGORITHM + " key error.");
			e.printStackTrace();
			return jsonError(ENCRYPTION_ALGORITHM + " key error.", HttpStatus.BAD_REQUEST);
		} catch (NoSuchPaddingException |
				IllegalBlockSizeException |
				BadPaddingException e) {
			logger.error(ENCRYPTION_ALGORITHM + " evaluation error.");
			e.printStackTrace();
			return jsonError(ENCRYPTION_ALGORITHM + " evaluation error.", HttpStatus.BAD_REQUEST);
        }

		long unixTime = System.currentTimeMillis();
		QuerySignerSignResponse responseClass = new QuerySignerSignResponse(query, signature, unixTime);

		String responseJsonString;
		try {
			responseJsonString = mapper.writeValueAsString(responseClass);
			ArrayList<String> removeAttributes = new ArrayList<String>();

			for (Map.Entry<String, String> entry : attributeNameToQueryName.entrySet()) {
				if (query.getQueryName().equals(entry.getValue())) {
					removeAttributes.add(entry.getKey());
				}
			}

			for (String removedAttribute: removeAttributes) {
				attributeNameToQueryName.remove(removedAttribute);
			}

			for (String attributeName : queryAttributeNames) {
				attributeNameToQueryName.put(attributeName, query.getQueryName());
			}
			queryNameToQuery.put(query.getQueryName(), !query.getQueryContent().isEmpty());

		} catch (JsonProcessingException e) {
			logger.error("JSON processing error.");
			e.printStackTrace();
			return jsonError("JSON processing error.", HttpStatus.BAD_REQUEST);
		}


		return new ResponseEntity<String>(responseJsonString, HttpStatus.OK);
	}

}

