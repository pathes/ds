package pl.edu.mimuw.cloudatlas.querysigner;

public class QuerySignerSignRequest {

	private String queryName, queryContent;

	public String getQueryContent() {
		return queryContent;
	}

	public void setQueryContent(String queryContent) {
		this.queryContent = queryContent;
	}

	public String getQueryName() {
		return queryName;
	}

	public void setQueryName(String queryName) {
		this.queryName = queryName;
	}
}
